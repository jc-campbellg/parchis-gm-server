{
    "id": "df45b056-ca08-4371-b46b-0932f41fe572",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonRedFirst",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "ccc01f10-e29c-4f40-99db-609e5602e2ca",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 329,
            "y": 33,
            "speed": 100
        },
        {
            "id": "70397fba-f61b-4825-8cfb-b0a2ad4728f9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 329,
            "y": 54,
            "speed": 100
        },
        {
            "id": "8d652ba7-8b13-4868-8da1-38730565dc83",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 329,
            "y": 78,
            "speed": 100
        },
        {
            "id": "41b9d48a-3142-453f-8af8-8faf0477c886",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 329,
            "y": 99,
            "speed": 100
        },
        {
            "id": "7509de66-49c6-425a-b420-1c881149aac4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 329,
            "y": 120,
            "speed": 100
        },
        {
            "id": "2011408f-9fe8-4526-9f4e-37e9445bfff7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 329,
            "y": 141,
            "speed": 100
        },
        {
            "id": "6bd715d2-1280-4ac0-97f4-1af091bdb82f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 329,
            "y": 162,
            "speed": 100
        },
        {
            "id": "3e840e15-27f4-4283-be50-ab9af4d820bc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 329,
            "y": 183,
            "speed": 100
        },
        {
            "id": "d0b84118-6d87-47ae-8298-bf6d538c8d1c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 329,
            "y": 206,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}