{
    "id": "2c17f128-a4a6-40f7-a267-d13b247e0f8e",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonOrange",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "5eb38f55-042b-43c0-ad6a-fe89c9524539",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 568,
            "y": 166,
            "speed": 100
        },
        {
            "id": "5e398437-9f02-44f8-a2e9-edf1807c601a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 549,
            "y": 177,
            "speed": 100
        },
        {
            "id": "5f2416f6-8e98-4278-8f02-2a5d99b27465",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 532,
            "y": 187,
            "speed": 100
        },
        {
            "id": "0c5b1be5-e741-41bd-9722-c3407d7d44a1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 513,
            "y": 198,
            "speed": 100
        },
        {
            "id": "22c8795a-7272-43c1-926b-58d8567a20fb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 495,
            "y": 208,
            "speed": 100
        },
        {
            "id": "4e6ad61c-c874-4c59-9f36-429594c81594",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 477,
            "y": 218,
            "speed": 100
        },
        {
            "id": "90375a6e-d4b7-4178-bf64-db8139da5bb1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 459,
            "y": 229,
            "speed": 100
        },
        {
            "id": "abec7cc1-156b-4944-a50f-eb542d5f6cc1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 441,
            "y": 240,
            "speed": 100
        },
        {
            "id": "a03979e5-9e52-4987-bc6e-27b4316542f9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 419,
            "y": 253,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}