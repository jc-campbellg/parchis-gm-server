{
    "id": "f86bfa9e-3366-4e85-a97d-2c07baace6c1",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonYellowSecond",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "a5a70928-81f3-4dfc-bfc4-83dc5e3fe120",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 109,
            "y": 416,
            "speed": 100
        },
        {
            "id": "2da2b05b-a497-4a3a-a3af-17d6d76b2b44",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 127,
            "y": 406,
            "speed": 100
        },
        {
            "id": "92dcd567-00c4-496d-80bc-0354a75456ff",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 146,
            "y": 395,
            "speed": 100
        },
        {
            "id": "d98ef33d-3f3f-48e9-9e43-45c14133b04c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 165,
            "y": 385,
            "speed": 100
        },
        {
            "id": "4730f80c-2c6b-407a-9ce7-03bdb496cf3c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 183,
            "y": 375,
            "speed": 100
        },
        {
            "id": "b8fbe46e-445c-44a2-8c5d-2ffbb626b54b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 201,
            "y": 364,
            "speed": 100
        },
        {
            "id": "ba11f016-96bc-48c7-835a-1df42bff27e0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 219,
            "y": 354,
            "speed": 100
        },
        {
            "id": "08991fbf-7400-410e-ada8-9ab3c9315708",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 238,
            "y": 343,
            "speed": 100
        },
        {
            "id": "22530b6c-cc8b-42f7-baa7-274fc29fa384",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 258,
            "y": 329,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}