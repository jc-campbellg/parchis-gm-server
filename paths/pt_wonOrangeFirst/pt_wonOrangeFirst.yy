{
    "id": "a0a2666d-6942-4295-b041-62419e2d2bbf",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonOrangeFirst",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "39755b25-fa1b-4f0c-bdcc-2a06e87fa176",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 561,
            "y": 154,
            "speed": 100
        },
        {
            "id": "d80ad49a-c0d9-4e02-8b33-f684a33d1fda",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 542,
            "y": 165,
            "speed": 100
        },
        {
            "id": "d34806b8-f0c0-4f4a-b111-a65e9e384dbb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 524,
            "y": 175,
            "speed": 100
        },
        {
            "id": "b4df4ab4-e38d-4a96-a88a-0b6a9fe4046e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 505,
            "y": 186,
            "speed": 100
        },
        {
            "id": "18085d09-c780-4710-ad3f-124c2cb54138",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 487,
            "y": 196,
            "speed": 100
        },
        {
            "id": "401307a3-e24d-4748-94ee-140a9b22a435",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 469,
            "y": 206,
            "speed": 100
        },
        {
            "id": "cc817a1c-e6af-4102-8904-a2ae2240de33",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 451,
            "y": 217,
            "speed": 100
        },
        {
            "id": "0a844b4e-f006-4719-b1aa-38ed840ef3cb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 433,
            "y": 228,
            "speed": 100
        },
        {
            "id": "8eb12b7e-94dd-47a9-8fe0-b5a520ad74d3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 412,
            "y": 240,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}