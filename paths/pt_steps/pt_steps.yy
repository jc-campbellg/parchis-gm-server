{
    "id": "6cfaeab7-6fb8-4cc4-a6a8-df150a334456",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_steps",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "6e0570d7-be92-4522-b790-1f46fc83d099",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 289,
            "y": 119,
            "speed": 100
        },
        {
            "id": "7d0957b0-bce0-4cfb-9692-696e3fe2f9a4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 289,
            "y": 140,
            "speed": 100
        },
        {
            "id": "109551e7-7f29-42f8-a150-6b19617b519d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 289,
            "y": 161,
            "speed": 100
        },
        {
            "id": "ed65a3b7-e13e-4b9c-9ebc-b6510cca617a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 289,
            "y": 182,
            "speed": 100
        },
        {
            "id": "3016bcc8-2447-4d4c-97cc-76c83de3e55c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 269,
            "y": 195,
            "speed": 100
        },
        {
            "id": "b0d2d2ee-d804-4ede-ab1f-9e4fdf90b643",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 250,
            "y": 185,
            "speed": 100
        },
        {
            "id": "b0fa43e3-bfa9-414f-95e9-f97bd5782232",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 232,
            "y": 174,
            "speed": 100
        },
        {
            "id": "22b9f269-d554-4fac-b1a8-343694e89c14",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 214,
            "y": 163,
            "speed": 100
        },
        {
            "id": "9fae7e91-4464-4a2a-8a3c-8d8052d22dac",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 196,
            "y": 153,
            "speed": 100
        },
        {
            "id": "c46d92f2-84e8-43da-9e5b-fb8eea93a867",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 178,
            "y": 143,
            "speed": 100
        },
        {
            "id": "f3d37a19-b178-4443-839a-98de2350d47d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 132,
            "speed": 100
        },
        {
            "id": "55cf793f-55d2-440b-a874-dd56a4a2de44",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 141,
            "y": 122,
            "speed": 100
        },
        {
            "id": "91aae1c2-2b05-48d8-b24d-758ef80c4792",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 114,
            "y": 167,
            "speed": 100
        },
        {
            "id": "457fd369-8b21-4eab-9e64-337b29942f55",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 89,
            "y": 212,
            "speed": 100
        },
        {
            "id": "53e64114-8610-4590-bca4-90af91222388",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 108,
            "y": 222,
            "speed": 100
        },
        {
            "id": "216195cb-c950-4804-9951-1cd291622598",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 126,
            "y": 233,
            "speed": 100
        },
        {
            "id": "957cfe97-fd46-43c9-9ac4-22d457131c91",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 144,
            "y": 243,
            "speed": 100
        },
        {
            "id": "71870e5d-1466-4fd8-a017-dc60d98dd351",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 162,
            "y": 254,
            "speed": 100
        },
        {
            "id": "761ac8d2-eee5-48c9-a260-7f0b0ac2d6a1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 180,
            "y": 264,
            "speed": 100
        },
        {
            "id": "7b11ded5-09e9-4884-a7ef-6666e53922ca",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 198,
            "y": 275,
            "speed": 100
        },
        {
            "id": "1a9986d7-50a6-4219-bd5a-f6d9ba85bcc7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 217,
            "y": 286,
            "speed": 100
        },
        {
            "id": "43c986d9-e2a9-41c4-9b67-52e3e7d6a028",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 216,
            "y": 307,
            "speed": 100
        },
        {
            "id": "9431dc9b-e39f-4b0f-a00d-9adc5540af06",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 198,
            "y": 317,
            "speed": 100
        },
        {
            "id": "b5de0ec0-7057-4dab-8f78-1ef0f5ec4a9d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 179,
            "y": 328,
            "speed": 100
        },
        {
            "id": "e5310166-9a1e-456d-a891-bec1b8019d53",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 161,
            "y": 338,
            "speed": 100
        },
        {
            "id": "0b41bcf8-8375-4837-bc9d-c30fde1245fc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 143,
            "y": 349,
            "speed": 100
        },
        {
            "id": "c2710488-e8d9-4685-9730-8e9240a6a90d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 125,
            "y": 359,
            "speed": 100
        },
        {
            "id": "762c4f43-b19f-430e-adbb-7ea6b34f15e4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 107,
            "y": 370,
            "speed": 100
        },
        {
            "id": "78679d96-95ef-4388-85eb-81f18f74e1c2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 89,
            "y": 380,
            "speed": 100
        },
        {
            "id": "85895380-ee33-40f6-823b-414916583a36",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 115,
            "y": 427,
            "speed": 100
        },
        {
            "id": "31f85424-cb01-4cc8-ad3e-a5606780ac6c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 141,
            "y": 471,
            "speed": 100
        },
        {
            "id": "8f3ece0f-0d26-41f0-87f4-85aab7b3a94f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 159,
            "y": 460,
            "speed": 100
        },
        {
            "id": "5831a061-f4d4-4cde-a07c-c93f2992ec1a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 178,
            "y": 450,
            "speed": 100
        },
        {
            "id": "e9d7ce47-5d1f-4d85-ad77-44cae0bfe8d8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 196,
            "y": 439,
            "speed": 100
        },
        {
            "id": "57fc5565-c18c-4ddd-b218-f385b43ab372",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 214,
            "y": 429,
            "speed": 100
        },
        {
            "id": "759c9d01-7cd3-4697-85fa-3bde2d89c607",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 232,
            "y": 418,
            "speed": 100
        },
        {
            "id": "cf896217-da88-42aa-a536-5524f4fc9788",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 250,
            "y": 408,
            "speed": 100
        },
        {
            "id": "a1f118ba-1489-4d51-8429-f34e9ad69c2f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 269,
            "y": 397,
            "speed": 100
        },
        {
            "id": "c62adc01-33dc-498f-8fa1-1e0cbd12f284",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 290,
            "y": 410,
            "speed": 100
        },
        {
            "id": "725c5c84-749f-44c5-94d4-10a1c2508d0e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 290,
            "y": 431,
            "speed": 100
        },
        {
            "id": "8720ca46-583f-40b4-9ed7-7af5abffa115",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 290,
            "y": 452,
            "speed": 100
        },
        {
            "id": "74f99edf-d808-446b-8881-9a66b92729c4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 290,
            "y": 473,
            "speed": 100
        },
        {
            "id": "c33727e8-de40-44ff-b88b-64002a0cb90d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 290,
            "y": 494,
            "speed": 100
        },
        {
            "id": "508aeaae-9eaa-4e24-88ef-701d7c4902b9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 290,
            "y": 515,
            "speed": 100
        },
        {
            "id": "c2d1ce4f-6b8d-4e32-94e5-3ebc2ca3c4bd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 290,
            "y": 536,
            "speed": 100
        },
        {
            "id": "166945c4-f496-4877-b1b2-da7aba12595a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 290,
            "y": 557,
            "speed": 100
        },
        {
            "id": "c306597c-7c90-4c83-9e4a-e1ab01364282",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 340,
            "y": 557,
            "speed": 100
        },
        {
            "id": "0fe4155a-3c74-4c7f-8756-e921ab173033",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 394,
            "y": 556,
            "speed": 100
        },
        {
            "id": "4247c7bd-29a1-4df0-9e9b-a9b1ff5881a1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 394,
            "y": 535,
            "speed": 100
        },
        {
            "id": "056273a9-f171-4d10-9c2a-6bbe2395e8cb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 394,
            "y": 514,
            "speed": 100
        },
        {
            "id": "f6f3c955-6d26-4bae-917b-5c62864f766e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 394,
            "y": 493,
            "speed": 100
        },
        {
            "id": "74564181-c695-4aff-aed3-9b2af6708525",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 394,
            "y": 472,
            "speed": 100
        },
        {
            "id": "980d928b-05ac-42a6-aa35-8eb9ca8ad92e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 394,
            "y": 451,
            "speed": 100
        },
        {
            "id": "7aa652a9-d288-4a17-a170-fc6bbc76f619",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 394,
            "y": 430,
            "speed": 100
        },
        {
            "id": "55ab3ce4-0bf2-4802-b156-8f8dee26712a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 394,
            "y": 409,
            "speed": 100
        },
        {
            "id": "a3cb0b17-2a62-4187-b69f-f7b4026601aa",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 414,
            "y": 398,
            "speed": 100
        },
        {
            "id": "85f80ce7-c2d1-4ff1-9cc2-52b32f240f3c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 433,
            "y": 408,
            "speed": 100
        },
        {
            "id": "899dda0a-973d-4bed-90d5-144bebf147b7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 451,
            "y": 419,
            "speed": 100
        },
        {
            "id": "094dcc58-6a33-4e64-a436-5a82f7d6e911",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 469,
            "y": 429,
            "speed": 100
        },
        {
            "id": "ecb5576c-b67e-4674-a228-513ca3212213",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 487,
            "y": 440,
            "speed": 100
        },
        {
            "id": "ba7fa1dd-2095-4e0e-8c5b-bb2ae7eee7c8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 505,
            "y": 450,
            "speed": 100
        },
        {
            "id": "b53dfbf1-f8a9-4d9a-a8e3-e0b8e73f432e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 523,
            "y": 461,
            "speed": 100
        },
        {
            "id": "b1070ff1-530f-4519-a7c3-1e34f6a295a7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 542,
            "y": 471,
            "speed": 100
        },
        {
            "id": "ed3f55cf-35f8-4c71-9e98-d4fbbabe864e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 567,
            "y": 426,
            "speed": 100
        },
        {
            "id": "13e50130-29e3-470c-bac6-106026a4d9d3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 594,
            "y": 380,
            "speed": 100
        },
        {
            "id": "5c23e260-dea8-40a4-aadb-7d6689ed6f51",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 575,
            "y": 370,
            "speed": 100
        },
        {
            "id": "99e6a6e9-d64e-4045-a7f1-c698568a2787",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 557,
            "y": 359,
            "speed": 100
        },
        {
            "id": "8967669a-a789-4c9a-b1b6-fadfd3b60709",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 539,
            "y": 348,
            "speed": 100
        },
        {
            "id": "270c35f7-19b9-4042-ae1e-f3fa4b59f649",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 521,
            "y": 338,
            "speed": 100
        },
        {
            "id": "aa4fc8e6-5153-4e4f-8180-528bdd3a7759",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 502,
            "y": 328,
            "speed": 100
        },
        {
            "id": "10b4b9ab-e988-420d-a7cd-d82a7f9132dd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 485,
            "y": 318,
            "speed": 100
        },
        {
            "id": "281135aa-2aca-44a6-b45e-79872258513c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 466,
            "y": 307,
            "speed": 100
        },
        {
            "id": "545a89a5-970d-47dc-84d0-17ccb702d301",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 467,
            "y": 286,
            "speed": 100
        },
        {
            "id": "d3c88548-6455-469e-afc6-b18f75d06170",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 485,
            "y": 275,
            "speed": 100
        },
        {
            "id": "f1070ec5-ceae-4181-9ba7-06abfc87a6e0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 503,
            "y": 264,
            "speed": 100
        },
        {
            "id": "79b6aeb7-60f0-4a68-8e7e-3c39c6044903",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 521,
            "y": 254,
            "speed": 100
        },
        {
            "id": "e272d7e7-8929-4b40-993f-0c0ef0baf547",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 539,
            "y": 244,
            "speed": 100
        },
        {
            "id": "bb3016a6-6c49-4198-87ed-64d01e3e29f9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 558,
            "y": 233,
            "speed": 100
        },
        {
            "id": "7cf0a70a-aefa-4613-abe3-2b3b3e50d7e3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 576,
            "y": 223,
            "speed": 100
        },
        {
            "id": "af8e4d4b-d3b8-41ee-90c2-e8905c8ca28e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 594,
            "y": 212,
            "speed": 100
        },
        {
            "id": "a777453a-4ecd-4a16-8fd1-86b8701e67d2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 567,
            "y": 167,
            "speed": 100
        },
        {
            "id": "bf8a8c74-2ebf-41f1-9dd6-07f890e03774",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 541,
            "y": 120,
            "speed": 100
        },
        {
            "id": "8ee3d57f-7c13-43cf-8bd8-305c12049ce6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 523,
            "y": 130,
            "speed": 100
        },
        {
            "id": "056336e1-66d9-4ca4-af44-02266fb6bdbc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 505,
            "y": 141,
            "speed": 100
        },
        {
            "id": "2cb1b6d6-f51d-4f73-a83b-464357ab49b0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 486,
            "y": 151,
            "speed": 100
        },
        {
            "id": "50199176-e29d-417b-bbe4-926fa5d5c7df",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 468,
            "y": 162,
            "speed": 100
        },
        {
            "id": "f7dc583c-07f3-4479-ba8d-c6f9e3bbae38",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 450,
            "y": 172,
            "speed": 100
        },
        {
            "id": "2ab69a69-32b4-4ca6-af0f-f943f345f142",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 432,
            "y": 183,
            "speed": 100
        },
        {
            "id": "46798048-c7c2-44ea-82d6-73817e32f4d8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 414,
            "y": 193,
            "speed": 100
        },
        {
            "id": "ec44b04e-8045-4d9b-aa46-926b9f5eb247",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 392,
            "y": 182,
            "speed": 100
        },
        {
            "id": "ca8a6d79-4137-4e94-b278-e3b809d64b93",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 392,
            "y": 161,
            "speed": 100
        },
        {
            "id": "164491f9-9155-45dc-a9c2-ebcfa5cc26ab",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 392,
            "y": 140,
            "speed": 100
        },
        {
            "id": "ab7c0eea-6890-4155-8e1f-2de3673c5fe7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 392,
            "y": 119,
            "speed": 100
        },
        {
            "id": "881a06c6-e803-4fa2-a7d4-42308b9e9e7c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 392,
            "y": 98,
            "speed": 100
        },
        {
            "id": "111d8c11-770a-45b4-999d-de3b78d2dd9c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 392,
            "y": 77,
            "speed": 100
        },
        {
            "id": "891b08bd-83c8-4079-ac9a-4b4a48a57ffc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 392,
            "y": 56,
            "speed": 100
        },
        {
            "id": "b0a1c679-7e02-4d23-84e4-825c2090b555",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 392,
            "y": 35,
            "speed": 100
        },
        {
            "id": "d934a728-239f-4da7-bfe1-d24f3a0a9f29",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 340,
            "y": 35,
            "speed": 100
        },
        {
            "id": "5dd69b07-1826-4b2f-bb0b-e0ed64538e1d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 289,
            "y": 35,
            "speed": 100
        },
        {
            "id": "eb7f6a95-9011-4af5-8180-60d68982b9d0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 289,
            "y": 56,
            "speed": 100
        },
        {
            "id": "f2a8e621-5bce-407d-818b-7c183e442e40",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 289,
            "y": 77,
            "speed": 100
        },
        {
            "id": "eb25ea68-b42a-4dfd-add3-63d68bfbfc55",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 289,
            "y": 98,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}