{
    "id": "5f584b03-f4ce-4897-ad90-18fa3bceeffe",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_stepsSecond",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "9c46d795-ce11-43da-9b9e-22f4fc69b22a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 305,
            "y": 119,
            "speed": 100
        },
        {
            "id": "9250c9f1-cbbc-4df2-a037-d5baf3d1a6cf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 305,
            "y": 140,
            "speed": 100
        },
        {
            "id": "5c9a94c7-7fd9-4010-8e20-6d21be31d484",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 305,
            "y": 161,
            "speed": 100
        },
        {
            "id": "a320d15f-aac7-4f8d-9025-73d772339cbd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 305,
            "y": 182,
            "speed": 100
        },
        {
            "id": "6539714f-ad67-4bdf-bd03-cd196261b709",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 264,
            "y": 207,
            "speed": 100
        },
        {
            "id": "199f0a80-c107-45cc-9d1c-7851512f316a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 245,
            "y": 197,
            "speed": 100
        },
        {
            "id": "7111da11-dc64-41d5-b734-39865ced50bb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 227,
            "y": 186,
            "speed": 100
        },
        {
            "id": "f7d3ea80-a30f-46bd-80bb-06451f69aa40",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 209,
            "y": 175,
            "speed": 100
        },
        {
            "id": "0c5b0637-ed47-4023-a915-e4aaca4509f1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 191,
            "y": 165,
            "speed": 100
        },
        {
            "id": "64c75c7f-35ae-4351-b9d3-2295e6050de2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 173,
            "y": 155,
            "speed": 100
        },
        {
            "id": "7552003e-587f-4b3e-94dc-a08aa7bd6cd9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 155,
            "y": 144,
            "speed": 100
        },
        {
            "id": "08afbf24-ca85-4b28-91ba-3d5011f2ae9f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 136,
            "y": 134,
            "speed": 100
        },
        {
            "id": "79f9ec28-1fd2-4888-9c71-55e5497c6e2a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 123,
            "y": 156,
            "speed": 100
        },
        {
            "id": "afe04d00-cd56-40e8-9c99-6b9ddfe8349d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 95,
            "y": 202,
            "speed": 100
        },
        {
            "id": "fcbff9b7-5748-46f0-9c78-e50922f5fc55",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 114,
            "y": 212,
            "speed": 100
        },
        {
            "id": "7cc844d4-379d-40db-93ea-4fe7df1c6df3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 132,
            "y": 223,
            "speed": 100
        },
        {
            "id": "b1616fcb-1b62-4095-868f-d5037378b59e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 150,
            "y": 233,
            "speed": 100
        },
        {
            "id": "9ae614ab-c571-4d8a-9671-73f9853a4b3c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 168,
            "y": 244,
            "speed": 100
        },
        {
            "id": "5d8aa21d-8cf6-4f47-9793-b5c87c34279a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 186,
            "y": 254,
            "speed": 100
        },
        {
            "id": "0e18a57b-495d-46d3-91b2-a5120919ce5c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 204,
            "y": 265,
            "speed": 100
        },
        {
            "id": "e9f97334-4cfd-41b4-8442-cdae300ea8f3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 223,
            "y": 276,
            "speed": 100
        },
        {
            "id": "6420300b-4bf9-4e8a-a885-dbccd9c8fcd1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 223,
            "y": 320,
            "speed": 100
        },
        {
            "id": "a6efbc06-0a7e-40ed-996b-e38470b0310d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 205,
            "y": 330,
            "speed": 100
        },
        {
            "id": "2a82f399-1ca4-402d-8e8c-7783ffd7efb0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 186,
            "y": 341,
            "speed": 100
        },
        {
            "id": "2812f972-337a-435c-9fb2-a81dfdd902ba",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 168,
            "y": 351,
            "speed": 100
        },
        {
            "id": "dd1387cd-2ef8-47ce-8e7b-5f95c02eed32",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 150,
            "y": 362,
            "speed": 100
        },
        {
            "id": "0dbae1c6-ce4d-4692-88cf-036945ed3658",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 132,
            "y": 372,
            "speed": 100
        },
        {
            "id": "df98f4a3-7268-45bf-9414-d3e5a37dbf6f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 114,
            "y": 383,
            "speed": 100
        },
        {
            "id": "83246227-4e0e-4b4c-a33f-f64d5a95055f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 96,
            "y": 393,
            "speed": 100
        },
        {
            "id": "73f9db8c-5667-441e-8d30-db71c81bbe48",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 108,
            "y": 415,
            "speed": 100
        },
        {
            "id": "885a304b-98c3-4561-a2a2-60ecd7fc7dbb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 134,
            "y": 459,
            "speed": 100
        },
        {
            "id": "a147c124-433d-4591-bd51-7aff64410d2a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 152,
            "y": 448,
            "speed": 100
        },
        {
            "id": "f10a9c7c-f2ea-4bee-a591-f64402ea5f8e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 171,
            "y": 438,
            "speed": 100
        },
        {
            "id": "3549db25-7bf0-4cf1-b4e6-5f451a1c0f98",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 189,
            "y": 427,
            "speed": 100
        },
        {
            "id": "06a1d043-4bad-4231-a32d-95491d309922",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 207,
            "y": 417,
            "speed": 100
        },
        {
            "id": "716dc341-b965-4802-ab78-ee5b86352c56",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 225,
            "y": 406,
            "speed": 100
        },
        {
            "id": "535a7ed9-cfb0-4ccd-b997-fa40386b5fd7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 243,
            "y": 396,
            "speed": 100
        },
        {
            "id": "17c31239-f2fe-4fbe-8673-11997b3eba2a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 262,
            "y": 385,
            "speed": 100
        },
        {
            "id": "7e942302-e5dd-43dd-bd55-fc8102dc09eb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 303,
            "y": 410,
            "speed": 100
        },
        {
            "id": "680b8885-1fe9-477d-978e-0daa4f0c02db",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 303,
            "y": 431,
            "speed": 100
        },
        {
            "id": "03737264-eeca-4ffc-bce3-fa6ece68e636",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 303,
            "y": 452,
            "speed": 100
        },
        {
            "id": "6e8541e4-98e7-4d77-a1b4-567762264fa7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 303,
            "y": 473,
            "speed": 100
        },
        {
            "id": "705c07c1-adba-443f-b67d-e33419643b1d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 303,
            "y": 494,
            "speed": 100
        },
        {
            "id": "35090aa2-6ad7-434e-bf7c-92bff1054fc4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 303,
            "y": 515,
            "speed": 100
        },
        {
            "id": "fc939730-6775-43a6-8f41-c9a2010b81d3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 303,
            "y": 536,
            "speed": 100
        },
        {
            "id": "4cf426b6-738e-4940-a96e-e80e6bcd8f7f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 303,
            "y": 557,
            "speed": 100
        },
        {
            "id": "268436d1-3058-45f3-9d3f-f1c62904d056",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 326,
            "y": 557,
            "speed": 100
        },
        {
            "id": "87413b41-3bce-4283-99bd-244305004157",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 380,
            "y": 556,
            "speed": 100
        },
        {
            "id": "b078a5ce-839a-448c-a4c8-0577f81fac8d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 380,
            "y": 535,
            "speed": 100
        },
        {
            "id": "3e3ed0e6-e4e9-4491-a0a1-def60576deb9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 380,
            "y": 514,
            "speed": 100
        },
        {
            "id": "c9edad37-cc6f-4cdf-86fe-c9ee8ac9b3c4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 380,
            "y": 493,
            "speed": 100
        },
        {
            "id": "23de26fe-1a93-4f5b-8138-419acad3ed67",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 380,
            "y": 472,
            "speed": 100
        },
        {
            "id": "39de3f1e-b73c-4207-a52c-d88936c30774",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 380,
            "y": 451,
            "speed": 100
        },
        {
            "id": "2f885dbe-a2fe-4d91-b84a-b9d070318685",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 380,
            "y": 430,
            "speed": 100
        },
        {
            "id": "538f59a6-32ed-41b7-a481-894f3973b281",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 380,
            "y": 409,
            "speed": 100
        },
        {
            "id": "0bc44de1-111f-4028-9218-5fcde49390d9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 421,
            "y": 385,
            "speed": 100
        },
        {
            "id": "5b772e85-ff03-4b3a-8d8b-20bc6ed4ae49",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 440,
            "y": 395,
            "speed": 100
        },
        {
            "id": "6f16bc14-1cb3-436f-b90a-d4b03349f019",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 458,
            "y": 406,
            "speed": 100
        },
        {
            "id": "89723896-bbc2-47a7-8084-a09c82d63429",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 476,
            "y": 416,
            "speed": 100
        },
        {
            "id": "7ffefbaf-5335-487e-8c05-c2051e7f191e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 494,
            "y": 427,
            "speed": 100
        },
        {
            "id": "847be939-d61a-4a05-a499-0c120078daf0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 437,
            "speed": 100
        },
        {
            "id": "c7c05082-9384-4d01-88fe-4debc36f120d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 530,
            "y": 448,
            "speed": 100
        },
        {
            "id": "1d63c90a-5527-40de-a8fa-2f070672bc16",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 549,
            "y": 458,
            "speed": 100
        },
        {
            "id": "479e218c-2116-4f96-a1d6-bbb8f38cdb08",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 562,
            "y": 437,
            "speed": 100
        },
        {
            "id": "0662edde-f2e9-4e13-a845-eaea072d0ef2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 589,
            "y": 391,
            "speed": 100
        },
        {
            "id": "d4dfa511-2fb3-4dd6-be2e-ef7c399c345a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 570,
            "y": 381,
            "speed": 100
        },
        {
            "id": "61cfe709-5d85-4d4d-bddc-db9b29e09c69",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 552,
            "y": 370,
            "speed": 100
        },
        {
            "id": "5040cf50-a216-4785-ba03-b064aad969b2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 534,
            "y": 359,
            "speed": 100
        },
        {
            "id": "a0815fb3-5715-4332-ac31-828df7d1c897",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 516,
            "y": 349,
            "speed": 100
        },
        {
            "id": "7098f182-a919-4bb5-9d69-51b994950480",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 497,
            "y": 339,
            "speed": 100
        },
        {
            "id": "b67b6f8f-2671-48ab-b0b8-0efee41caf76",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 480,
            "y": 329,
            "speed": 100
        },
        {
            "id": "7af88267-77db-4bc8-8c09-c99ae8ac3b85",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 461,
            "y": 318,
            "speed": 100
        },
        {
            "id": "8da3ac6e-7429-4a2f-84a5-5a80d7e7f732",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 460,
            "y": 277,
            "speed": 100
        },
        {
            "id": "407daaa4-b156-44e6-af3e-f541d0a1082b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 478,
            "y": 266,
            "speed": 100
        },
        {
            "id": "6b78020a-d777-450e-9d01-437f08ae258c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 496,
            "y": 255,
            "speed": 100
        },
        {
            "id": "3f532b09-81d3-4d15-ae29-851db2d9767e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 514,
            "y": 245,
            "speed": 100
        },
        {
            "id": "85e75f6d-8705-4484-a771-114d911e1ca2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 532,
            "y": 235,
            "speed": 100
        },
        {
            "id": "4a7d1e83-b39d-4e2c-83ee-c97657b940ab",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 551,
            "y": 224,
            "speed": 100
        },
        {
            "id": "cd4f7891-81ca-460b-8b0a-32bc3b49d29f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 569,
            "y": 214,
            "speed": 100
        },
        {
            "id": "ef0b4c4c-dbe0-4ee3-bede-b6c478c32e12",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 587,
            "y": 203,
            "speed": 100
        },
        {
            "id": "cfca8667-9817-4fbe-b1dc-a2a3231e6d2f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 574,
            "y": 180,
            "speed": 100
        },
        {
            "id": "41d3e39e-6609-49ea-b1ae-42488bc80d99",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 548,
            "y": 133,
            "speed": 100
        },
        {
            "id": "6e94c0ae-5a44-4867-b2be-dad72b16da88",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 530,
            "y": 143,
            "speed": 100
        },
        {
            "id": "3aa3c051-0dd9-481b-a6e1-798a36e3b149",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 512,
            "y": 154,
            "speed": 100
        },
        {
            "id": "6119ca4f-5236-41ec-b38d-d544c674f5f3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 493,
            "y": 164,
            "speed": 100
        },
        {
            "id": "deacb3c1-0234-4b0c-be20-76b934763f74",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 475,
            "y": 175,
            "speed": 100
        },
        {
            "id": "89e4cae6-3192-41b3-ae81-cd2cb9b86e8d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 457,
            "y": 185,
            "speed": 100
        },
        {
            "id": "f3e1e5a9-2144-47de-8709-5906357190fa",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 439,
            "y": 196,
            "speed": 100
        },
        {
            "id": "a70cd596-d070-4404-b9f6-588c262f1edc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 421,
            "y": 206,
            "speed": 100
        },
        {
            "id": "693c9dbc-8339-46a8-b6cc-a75fead9f1a5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 382,
            "y": 182,
            "speed": 100
        },
        {
            "id": "48840c63-39d5-46eb-b3ea-6d618673eefe",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 382,
            "y": 161,
            "speed": 100
        },
        {
            "id": "bd9ec5d5-01b1-436f-9ce8-d6e7e3c6a749",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 382,
            "y": 140,
            "speed": 100
        },
        {
            "id": "d02fefd1-8019-4fe8-99ae-731087207736",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 382,
            "y": 119,
            "speed": 100
        },
        {
            "id": "b13450ea-0db1-4df9-8524-4a12718aee43",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 382,
            "y": 98,
            "speed": 100
        },
        {
            "id": "24d4fc03-a614-41b6-8f5b-b84c177287ec",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 382,
            "y": 77,
            "speed": 100
        },
        {
            "id": "5c3a7c05-9d65-473f-9f61-59e55357f6e4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 382,
            "y": 56,
            "speed": 100
        },
        {
            "id": "b8e93a19-25e7-4d50-86c8-8dffda6b7cdd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 382,
            "y": 35,
            "speed": 100
        },
        {
            "id": "decb84cc-8fa3-4bc5-9abc-183f8223d908",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 350,
            "y": 35,
            "speed": 100
        },
        {
            "id": "fc787d2a-14fa-44d7-9931-3c04f6ab31a2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 305,
            "y": 35,
            "speed": 100
        },
        {
            "id": "b1f1cdb4-962c-4457-a448-22e5d0346430",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 305,
            "y": 56,
            "speed": 100
        },
        {
            "id": "6122e569-48b8-4ae2-b446-9258c9de6e34",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 305,
            "y": 77,
            "speed": 100
        },
        {
            "id": "004ec850-e196-4a42-a034-f1f1bc4c3455",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 305,
            "y": 98,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}