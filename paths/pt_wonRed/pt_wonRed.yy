{
    "id": "006d9b9e-71c8-42b6-a6d8-fcb230587971",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonRed",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "7947e299-36f8-4729-8347-17a9fd75c399",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 341,
            "y": 34,
            "speed": 100
        },
        {
            "id": "a46f8b42-28c0-4707-86b3-4f0fbab9acf0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 341,
            "y": 56,
            "speed": 100
        },
        {
            "id": "69b0b150-d4f6-4585-b877-28f7d05e103e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 341,
            "y": 78,
            "speed": 100
        },
        {
            "id": "1b750d1e-61c8-456c-871c-95b8b69d3cf3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 341,
            "y": 99,
            "speed": 100
        },
        {
            "id": "65743d52-d968-4a20-b55b-47215f63e2a0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 341,
            "y": 120,
            "speed": 100
        },
        {
            "id": "17172b36-a916-4d46-a38d-3b57098801ab",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 341,
            "y": 141,
            "speed": 100
        },
        {
            "id": "4bed3dba-39ac-44b1-bb36-a72a96312e0a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 341,
            "y": 162,
            "speed": 100
        },
        {
            "id": "266a9648-401e-4327-889e-11c99fc16241",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 341,
            "y": 183,
            "speed": 100
        },
        {
            "id": "44eef383-d39e-40c9-87fa-4a4752662370",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 341,
            "y": 206,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}