{
    "id": "dad280da-4a22-4b54-ba37-79d5a90fc962",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonBlue",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "9f8dfba6-9d6d-4ebb-aa55-fc0fbfdbbe77",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 113,
            "y": 163,
            "speed": 100
        },
        {
            "id": "feef97c4-832f-4bd7-9a7f-4d391431a996",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 133,
            "y": 175,
            "speed": 100
        },
        {
            "id": "5114e335-e01c-4c69-b807-57026484868e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 152,
            "y": 187,
            "speed": 100
        },
        {
            "id": "6aeff58d-7cdc-4908-8c3f-0c454de1247b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 170,
            "y": 197,
            "speed": 100
        },
        {
            "id": "9b73496e-bc01-4434-ac39-861bbae3e5ae",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 188,
            "y": 208,
            "speed": 100
        },
        {
            "id": "986c559b-3316-4545-b1bc-bba79486004d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 206,
            "y": 218,
            "speed": 100
        },
        {
            "id": "7c15edd9-af8d-43af-9f63-4afef85a6652",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 224,
            "y": 229,
            "speed": 100
        },
        {
            "id": "0ebe7092-974d-46f6-b864-8c3f0b9595ba",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 243,
            "y": 240,
            "speed": 100
        },
        {
            "id": "70e05f6b-8377-43aa-8363-655d8c1c52fd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 265,
            "y": 254,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}