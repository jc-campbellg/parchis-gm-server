{
    "id": "3f065d9b-c870-429a-9ecb-cb78fd71e48a",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonPurpleSecond",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "29b741da-737c-442d-b962-b414c077f1d6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 562,
            "y": 439,
            "speed": 100
        },
        {
            "id": "46d384ae-2a04-47dc-afe6-1a96f540c822",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 543,
            "y": 428,
            "speed": 100
        },
        {
            "id": "1fc107ec-0ad6-4026-b3f5-4ad9d4facbf8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 526,
            "y": 417,
            "speed": 100
        },
        {
            "id": "ac2f6fe1-e50c-47dc-9d9b-5c311f4c962b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 508,
            "y": 406,
            "speed": 100
        },
        {
            "id": "c66ca40d-2110-494a-a7fb-0bff1812e31c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 490,
            "y": 396,
            "speed": 100
        },
        {
            "id": "6f40b266-77f3-4c24-86fb-9eacab85aae8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 471,
            "y": 386,
            "speed": 100
        },
        {
            "id": "951defd8-8dd5-4caa-b956-3ecba44018af",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 454,
            "y": 376,
            "speed": 100
        },
        {
            "id": "6da6a3a1-78da-4e79-8807-f5b9a233017b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 435,
            "y": 365,
            "speed": 100
        },
        {
            "id": "9e6bee3c-c71d-43f3-b72c-2ed67e30c392",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 412,
            "y": 352,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}