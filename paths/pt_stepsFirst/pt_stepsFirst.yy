{
    "id": "d6d2aa0a-9e84-4c2f-9ea4-4e15b5f17c73",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_stepsFirst",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "f942f6fa-6f14-4a56-b472-20ffe19742c3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 276,
            "y": 119,
            "speed": 100
        },
        {
            "id": "5f3b8b4f-c075-4989-b4ad-d6d67d685bf9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 276,
            "y": 140,
            "speed": 100
        },
        {
            "id": "e9448fa0-244f-44b1-8fc1-2ed651390c3c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 276,
            "y": 160,
            "speed": 100
        },
        {
            "id": "2920b0be-0aca-419b-a031-fd8bbe3af8a9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 289,
            "y": 182,
            "speed": 100
        },
        {
            "id": "395c3b42-eeaf-4bd4-9dce-e0c353a139ec",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 269,
            "y": 195,
            "speed": 100
        },
        {
            "id": "ff7159f3-1af7-4046-baf7-82df2034efd4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 256,
            "y": 173,
            "speed": 100
        },
        {
            "id": "144c33d7-d9ce-416a-a83b-cce1c8672db1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 238,
            "y": 162,
            "speed": 100
        },
        {
            "id": "d422f539-9782-4998-8f91-dbda30496d2a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 220,
            "y": 151,
            "speed": 100
        },
        {
            "id": "c7110456-a5bd-434c-a8c1-425420d66780",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 202,
            "y": 141,
            "speed": 100
        },
        {
            "id": "1052b079-2a8b-49bb-8b63-e7466f90e536",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 184,
            "y": 131,
            "speed": 100
        },
        {
            "id": "3795aef7-953c-4125-a556-1c9e78f50e0d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 166,
            "y": 120,
            "speed": 100
        },
        {
            "id": "4c35d997-cbf3-4a44-be32-b44a645efba1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 147,
            "y": 110,
            "speed": 100
        },
        {
            "id": "3090f3b9-c42d-46dc-bb8a-822cb72c6063",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 109,
            "y": 176,
            "speed": 100
        },
        {
            "id": "c4c9df99-4db3-4714-99ec-fbead79e4627",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 80,
            "y": 225,
            "speed": 100
        },
        {
            "id": "bcea9329-d134-4e90-ad1c-b998910293e2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 99,
            "y": 235,
            "speed": 100
        },
        {
            "id": "9e33b361-3377-410b-9bd6-ccaca860d94e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 117,
            "y": 246,
            "speed": 100
        },
        {
            "id": "e2ceb046-ffe0-434b-91e6-acd5c96d958d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 135,
            "y": 256,
            "speed": 100
        },
        {
            "id": "38c9749e-203d-49cb-b90b-f7855dba668a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 153,
            "y": 267,
            "speed": 100
        },
        {
            "id": "df51ed11-ed5b-43f5-bf76-f502489f1889",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 171,
            "y": 277,
            "speed": 100
        },
        {
            "id": "8b97a7ec-e074-4b2f-b055-0ed988187104",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 189,
            "y": 288,
            "speed": 100
        },
        {
            "id": "7af4c4d7-9cc8-4c56-bc1f-5282ec56522a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 217,
            "y": 286,
            "speed": 100
        },
        {
            "id": "a33a1242-ee58-48ab-b18a-cff2197ac34f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 216,
            "y": 307,
            "speed": 100
        },
        {
            "id": "545ce7cd-b209-4629-b189-d80acdf22dbd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 191,
            "y": 308,
            "speed": 100
        },
        {
            "id": "556ed5a6-8194-4d83-b284-397c572a25af",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 172,
            "y": 319,
            "speed": 100
        },
        {
            "id": "cc817e7c-3ea6-42b3-a8bf-5e07d464b463",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 154,
            "y": 329,
            "speed": 100
        },
        {
            "id": "0e16bb31-985a-44bd-8a1b-e493399c3e29",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 136,
            "y": 340,
            "speed": 100
        },
        {
            "id": "a8fc8637-8eef-40c2-b839-8aa9a2dfd027",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 118,
            "y": 350,
            "speed": 100
        },
        {
            "id": "e8545a2d-5479-42e3-acfe-3328072eabde",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 100,
            "y": 361,
            "speed": 100
        },
        {
            "id": "35391d94-2347-41db-b337-d75bd3aa3cba",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 82,
            "y": 371,
            "speed": 100
        },
        {
            "id": "0fa5fe09-6262-426d-a229-4d591bc73034",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 120,
            "y": 435,
            "speed": 100
        },
        {
            "id": "60fb8d3d-d1dd-4e52-b598-16ffc75b25db",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 148,
            "y": 484,
            "speed": 100
        },
        {
            "id": "0c022d5f-87a9-4d34-a4fe-28fc36c31027",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 166,
            "y": 473,
            "speed": 100
        },
        {
            "id": "212c867c-36cf-4d8e-b503-36b01920dab4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 185,
            "y": 463,
            "speed": 100
        },
        {
            "id": "e55f7946-5d82-4213-acdf-c23408e11d7d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 203,
            "y": 452,
            "speed": 100
        },
        {
            "id": "3e6a4af7-3f6f-4bcc-87ee-d02c841f4233",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 221,
            "y": 442,
            "speed": 100
        },
        {
            "id": "18da762e-ef44-42e1-9d70-faac99712002",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 239,
            "y": 431,
            "speed": 100
        },
        {
            "id": "24243d44-2b3a-47e4-b2a9-92e788fea738",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 257,
            "y": 421,
            "speed": 100
        },
        {
            "id": "d86f813f-cae4-48ba-bb28-b12cd7b7b096",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 269,
            "y": 397,
            "speed": 100
        },
        {
            "id": "66b0ec9d-9bed-4c6b-812f-7bbf31645a3f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 290,
            "y": 410,
            "speed": 100
        },
        {
            "id": "cc7c6fd5-3878-4709-9a97-3fb95677a8cc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 275,
            "y": 432,
            "speed": 100
        },
        {
            "id": "c1acd692-fd04-4d92-ad35-abccab0adc1b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 275,
            "y": 453,
            "speed": 100
        },
        {
            "id": "c293bb83-8b3b-4733-b741-37dabda01253",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 275,
            "y": 474,
            "speed": 100
        },
        {
            "id": "81f35e59-1a11-4ce5-8387-d31f5fd0f0e9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 275,
            "y": 495,
            "speed": 100
        },
        {
            "id": "03067fb5-e39d-41b5-b748-17660be88c87",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 275,
            "y": 516,
            "speed": 100
        },
        {
            "id": "c19cd2b1-ef5d-46f4-bfb7-704f258da92e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 275,
            "y": 537,
            "speed": 100
        },
        {
            "id": "3a33fe64-def2-4982-9ddf-5a97d990dfa7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 275,
            "y": 558,
            "speed": 100
        },
        {
            "id": "4276b4bd-c90f-4dd1-9af6-d1ce397732e5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 354,
            "y": 556,
            "speed": 100
        },
        {
            "id": "9117e07d-b673-49bb-8cc4-56f35d2c70c5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 409,
            "y": 556,
            "speed": 100
        },
        {
            "id": "6234d243-c63e-4801-a53f-9528da65b4f9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 409,
            "y": 535,
            "speed": 100
        },
        {
            "id": "d31ace6d-8032-4f20-ab3a-5ce3feea3938",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 409,
            "y": 514,
            "speed": 100
        },
        {
            "id": "55d91e52-7e02-40ea-a6b3-627eae47bd12",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 409,
            "y": 493,
            "speed": 100
        },
        {
            "id": "c70b70f4-afdd-488f-9bec-494fb243f62c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 409,
            "y": 472,
            "speed": 100
        },
        {
            "id": "df378921-7bb6-4462-a2bd-b82d5d9bf3aa",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 409,
            "y": 451,
            "speed": 100
        },
        {
            "id": "e7e0f165-81de-41fc-81c2-647112e21c1b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 409,
            "y": 430,
            "speed": 100
        },
        {
            "id": "607604db-2adb-4b02-b19d-dc0e209a42cf",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 394,
            "y": 409,
            "speed": 100
        },
        {
            "id": "98b37a7f-51d5-459f-9a5a-fd77306aea4f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 414,
            "y": 398,
            "speed": 100
        },
        {
            "id": "b5c41f30-4f3a-4711-83ad-ff8c9905d5a1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 425,
            "y": 423,
            "speed": 100
        },
        {
            "id": "1199b4f0-25fa-4990-a543-a9611188e0c9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 443,
            "y": 434,
            "speed": 100
        },
        {
            "id": "17aebced-078e-4a73-8ee7-4d79b58893b2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 461,
            "y": 444,
            "speed": 100
        },
        {
            "id": "8f67dd60-47c0-4f40-b588-5e8ca1434650",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 479,
            "y": 455,
            "speed": 100
        },
        {
            "id": "5ad1edf6-6e58-41aa-ae6b-5760bf676363",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 497,
            "y": 465,
            "speed": 100
        },
        {
            "id": "4a61b6a3-42a9-4a62-aea7-87c31ab5bb2e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 515,
            "y": 476,
            "speed": 100
        },
        {
            "id": "65a5c96e-d3a8-4853-a0e3-b5947362e218",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 534,
            "y": 486,
            "speed": 100
        },
        {
            "id": "b11a2c51-9a0f-4444-84e0-f83c773f18fe",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 573,
            "y": 417,
            "speed": 100
        },
        {
            "id": "e304fbea-5aa2-4e45-a6d5-0e9f43fa14dc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 601,
            "y": 368,
            "speed": 100
        },
        {
            "id": "4341653d-6081-4601-b831-ecf6a89bb37f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 582,
            "y": 358,
            "speed": 100
        },
        {
            "id": "27bcec2b-aab0-4604-a246-80bfe2b765ce",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 564,
            "y": 347,
            "speed": 100
        },
        {
            "id": "e0a9d670-1112-4da5-941a-5a4da7e75cac",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 546,
            "y": 336,
            "speed": 100
        },
        {
            "id": "718d5586-220b-45ed-b1b7-76aff389a8f2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 528,
            "y": 326,
            "speed": 100
        },
        {
            "id": "1cca363d-340b-4033-93d2-58876ea6be6e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 509,
            "y": 316,
            "speed": 100
        },
        {
            "id": "e950b3c8-85ed-4ca8-b26e-374b60671e05",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 492,
            "y": 306,
            "speed": 100
        },
        {
            "id": "5ebe3de4-44e2-4af4-bbde-e0a0c891f4c6",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 466,
            "y": 307,
            "speed": 100
        },
        {
            "id": "99441fdd-f7d7-492b-8739-b260d9204ca8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 467,
            "y": 286,
            "speed": 100
        },
        {
            "id": "835186ea-53bf-4fa8-bf71-405d5533fb41",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 492,
            "y": 290,
            "speed": 100
        },
        {
            "id": "87f4a0d8-8bca-46bb-8996-bb98ab823a6b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 510,
            "y": 279,
            "speed": 100
        },
        {
            "id": "a6093d81-9352-4556-95be-524cbd40a2a2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 528,
            "y": 269,
            "speed": 100
        },
        {
            "id": "f9a4fabd-3bd7-4696-9943-4ccd0b5db6f1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 546,
            "y": 259,
            "speed": 100
        },
        {
            "id": "afdcdffd-b325-4cdd-82a0-cb548ee60ac8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 565,
            "y": 248,
            "speed": 100
        },
        {
            "id": "f724d5a5-120d-4e55-b92f-f8f6828e4e49",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 583,
            "y": 238,
            "speed": 100
        },
        {
            "id": "20d23e9a-4efb-4804-bfea-a0d3df0a8abc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 601,
            "y": 227,
            "speed": 100
        },
        {
            "id": "67bf352c-b7cc-4e2c-b616-81070bd6bd64",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 560,
            "y": 157,
            "speed": 100
        },
        {
            "id": "8e1a839e-5c0f-4826-8551-fed4b847d9d0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 533,
            "y": 111,
            "speed": 100
        },
        {
            "id": "35112411-fe76-4c50-a4fd-f888382ee912",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 515,
            "y": 121,
            "speed": 100
        },
        {
            "id": "950dc141-8a73-4fbc-88ae-335cb41664c1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 497,
            "y": 132,
            "speed": 100
        },
        {
            "id": "55a21609-dd85-4722-a588-33f7aa3a0bb0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 478,
            "y": 142,
            "speed": 100
        },
        {
            "id": "77dfe84d-9804-4550-81a5-2ca5f0960650",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 460,
            "y": 153,
            "speed": 100
        },
        {
            "id": "dbcf2def-f4a1-4da1-9f42-b9c343366b8c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 442,
            "y": 163,
            "speed": 100
        },
        {
            "id": "9dc7a280-c2cb-4e0e-89bb-5b838d4c2aca",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 424,
            "y": 174,
            "speed": 100
        },
        {
            "id": "1794b572-01c6-40d5-9625-37d5d3aa8d38",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 414,
            "y": 193,
            "speed": 100
        },
        {
            "id": "dd375841-5e9c-4420-879b-b1e34ee8c394",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 392,
            "y": 182,
            "speed": 100
        },
        {
            "id": "f97e553c-f8e7-4430-8c71-06cfb2620cb8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 405,
            "y": 161,
            "speed": 100
        },
        {
            "id": "ff0bbe6e-b5b9-41a8-aef1-f92d049bf3ce",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 405,
            "y": 140,
            "speed": 100
        },
        {
            "id": "59bc51b1-b3c4-4999-bf69-f8430b28d9de",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 405,
            "y": 119,
            "speed": 100
        },
        {
            "id": "281eb006-8b25-403e-8fa0-92af47f847b4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 405,
            "y": 98,
            "speed": 100
        },
        {
            "id": "577f081c-f5f9-4b67-9a4f-ca301f44be08",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 405,
            "y": 77,
            "speed": 100
        },
        {
            "id": "de19542a-5d30-4240-ae27-5008dc1a193d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 405,
            "y": 56,
            "speed": 100
        },
        {
            "id": "e7246d9b-3ab9-453d-a93a-b6f4d93f612e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 405,
            "y": 35,
            "speed": 100
        },
        {
            "id": "f6eec0f1-c259-4b74-8e82-ae4d81f1773e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 327,
            "y": 35,
            "speed": 100
        },
        {
            "id": "497ac837-96af-476f-b939-fa57686f7b92",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 276,
            "y": 35,
            "speed": 100
        },
        {
            "id": "cc8d40a0-3ed8-47e8-993e-4e76a9792cb2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 276,
            "y": 56,
            "speed": 100
        },
        {
            "id": "08accbfa-6471-47ed-814e-7b32ab065209",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 276,
            "y": 77,
            "speed": 100
        },
        {
            "id": "d3ff16b0-d380-4ae9-ac6f-84a3a5394984",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 276,
            "y": 98,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}