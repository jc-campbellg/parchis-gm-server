{
    "id": "49160b62-0c01-47e7-a1c5-6da2b294ea91",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_spawn",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "3bb9bc5c-70ee-461b-8eb8-dda57df45c67",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 250,
            "y": 55,
            "speed": 100
        },
        {
            "id": "93948ca2-edcc-4def-96c0-183e72b1a726",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 248,
            "y": 136,
            "speed": 100
        },
        {
            "id": "fb39efde-9582-4a9b-b5bf-da7163730c10",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 180,
            "y": 96,
            "speed": 100
        },
        {
            "id": "f1e1e99a-d3be-4554-b539-603c213c2ab8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 231,
            "y": 96,
            "speed": 100
        },
        {
            "id": "984bd042-f5de-4dc9-8a8e-b699bc87d804",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 88,
            "y": 256,
            "speed": 100
        },
        {
            "id": "362727c3-1137-487b-bc8f-82fa5a2d69f5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 150,
            "y": 296,
            "speed": 100
        },
        {
            "id": "d52d88f8-0dc2-4a3b-806e-70a74190e7f2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 88,
            "y": 335,
            "speed": 100
        },
        {
            "id": "39f138aa-08bd-406f-8e5f-bba50708f972",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 102,
            "y": 297,
            "speed": 100
        },
        {
            "id": "28bfa32d-284d-484b-b941-7ca944e3ae0a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 250,
            "y": 457,
            "speed": 100
        },
        {
            "id": "784cc5ac-439d-49fe-80e5-cc4a49706bb1",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 250,
            "y": 536,
            "speed": 100
        },
        {
            "id": "6057e8cb-3305-4cc2-8427-4f0d05bffd53",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 180,
            "y": 496,
            "speed": 100
        },
        {
            "id": "918195b7-cc95-416c-90e9-6d5998a5222d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 227,
            "y": 492,
            "speed": 100
        },
        {
            "id": "a50fd274-6195-4fe3-9a67-f958098d4e85",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 432,
            "y": 454,
            "speed": 100
        },
        {
            "id": "991d8352-446b-4c5b-828d-561bdd9cf44f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 504,
            "y": 495,
            "speed": 100
        },
        {
            "id": "e9b1e063-07e0-4d6e-be21-cc796a96f25e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 435,
            "y": 535,
            "speed": 100
        },
        {
            "id": "7d57301f-c724-4084-8308-67ae50c3141f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 452,
            "y": 495,
            "speed": 100
        },
        {
            "id": "fbc8a7ac-9bef-4331-a659-77f90d584484",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 526,
            "y": 296,
            "speed": 100
        },
        {
            "id": "44ffacd1-da00-4e0c-ab5f-435718f03386",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 597,
            "y": 257,
            "speed": 100
        },
        {
            "id": "52e1b074-082c-481b-a132-59d845a5bbb8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 596,
            "y": 332,
            "speed": 100
        },
        {
            "id": "01335864-49a9-4ae1-8375-7f34ab54ab11",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 565,
            "y": 298,
            "speed": 100
        },
        {
            "id": "71a0d63e-ffa5-4147-ba03-f6a2931f0fbc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 432,
            "y": 136,
            "speed": 100
        },
        {
            "id": "590e2888-e399-4ec8-8fc3-5e71b1b52229",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 432,
            "y": 53,
            "speed": 100
        },
        {
            "id": "9804a2e5-be95-43d3-bcdf-62bbe005219c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 504,
            "y": 95,
            "speed": 100
        },
        {
            "id": "e772f3e2-dda2-4373-b516-94d8d3854548",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 450,
            "y": 101,
            "speed": 100
        }
    ],
    "precision": 1,
    "vsnap": 0
}