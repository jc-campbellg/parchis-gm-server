{
    "id": "4d222610-e591-4fc5-a108-b82863dba1d2",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonGreen",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "59999066-e2d0-4002-9035-09f5ecdf8974",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 342,
            "y": 559,
            "speed": 100
        },
        {
            "id": "ed8b57fb-5bb8-42e4-838e-4bc9c94f398c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 342,
            "y": 535,
            "speed": 100
        },
        {
            "id": "16f03481-94f0-4220-99c1-492f02236c92",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 342,
            "y": 513,
            "speed": 100
        },
        {
            "id": "fd0413f5-523b-45e0-a6d1-c457d94cdaa0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 342,
            "y": 492,
            "speed": 100
        },
        {
            "id": "a72cc883-af73-46e9-b7a1-2b93ddd29ead",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 342,
            "y": 471,
            "speed": 100
        },
        {
            "id": "8fda4927-802d-4015-b0ec-65fe3e280f64",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 342,
            "y": 450,
            "speed": 100
        },
        {
            "id": "12b5a478-dc18-4acd-9a25-33693abccff7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 342,
            "y": 429,
            "speed": 100
        },
        {
            "id": "8914fa2b-a6ee-4faf-bc3e-e9e4ea2af3f7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 342,
            "y": 408,
            "speed": 100
        },
        {
            "id": "03a02584-f44d-45ff-b03c-67465827c54e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 342,
            "y": 382,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}