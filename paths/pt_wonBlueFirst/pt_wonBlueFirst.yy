{
    "id": "f6729e3c-6659-444a-949d-6c9de625ad74",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonBlueFirst",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "0329e684-45b2-42ca-81da-1bf4f001bb8d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 108,
            "y": 177,
            "speed": 100
        },
        {
            "id": "c9f1c3c9-3da8-4f73-9384-a15e5121e059",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 127,
            "y": 188,
            "speed": 100
        },
        {
            "id": "2f56d5d5-244d-43da-a7ec-a6e258902c49",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 145,
            "y": 199,
            "speed": 100
        },
        {
            "id": "4cbccfc1-c8fd-48d9-947e-6ecdf40ede4a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 163,
            "y": 209,
            "speed": 100
        },
        {
            "id": "157e6dcb-1f87-4497-9087-c0c2aab95354",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 181,
            "y": 220,
            "speed": 100
        },
        {
            "id": "a3ea6e9c-b8e0-4253-a853-f75fb6495a28",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 199,
            "y": 230,
            "speed": 100
        },
        {
            "id": "7e907e2b-6337-48d0-b087-b1fc48029456",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 217,
            "y": 241,
            "speed": 100
        },
        {
            "id": "a2db4a93-883f-4792-9ad2-fec26adccf6c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 236,
            "y": 252,
            "speed": 100
        },
        {
            "id": "24473cde-66a6-4099-b710-dcdc672b8a02",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 254,
            "y": 265,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}