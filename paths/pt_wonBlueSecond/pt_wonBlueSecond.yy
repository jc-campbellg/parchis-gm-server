{
    "id": "097bc79e-a77d-4f2b-8dbc-8f316d88f83e",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonBlueSecond",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "769d0269-450c-425f-98bf-9430fd5223ea",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 120,
            "y": 151,
            "speed": 100
        },
        {
            "id": "ce74aafd-cfa2-4005-9a82-588e83774045",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 140,
            "y": 163,
            "speed": 100
        },
        {
            "id": "a2373789-b094-405f-93cf-eba18cfe794b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 160,
            "y": 175,
            "speed": 100
        },
        {
            "id": "c43a7874-1ce1-4e28-a4c2-fa1f0818360c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 178,
            "y": 185,
            "speed": 100
        },
        {
            "id": "bac9e093-00ac-4642-a947-875279ade524",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 196,
            "y": 196,
            "speed": 100
        },
        {
            "id": "bcd1fc57-c633-4fda-a01e-6af3410449b4",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 214,
            "y": 206,
            "speed": 100
        },
        {
            "id": "b4b5ebe6-3669-4dd7-a97f-2309cd8fe623",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 232,
            "y": 217,
            "speed": 100
        },
        {
            "id": "616d8f4b-8aaf-465b-a915-e683b5ac3a29",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 251,
            "y": 228,
            "speed": 100
        },
        {
            "id": "2f9b55a4-3f8f-4b1d-81fb-7f74d34b094e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 269,
            "y": 239,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}