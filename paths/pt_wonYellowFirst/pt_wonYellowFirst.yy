{
    "id": "eace6039-9e1d-42b0-b456-be17cc1c89c4",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonYellowFirst",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "30a17115-8f79-4f0b-bc7b-c3d92985c001",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 122,
            "y": 439,
            "speed": 100
        },
        {
            "id": "9a1ad870-84b4-491e-be28-6eb1d8cf5571",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 139,
            "y": 429,
            "speed": 100
        },
        {
            "id": "e4031de4-9ac1-42a0-b18e-27b19ad523ec",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 159,
            "y": 418,
            "speed": 100
        },
        {
            "id": "b0adb87f-6c18-4b87-b53f-a3da455f42ea",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 177,
            "y": 407,
            "speed": 100
        },
        {
            "id": "05583e00-241f-4fa6-b36a-ac9825005a52",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 195,
            "y": 397,
            "speed": 100
        },
        {
            "id": "20ba7c36-72ef-45be-933b-87a60aa549d3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 213,
            "y": 386,
            "speed": 100
        },
        {
            "id": "1063e900-2eb4-401e-8b7f-a94aa7edf2f8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 231,
            "y": 376,
            "speed": 100
        },
        {
            "id": "06c68960-8224-4dc7-ab1e-d1eeb7d5bbe0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 250,
            "y": 365,
            "speed": 100
        },
        {
            "id": "51edc81d-0899-492d-8818-a3758050f299",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 270,
            "y": 354,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}