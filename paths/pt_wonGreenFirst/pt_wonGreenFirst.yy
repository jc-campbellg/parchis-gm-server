{
    "id": "c25228be-ccfc-4752-8c65-88ccdf20a4a1",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonGreenFirst",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "7e987a15-b1df-4eff-b91d-4153234eca90",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 357,
            "y": 559,
            "speed": 100
        },
        {
            "id": "ad210600-6f3a-4eb4-be93-ffd8b2a8fcaa",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 357,
            "y": 537,
            "speed": 100
        },
        {
            "id": "91fb7586-9f51-4d06-8fbe-4f889a4dd887",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 357,
            "y": 513,
            "speed": 100
        },
        {
            "id": "cf519b94-29f7-4abc-8247-062e650c743e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 357,
            "y": 492,
            "speed": 100
        },
        {
            "id": "fe15435e-3176-4ad5-bf8d-f82eb2a8a571",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 357,
            "y": 471,
            "speed": 100
        },
        {
            "id": "1b1b0467-2285-425b-b3a4-a98e10ff3b5c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 357,
            "y": 450,
            "speed": 100
        },
        {
            "id": "a1b4af9a-9d1b-4d49-8407-3da5eb872984",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 357,
            "y": 429,
            "speed": 100
        },
        {
            "id": "ec788090-62dd-4831-91f2-92461dc403fc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 357,
            "y": 408,
            "speed": 100
        },
        {
            "id": "2897f803-f33f-4636-969a-0e97884224ff",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 357,
            "y": 383,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}