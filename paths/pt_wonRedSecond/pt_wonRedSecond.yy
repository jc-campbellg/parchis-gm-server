{
    "id": "50de7780-9840-4b3b-8500-97ca3ba17aaf",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonRedSecond",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "2c391cc0-3166-49d9-b76e-fa3337ca362e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 354,
            "y": 34,
            "speed": 100
        },
        {
            "id": "1a240ea2-987a-4ad9-9164-1ab51ecbc7b3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 354,
            "y": 55,
            "speed": 100
        },
        {
            "id": "45620458-862b-4ef2-9ab0-2f84d505ee74",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 354,
            "y": 78,
            "speed": 100
        },
        {
            "id": "acdededf-db7e-4fd3-afe8-92a240e63745",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 354,
            "y": 99,
            "speed": 100
        },
        {
            "id": "520b3f26-8f57-4dd7-ae22-9baf16069df3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 354,
            "y": 120,
            "speed": 100
        },
        {
            "id": "931a6aa1-50b3-40a1-9ff3-983ea92c82ac",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 354,
            "y": 141,
            "speed": 100
        },
        {
            "id": "291d344d-1677-47c8-a165-8af3ec93a62c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 354,
            "y": 162,
            "speed": 100
        },
        {
            "id": "a84915b0-d75c-4c1d-8fbb-e259f7950b3a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 354,
            "y": 183,
            "speed": 100
        },
        {
            "id": "6d4c6970-f9a6-4e18-a236-b29e4b2940e5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 354,
            "y": 206,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}