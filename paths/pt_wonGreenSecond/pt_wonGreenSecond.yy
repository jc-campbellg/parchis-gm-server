{
    "id": "1786338c-46ff-4971-8b45-97a759d852dd",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonGreenSecond",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "65d2b654-e250-413e-bef0-87df9a4f47fa",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 328,
            "y": 557,
            "speed": 100
        },
        {
            "id": "68e917e6-6337-444e-abd2-de413abf1e0f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 328,
            "y": 537,
            "speed": 100
        },
        {
            "id": "446a75f4-9edf-4bd2-bb34-f0328d84e796",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 328,
            "y": 513,
            "speed": 100
        },
        {
            "id": "f4abf75a-56ca-4ad1-8afb-fa6cfa9e2132",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 328,
            "y": 492,
            "speed": 100
        },
        {
            "id": "575c2227-6ecb-4636-a098-40d5f346f86d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 328,
            "y": 471,
            "speed": 100
        },
        {
            "id": "fe203ce2-c020-45ff-85d9-6f52d0f95285",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 328,
            "y": 450,
            "speed": 100
        },
        {
            "id": "3253cb09-e4d5-45be-824f-09ea0bcc71b9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 328,
            "y": 429,
            "speed": 100
        },
        {
            "id": "5ec277a5-97f8-4776-8aa9-ba0ed8e9efe0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 328,
            "y": 408,
            "speed": 100
        },
        {
            "id": "eec09796-99df-43f2-b04b-c4e1437a20c7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 328,
            "y": 385,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}