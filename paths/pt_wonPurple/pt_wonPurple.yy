{
    "id": "5b59900f-1317-485b-a51b-da63688ec690",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonPurple",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "12955ba2-b692-4d31-ac1b-41e3d0400c40",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 568,
            "y": 426,
            "speed": 100
        },
        {
            "id": "d01c26a7-fd61-4a6a-8ce0-ed6516406428",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 550,
            "y": 416,
            "speed": 100
        },
        {
            "id": "9c2c7193-ac64-437a-9a6a-a837fdc0b37f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 532,
            "y": 405,
            "speed": 100
        },
        {
            "id": "b05f28ee-477d-459d-8bde-4cd6e7cc6a21",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 514,
            "y": 394,
            "speed": 100
        },
        {
            "id": "bb369e41-5e9a-4929-a8ea-ee2464dd9312",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 496,
            "y": 384,
            "speed": 100
        },
        {
            "id": "59e56625-dec8-4d83-bf7b-7e15271c88c9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 477,
            "y": 374,
            "speed": 100
        },
        {
            "id": "e214b865-2a49-47e8-98a7-73881b734504",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 460,
            "y": 364,
            "speed": 100
        },
        {
            "id": "355ebb76-853e-4992-82bd-7dce720b2c51",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 441,
            "y": 353,
            "speed": 100
        },
        {
            "id": "315ad3c4-a93f-4a5d-b8ce-4d0c7ef5986e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 421,
            "y": 341,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}