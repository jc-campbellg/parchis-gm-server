{
    "id": "dab02957-2421-45da-859c-b98d6e292800",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonOrangeSecond",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "1eefa4d7-e705-4be5-93f2-271ebe652174",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 575,
            "y": 176,
            "speed": 100
        },
        {
            "id": "a7a83674-0cc8-466f-93e5-7fd39fc13f83",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 556,
            "y": 187,
            "speed": 100
        },
        {
            "id": "45490ef5-8860-4e7f-830c-96f6a33b51ce",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 537,
            "y": 198,
            "speed": 100
        },
        {
            "id": "a0d857e6-a004-4627-82b9-ead5cc864684",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 518,
            "y": 209,
            "speed": 100
        },
        {
            "id": "88a3f202-6f5b-4812-aaac-9c28729a44ea",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 500,
            "y": 219,
            "speed": 100
        },
        {
            "id": "c13bc5ca-e1e4-4d9c-a0e5-40c18f55cf0a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 482,
            "y": 229,
            "speed": 100
        },
        {
            "id": "21ac0289-34c7-4eba-85e4-7c232ffa53ea",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 464,
            "y": 240,
            "speed": 100
        },
        {
            "id": "547e519a-76dd-4cce-882e-b6642723a86b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 446,
            "y": 251,
            "speed": 100
        },
        {
            "id": "86020384-73b5-448b-9af6-92ad15fdd81e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 423,
            "y": 265,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}