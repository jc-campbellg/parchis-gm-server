{
    "id": "efd7faca-d4b0-416f-bfca-8808d6456499",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonYellow",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "b0356611-15c7-4d63-9dd8-406120fb90fb",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 115,
            "y": 429,
            "speed": 100
        },
        {
            "id": "5103a419-3296-410b-a5f8-81a58d260f3c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 134,
            "y": 419,
            "speed": 100
        },
        {
            "id": "aab42990-c2a0-4f99-bd9f-9de92310eebd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 153,
            "y": 408,
            "speed": 100
        },
        {
            "id": "99f3b32b-5725-45dc-afce-a7a44eb1aef7",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 171,
            "y": 397,
            "speed": 100
        },
        {
            "id": "946e8c77-4554-4192-be84-ac11a03ca336",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 189,
            "y": 387,
            "speed": 100
        },
        {
            "id": "77658973-c942-4f9c-8430-3ee9c101c07e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 207,
            "y": 376,
            "speed": 100
        },
        {
            "id": "1e15007a-0f2f-4d02-9cc9-6c572a02fb9d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 225,
            "y": 366,
            "speed": 100
        },
        {
            "id": "edbd94c8-4515-42bd-8077-0d54b0457b91",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 244,
            "y": 355,
            "speed": 100
        },
        {
            "id": "08da0880-532b-4053-a3f5-f5f7cee58ffd",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 266,
            "y": 342,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}