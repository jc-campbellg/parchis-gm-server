{
    "id": "b92f2d40-a420-49ac-bd1c-5488a3b96632",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_goal",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "e22d14c1-a50d-4d19-8ae6-e4c75cf03ff2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 373,
            "y": 221,
            "speed": 100
        },
        {
            "id": "8cdd17a3-a3b8-47e3-98a6-5a5e76982b99",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 345,
            "y": 253,
            "speed": 100
        },
        {
            "id": "cc3a94a4-ba0e-4ea1-bf04-b5b65e8db2ca",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 319,
            "y": 224,
            "speed": 100
        },
        {
            "id": "20efeb4e-796d-4361-9921-71c1747df995",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 343,
            "y": 206,
            "speed": 100
        },
        {
            "id": "0d17d87d-8f6b-4d2b-8964-96801c6fba1b",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 290,
            "y": 242,
            "speed": 100
        },
        {
            "id": "b1302444-6ac4-4622-9c9a-ab82aeab9038",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 312,
            "y": 274,
            "speed": 100
        },
        {
            "id": "15bedd28-034d-4d8f-9e0c-341ccaf90e74",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 268,
            "y": 277,
            "speed": 100
        },
        {
            "id": "7c4b2ceb-a699-448f-a9df-1be707ff17e0",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 264,
            "y": 248,
            "speed": 100
        },
        {
            "id": "41140c1f-3d4f-431a-bd1b-cbb14a47eb8e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 270,
            "y": 308,
            "speed": 100
        },
        {
            "id": "4e48e023-79db-4fc8-96da-62f790e0178d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 308,
            "y": 311,
            "speed": 100
        },
        {
            "id": "a081c48a-e111-4609-b51c-1bf3eb9ac58e",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 295,
            "y": 344,
            "speed": 100
        },
        {
            "id": "beb9d897-783f-4c5e-b3fc-dc1361a5a7e3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 261,
            "y": 336,
            "speed": 100
        },
        {
            "id": "df45ae52-52b0-4f2f-904b-753d0b9f3039",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 321,
            "y": 356,
            "speed": 100
        },
        {
            "id": "7e8ba9f6-0fe7-48e2-9d92-d1d44308d59d",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 339,
            "y": 331,
            "speed": 100
        },
        {
            "id": "30f38b78-90f7-44cb-a9cd-51c25217e471",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 361,
            "y": 355,
            "speed": 100
        },
        {
            "id": "62ac8ac3-c89a-4bbb-aaa5-16f093f25ec8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 336,
            "y": 379,
            "speed": 100
        },
        {
            "id": "67c47341-e79d-4cd1-adb1-d224968a4f5c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 384,
            "y": 344,
            "speed": 100
        },
        {
            "id": "fc67e71c-68df-4331-be03-d64afbe2d2ab",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 372,
            "y": 312,
            "speed": 100
        },
        {
            "id": "8f01f62e-b036-40d4-bb91-77f25244168c",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 406,
            "y": 307,
            "speed": 100
        },
        {
            "id": "e00b855e-9a75-4c4a-bff8-488948654fc5",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 417,
            "y": 337,
            "speed": 100
        },
        {
            "id": "956c27b4-01b8-4f94-a339-ca2356e750d9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 402,
            "y": 284,
            "speed": 100
        },
        {
            "id": "8140257c-ca6c-4045-a018-2dccac3d47f9",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 371,
            "y": 277,
            "speed": 100
        },
        {
            "id": "400e05b3-6d41-4360-98c8-fe54d0d627e2",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 386,
            "y": 245,
            "speed": 100
        },
        {
            "id": "da642c5b-42d5-448e-b94c-34076d5783b8",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 418,
            "y": 257,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}