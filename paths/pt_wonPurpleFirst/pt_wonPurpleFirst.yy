{
    "id": "9e7e3c7b-f321-4e08-8ac9-3fe8a1582ecd",
    "modelName": "GMPath",
    "mvc": "1.0",
    "name": "pt_wonPurpleFirst",
    "closed": false,
    "hsnap": 0,
    "kind": 0,
    "points": [
        {
            "id": "72ad726a-b6b8-401a-bc27-476fc8d67c4f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 575,
            "y": 415,
            "speed": 100
        },
        {
            "id": "922c75c3-3cf6-4da8-8e73-2cc968a5a3df",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 558,
            "y": 405,
            "speed": 100
        },
        {
            "id": "c282bfda-8fb4-4c47-8c17-6a1407959dfc",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 538,
            "y": 393,
            "speed": 100
        },
        {
            "id": "33635cb7-a52f-49d9-ae88-486d4a155378",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 520,
            "y": 382,
            "speed": 100
        },
        {
            "id": "356c8866-029f-4ca8-b986-815aea54bd4a",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 502,
            "y": 372,
            "speed": 100
        },
        {
            "id": "21a741b8-6f67-4eb4-b898-704f65b5f2d3",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 483,
            "y": 362,
            "speed": 100
        },
        {
            "id": "640023cc-8c7f-4f66-8b6f-6fdf710c3e8f",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 466,
            "y": 352,
            "speed": 100
        },
        {
            "id": "6015a2f0-1f12-4ed4-b38d-355f51bc4602",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 447,
            "y": 341,
            "speed": 100
        },
        {
            "id": "f72d8e0d-9889-4930-8709-6fccfab31f81",
            "modelName": "GMPathPoint",
            "mvc": "1.0",
            "x": 428,
            "y": 326,
            "speed": 100
        }
    ],
    "precision": 4,
    "vsnap": 0
}