var num = argument0;
var spr = instance_find(o_piece, num).sprite_index;

switch(spr) {
	case s_pieceRed:
		return o_playerRed.socket;
	case s_pieceBlue:
		return o_playerBlue.socket;
	case s_pieceYellow:
		return o_playerYellow.socket;
	case s_pieceGreen:
		return o_playerGreen.socket;
	case s_piecePurple:
		return o_playerPurple.socket;
	case s_pieceOrange:
		return o_playerOrange.socket;
}