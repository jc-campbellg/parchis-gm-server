switch (sprite_index) {
    case s_pieceRed:
        return gameColor.red;
	case s_pieceBlue:
        return gameColor.blue;
	case s_pieceYellow:
        return gameColor.yellow;
	case s_pieceGreen:
        return gameColor.green;
	case s_piecePurple:
        return gameColor.purple;
	case s_pieceOrange:
        return gameColor.orange;
}