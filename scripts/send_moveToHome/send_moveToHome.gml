var socketList = o_server.socketList;
var serverBuffer = o_server.serverBuffer;
var moveSpeed = o_gameState.moveSpeed;

buffer_seek(serverBuffer, buffer_seek_start, 0);
buffer_write(serverBuffer, buffer_u8, net.moveToPosition);
buffer_write(serverBuffer, buffer_u8, myNumber);
buffer_write(serverBuffer, buffer_u8, 2);

// Create path
movePath = path_add();

var num = get_instance_number(o_piece);
var px = path_get_point_x(pt_spawn, num);
var py = path_get_point_y(pt_spawn, num);

path_add_point(movePath, x, y, moveSpeed);
path_add_point(movePath, px, py, moveSpeed);

buffer_write(serverBuffer, buffer_u16, x);
buffer_write(serverBuffer, buffer_u16, y);

buffer_write(serverBuffer, buffer_u16, px);
buffer_write(serverBuffer, buffer_u16, py);
	
pos = -1;

var sockets = ds_list_size(socketList);
for (var i = 0; i < sockets; i++) {
	network_send_packet(socketList[| i], serverBuffer, buffer_tell(serverBuffer));
}

path_set_closed(movePath, false);
path_start(movePath, moveSpeed, path_action_stop, true);