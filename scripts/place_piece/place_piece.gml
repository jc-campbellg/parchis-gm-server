var n = argument0;

with(instance_find(o_piece, n)) {
	x = path_get_point_x(pt_spawn, n);
	y = path_get_point_y(pt_spawn, n);
}