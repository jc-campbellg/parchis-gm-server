var socketList = o_server.socketList;
var serverBuffer = o_server.serverBuffer;

o_gameState.start = true;
o_gameState.turn = round(random(5));

buffer_seek(serverBuffer, buffer_seek_start, 0);
buffer_write(serverBuffer, buffer_u8, net.gameStart);
buffer_write(serverBuffer, buffer_u8, o_gameState.turn);

var sockets = ds_list_size(socketList);
for (var i = 0; i < sockets; i++) {
	network_send_packet(socketList[| i], serverBuffer, buffer_tell(serverBuffer));
}