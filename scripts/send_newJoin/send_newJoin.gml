var newClient = argument0;
var name = argument1;
var color = argument2

var i = 0;
show_debug_message("new client: " + string(newClient));

buffer_seek(serverBuffer, buffer_seek_start, 0);
buffer_write(serverBuffer, buffer_u8, net.newJoin);
buffer_write(serverBuffer, buffer_string, name);
buffer_write(serverBuffer, buffer_u8, color);

var sockets = ds_list_size(socketList);
for (var i = 0; i < sockets; i++) {
	if (socketList[| i] != newClient) {
		network_send_packet(socketList[| i], serverBuffer, buffer_tell(serverBuffer));
	}
}