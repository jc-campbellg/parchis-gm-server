var socket = argument0;
var buffer = argument1;

var msgId = buffer_read(buffer, buffer_u8);

switch(msgId) {
	case net.join:
		var name = buffer_read(buffer, buffer_string);
		var color = buffer_read(buffer, buffer_u8);
		if (playerInstances[color].available) {
			playerInstances[color].available = false;
			playerInstances[color].used = true;
			playerInstances[color].name = name;
			playerInstances[color].socket = socket;
			playerInstances[color].ready = false;
			send_newJoin(socket, name, color);
			send_join(socket);
		} else {
			send_tryAgainColor(socket);
		}
		break;
	
	case net.syncPositions:
		var c = 0;
		repeat(6) {
			var n = c * 4;
			repeat(4) {
				send_jumpToPosition(n);
				n = n + 1;
			}
			c = c + 1;
		}
		break;
		
	case net.ready:
		var color = buffer_read(buffer, buffer_u8);
		playerInstances[color].ready = true;
		send_ready(color);
		allReady = true;
		for (var i = 0; i < 6; ++i) {
		    if (!playerInstances[i].ready) {
				allReady = false;
			}
		}
		if (allReady) {
			send_gameStart();
		}
		break;
	
	case net.chat:
		var message = buffer_read(buffer, buffer_string);
		send_chat(message);
		break;
}