var socketList = o_server.socketList;
var serverBuffer = o_server.serverBuffer;

o_gameState.start = true;

buffer_seek(serverBuffer, buffer_seek_start, 0);
buffer_write(serverBuffer, buffer_u8, net.chat);
buffer_write(serverBuffer, buffer_string, argument0);

var sockets = ds_list_size(socketList);
for (var i = 0; i < sockets; i++) {
	network_send_packet(socketList[| i], serverBuffer, buffer_tell(serverBuffer));
}