var playerToRemove = argument0;

// Find Color
var color = -1;

if (o_playerRed.socket == playerToRemove) {
	o_playerRed.available = true;
	o_playerRed.used = false;
	o_playerRed.ready = false;
	o_playerRed.name = "";
	o_playerRed.socket = -1;
	color = gameColor.red;
} else if (o_playerBlue.socket == playerToRemove) {
	o_playerBlue.available = true;
	o_playerBlue.used = false;
	o_playerBlue.ready = false;
	o_playerBlue.name = "";
	o_playerBlue.socket = -1;
	color = gameColor.blue;
} else if (o_playerYellow.socket == playerToRemove) {
	o_playerYellow.available = true;
	o_playerYellow.used = false;
	o_playerYellow.ready = false;
	o_playerYellow.name = "";
	o_playerYellow.socket = -1;
	color = gameColor.yellow;
} else if (o_playerGreen.socket == playerToRemove) {
	o_playerGreen.available = true;
	o_playerGreen.used = false;
	o_playerGreen.ready = false;
	o_playerGreen.name = ""
	o_playerGreen.socket = -1;;
	color = gameColor.green;
} else if (o_playerOrange.socket == playerToRemove) {
	o_playerOrange.available = true;
	o_playerOrange.used = false;
	o_playerOrange.ready = false;
	o_playerOrange.name = "";
	o_playerOrange.socket = -1;;
	color = gameColor.purple;
} else if (o_playerPurple.socket == playerToRemove) {
	o_playerPurple.available = true;
	o_playerPurple.used = false;
	o_playerPurple.ready = false;
	o_playerPurple.name = "";
	o_playerPurple.socket = -1;;
	color = gameColor.orange;
}

if (color != -1) {
	buffer_seek(serverBuffer, buffer_seek_start, 0);
	buffer_write(serverBuffer, buffer_u8, net.removePlayer);
	buffer_write(serverBuffer, buffer_u8, color);
		
	var sockets = ds_list_size(socketList);
	for (var i = 0; i < sockets; i++) {
		network_send_packet(socketList[| i], serverBuffer, buffer_tell(serverBuffer));
	}
}