var steps = argument0;
var moveSpeed = o_gameState.moveSpeed;
var socketList = o_server.socketList;
var serverBuffer = o_server.serverBuffer;

// Create path
movePath = path_add();
var piecesInPos = find_piece_in_pos(pos, onWinningSpace);
if (array_length_1d(piecesInPos)) {
	var otherPiece = piecesInPos[0];
	with (otherPiece) {
		var pathArray = [pt_steps, pt_stepsFirst, pt_stepsSecond];
	
		if onWinningSpace then pathArray = get_won_path_by_sprite();
				
		x = path_get_point_x(pathArray[0], onWinningSpace ? pos-winSpot : pos);
		y = path_get_point_y(pathArray[0], onWinningSpace ? pos-winSpot : pos);
		
		send_jumpToPosition(myNumber);
	}
}
var here = pos + 1;

buffer_seek(serverBuffer, buffer_seek_start, 0);
buffer_write(serverBuffer, buffer_u8, net.moveToPosition);
buffer_write(serverBuffer, buffer_u8, myNumber);
buffer_write(serverBuffer, buffer_u8, steps+1);
buffer_write(serverBuffer, buffer_u16, x);
buffer_write(serverBuffer, buffer_u16, y);

path_add_point(movePath, x, y, 15);

repeat(steps) {
	here = (here < 102) || onWinningSpace ? here : 0;
	var pathArray = [pt_steps, pt_stepsFirst, pt_stepsSecond];
	if (here == winSpot+1) {
		// Inside a win space
		onWinningSpace = true;
	}
	
	if onWinningSpace then pathArray = get_won_path_by_sprite();
	
	var piecesInHere = find_piece_in_pos(here, onWinningSpace);
	
	var pathToUse = array_length_1d(piecesInHere) == 0 ? pathArray[0] : pathArray[2];
	var px = path_get_point_x(pathToUse, onWinningSpace ? -abs(((here-winSpot) % 16) - 8) + 8 : here);
	var py = path_get_point_y(pathToUse, onWinningSpace ? -abs(((here-winSpot) % 16) - 8) + 8 : here);
	
	buffer_write(serverBuffer, buffer_u16, px);
	buffer_write(serverBuffer, buffer_u16, py);
	
	path_add_point(movePath, px, py, moveSpeed);
	
	here = here + 1;
}

pos = here-1;

var sockets = ds_list_size(socketList);
for (var i = 0; i < sockets; i++) {
	network_send_packet(socketList[| i], serverBuffer, buffer_tell(serverBuffer));
}

path_set_closed(movePath, false);
path_start(movePath, moveSpeed, path_action_stop, true);