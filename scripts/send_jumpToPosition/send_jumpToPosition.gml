var number = argument0;
var socketList = o_server.socketList;
var serverBuffer = o_server.serverBuffer;

buffer_seek(serverBuffer, buffer_seek_start, 0);
buffer_write(serverBuffer, buffer_u8, net.jumpToPosition);

var inst = instance_find(o_piece, number);

buffer_write(serverBuffer, buffer_u8, number);
buffer_write(serverBuffer, buffer_u16, inst.x);
buffer_write(serverBuffer, buffer_u16, inst.y);

var sockets = ds_list_size(socketList);
for (var i = 0; i < sockets; i++) {
	network_send_packet(socketList[| i], serverBuffer, buffer_tell(serverBuffer));
}