switch (sprite_index) {
    case s_pieceRed:
        return [pt_wonRed, pt_wonRedFirst, pt_wonRedSecond];
	case s_pieceBlue:
        return [pt_wonBlue, pt_wonBlueFirst, pt_wonBlueSecond];
	case s_pieceYellow:
        return [pt_wonYellow, pt_wonYellowFirst, pt_wonYellowSecond];
	case s_pieceGreen:
        return [pt_wonGreen, pt_wonGreenFirst, pt_wonGreenSecond];
	case s_piecePurple:
        return [pt_wonPurple, pt_wonPurpleFirst, pt_wonPurpleSecond];
	case s_pieceOrange:
        return [pt_wonOrange, pt_wonOrangeFirst, pt_wonOrangeSecond];
}