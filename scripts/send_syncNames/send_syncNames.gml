var socket = argument0;

buffer_seek(serverBuffer, buffer_seek_start, 0);
buffer_write(serverBuffer, buffer_u8, net.syncNames);
// Red
buffer_write(serverBuffer, buffer_bool, o_playerRed.available);
buffer_write(serverBuffer, buffer_string, o_playerRed.name);
buffer_write(serverBuffer, buffer_bool, o_playerRed.ready);
// Blue
buffer_write(serverBuffer, buffer_bool, o_playerBlue.available);
buffer_write(serverBuffer, buffer_string, o_playerBlue.name);
buffer_write(serverBuffer, buffer_bool, o_playerBlue.ready);
// Yellow
buffer_write(serverBuffer, buffer_bool, o_playerYellow.available);
buffer_write(serverBuffer, buffer_string, o_playerYellow.name);
buffer_write(serverBuffer, buffer_bool, o_playerYellow.ready);
// Green
buffer_write(serverBuffer, buffer_bool, o_playerGreen.available);
buffer_write(serverBuffer, buffer_string, o_playerGreen.name);
buffer_write(serverBuffer, buffer_bool, o_playerGreen.ready);
// Orange
buffer_write(serverBuffer, buffer_bool, o_playerOrange.available);
buffer_write(serverBuffer, buffer_string, o_playerOrange.name);
buffer_write(serverBuffer, buffer_bool, o_playerOrange.ready);
// Purple
buffer_write(serverBuffer, buffer_bool, o_playerPurple.available);
buffer_write(serverBuffer, buffer_string, o_playerPurple.name);
buffer_write(serverBuffer, buffer_bool, o_playerPurple.ready);
network_send_packet(socket, serverBuffer, buffer_tell(serverBuffer));