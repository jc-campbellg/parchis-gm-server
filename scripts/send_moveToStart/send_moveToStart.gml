var socketList = o_server.socketList;
var serverBuffer = o_server.serverBuffer;
buffer_seek(serverBuffer, buffer_seek_start, 0);
buffer_write(serverBuffer, buffer_u8, net.moveToPosition);
buffer_write(serverBuffer, buffer_u8, myNumber);
buffer_write(serverBuffer, buffer_u8, 2);

// Create path
movePath = path_add();

path_add_point(movePath, x, y, 15);
var here = startSpot;
var piecesInHere = find_piece_in_pos(here, false);

var pathToUse = pt_steps;
if (array_length_1d(piecesInHere) > 0) then pathToUse = pt_stepsSecond;

var px = path_get_point_x(pathToUse, here);
var py = path_get_point_y(pathToUse, here);

path_add_point(movePath, x, y, 15);
path_add_point(movePath, px, py, 15);

buffer_write(serverBuffer, buffer_u16, x);
buffer_write(serverBuffer, buffer_u16, y);

buffer_write(serverBuffer, buffer_u16, px);
buffer_write(serverBuffer, buffer_u16, py);
	
pos = here;

var sockets = ds_list_size(socketList);
for (var i = 0; i < sockets; i++) {
	network_send_packet(socketList[| i], serverBuffer, buffer_tell(serverBuffer));
}

path_set_closed(movePath, false);
path_start(movePath, 15, path_action_stop, true);