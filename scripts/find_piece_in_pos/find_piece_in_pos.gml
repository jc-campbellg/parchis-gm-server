var searchHere = argument0;
var goingToWinning = argument1;
var pieces = [];

var i = 0;
repeat(24) {
	var ins = instance_find(o_piece, i);
	
	if (ins != self) {
		if (goingToWinning) {
			if (ins.sprite_index == sprite_index) {
				if (ins.pos == searchHere) {
					pieces[array_length_1d(pieces)] = ins;
				}
			}
		} else {
			if (!ins.onWinningSpace) {
				if (ins.pos == searchHere) {
					pieces[array_length_1d(pieces)] = ins;
				}
			}
		}
	}
	
	i = i+1;
}
return pieces;