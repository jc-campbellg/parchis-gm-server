var here = argument0;
var goingToWinning = onWinningSpace;
var uTurn = o_gameState.uTurn;

here = (here < 102) || goingToWinning ? here : 0;

if (here == winSpot+1) {
	// Inside a win space
	goingToWinning = true;
}
	
var piecesInHere = find_piece_in_pos(here, goingToWinning);
	
if (array_length_1d(piecesInHere) == 2) return false;
	
if (!uTurn) {
	if here-winSpot > 8 then return false;
}

return true;