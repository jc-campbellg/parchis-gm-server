{
    "id": "3427e1c9-8f8b-42ee-9ac4-4b66952d39ec",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "ft_console",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": false,
    "charset": 0,
    "first": 0,
    "fontName": "Arial",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "175c62c5-272d-4238-8acf-a59c5301e86c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "7b6472c1-9e1d-403b-9ae8-78832879f0ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 51,
                "y": 107
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "6eddcca8-8b47-450e-9335-0da7803f3edc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 5,
                "x": 56,
                "y": 107
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "04e01ae6-e21f-4ed6-9e51-be28818cdd17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 63,
                "y": 107
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "71c34d9f-4617-4a0d-ba78-38ae4d377ba2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 74,
                "y": 107
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "329766ff-8b18-4c9b-8408-64f61ebf3021",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 85,
                "y": 107
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "f36f3ac4-2ee0-47ed-9ed4-e11c70f9ff6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 101,
                "y": 107
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "f2f0ae61-14e2-4502-a6b7-21defb07ec29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 19,
                "offset": 0,
                "shift": 3,
                "w": 3,
                "x": 114,
                "y": 107
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "1d4fa01b-f27f-436e-8856-02875fa2ae5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 119,
                "y": 107
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "02d60f9c-9557-44be-b953-01cccd4feabd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 126,
                "y": 107
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "984a847d-2ac7-47a4-94ee-bba52573df1b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 144,
                "y": 107
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "7bad74ef-0dc8-4a67-9219-62aecff87610",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 227,
                "y": 107
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "2bd975c0-1716-4bcd-9e73-d0e098321163",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 152,
                "y": 107
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "bae17074-0545-4b65-8242-75bac9285027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 157,
                "y": 107
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "8786aa72-15d3-4d3f-b8e7-082d2ee58b7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 164,
                "y": 107
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "33b12028-195d-44e5-afc4-90ee7b938387",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 169,
                "y": 107
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "eb7fff2e-f0a9-4443-a1d7-73a0569cb034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 176,
                "y": 107
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "16d58571-08a2-442b-9638-c026450c383c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 5,
                "x": 187,
                "y": 107
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "2c435923-92a6-442e-b336-84ddd00c5d3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 194,
                "y": 107
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "b9938046-bf68-496e-85ac-af1bf778fbae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 205,
                "y": 107
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "e337ddc7-71b4-4eb4-ac3d-a8cd40f20d73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 216,
                "y": 107
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "2deaee4a-a612-4612-9f5a-987ddb6ac420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 40,
                "y": 107
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "039b1df7-ad07-4ae7-b113-9f3890387fc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 133,
                "y": 107
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "bb9445b2-5cd3-4136-b334-02ee3a817560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 29,
                "y": 107
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "98b02e24-fc38-46f8-aae7-9f6a8391cfce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 130,
                "y": 86
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "447b9aa5-e27b-4519-a840-d36f2a7aa5ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 33,
                "y": 86
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "6c5dbdda-4815-4d92-9b04-554e6e491997",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 44,
                "y": 86
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "875391a8-d8ab-42e9-ac0f-e53914f5cb14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 49,
                "y": 86
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "b9c1fa41-f512-416d-a060-3960a215ea0b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 54,
                "y": 86
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "d16e3c1b-f8f4-4bf9-99c0-4e74b3a87422",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 65,
                "y": 86
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "679d7f49-a877-4d7b-a277-c5b6fc44829c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 76,
                "y": 86
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "688ac1aa-7212-4d47-b697-d677e42bef21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 87,
                "y": 86
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "0152ffb1-84c4-4415-b5d7-cd107c2d5ecd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 98,
                "y": 86
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "19079c7e-2523-492d-93c8-5c0401dc0dec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 19,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 116,
                "y": 86
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "27ab8413-e634-4f99-81f4-0b54d394d7eb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 141,
                "y": 86
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "f59bea7b-e2b2-4258-9e28-d01bfcfa4176",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 107
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "d1823c4a-21e4-4cf1-8cef-bfee330c7730",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 152,
                "y": 86
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "192c4375-ea2f-4260-827b-de043b786ccc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 164,
                "y": 86
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "ae8b95f5-724b-4c69-af1e-7807af3e4bcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 175,
                "y": 86
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "3b0a5350-a5db-466f-8b6a-ae15621ae0da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 186,
                "y": 86
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "83160e20-cca7-4815-a8cf-d3bd7c689686",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 200,
                "y": 86
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "eae2fcf7-2768-465c-bd9f-ca3b4cb65a4b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 212,
                "y": 86
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "ffe65f91-cd0d-40e4-bea3-ef09be76140f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 7,
                "x": 217,
                "y": 86
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "6fe6d752-febf-4ecb-b411-c385e2e7ff9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 10,
                "x": 226,
                "y": 86
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "7de4b24f-0d91-4d8b-b1c9-db61a6193ee0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 238,
                "y": 86
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "f3573594-833e-4e9c-a01e-bce1a2df8c9d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 15,
                "y": 107
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "3547b29d-a9b8-4553-a6a2-e3c3d66dcf36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 238,
                "y": 107
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "fabbe29b-b03f-46c9-9e0a-4eb8f3bc83ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 128
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "ae1e993f-682c-4633-ad83-2796d8740f9f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 16,
                "y": 128
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "2b2797c8-e709-49d8-ba50-10e6eaf19d31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 235,
                "y": 128
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "cf59c3ac-87f0-480e-83db-7fe169019f95",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 11,
                "x": 2,
                "y": 149
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "32ba478a-1d1b-4c99-bb96-befb868acf75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 15,
                "y": 149
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1f780ee0-21d0-4099-a6be-ef100e084c58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 27,
                "y": 149
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "49396e59-324d-4c58-8baf-e97d8f8f7ed2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 39,
                "y": 149
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "31bb50be-c855-49cc-a5c6-a190a3703b3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 51,
                "y": 149
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "22ec7e78-575e-460a-bb7e-8bb5afc91daf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 19,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 64,
                "y": 149
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "256b801d-6067-4560-b9a2-ad59d9f3d17d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 81,
                "y": 149
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "75279588-d304-4ab6-ac1e-1415fcbe24fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 94,
                "y": 149
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4100a006-ebc4-4ad1-a115-950dd48146f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 19,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 107,
                "y": 149
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "57731d28-f408-457f-a40b-44098e28600b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 119,
                "y": 149
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "afbae804-3803-492c-b973-6b561d4e19e6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 125,
                "y": 149
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "2f51de6f-9390-465b-ae15-f5742c094c20",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 132,
                "y": 149
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0bfa9968-195a-4bb8-83b5-cffbc7a0e345",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 138,
                "y": 149
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "4f198de5-92d7-4775-a6a2-4bba512648aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 19,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 148,
                "y": 149
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "b94ea7b9-5eb0-46e2-b329-aa2ba92869a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 161,
                "y": 149
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "dac6277c-87d0-4b9f-99af-6d43716defe2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 167,
                "y": 149
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "30838ad7-85da-4b58-9fda-1a658323a24d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 178,
                "y": 149
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "9cafc1b1-757c-4290-9e12-8e1b0e362ee3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 188,
                "y": 149
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "6f43baa1-5456-46fc-9364-5c6c5b86328f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 198,
                "y": 149
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "6ddafe57-2e98-444a-94f8-c8235d60668d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 208,
                "y": 149
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "c889ae88-aa9d-4328-8216-50149c404dbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 228,
                "y": 128
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ef43f4f1-4bd3-4630-8e1c-29d0f215a4bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 218,
                "y": 128
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c3727fd4-ead7-47bf-af61-0765d1d5389c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 209,
                "y": 128
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "3cc7ca8d-3002-46c8-8b2c-77464cd24c38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 107,
                "y": 128
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "a383a410-ee59-4d48-9802-3c46bffa910b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 19,
                "offset": -1,
                "shift": 4,
                "w": 4,
                "x": 27,
                "y": 128
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "237f5f85-6b43-4b87-ad1c-6e042ecf9bed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 19,
                "offset": 1,
                "shift": 8,
                "w": 7,
                "x": 33,
                "y": 128
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "8bd49f25-9607-40fe-b285-01dd3cdd72e4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 42,
                "y": 128
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "566ad0a2-538d-47ec-be50-e36f81970c6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 19,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 46,
                "y": 128
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "380839be-db9d-436d-a56c-4ebb7bf2468a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 60,
                "y": 128
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "28926571-f786-4670-b81c-27bd478d7205",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 69,
                "y": 128
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "1215663d-20b8-485d-8ebb-edcc9397c109",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 80,
                "y": 128
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "b29054b9-91fd-4c4b-8488-532f42b4ca61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 90,
                "y": 128
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "7dd0b6bc-2753-4fc0-9f6e-37f68cf0abbe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 5,
                "x": 100,
                "y": 128
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "71fd0884-b6f5-46c5-a262-66de409f248f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 111,
                "y": 128
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "e269c674-1468-42ba-8153-1c165841ec14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 202,
                "y": 128
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "d71bbe5f-4461-4df9-b176-689d94fc9748",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 121,
                "y": 128
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "3a9310ca-d579-40c7-b737-464ecd430a3d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 130,
                "y": 128
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "089a7f0e-c5ff-4afa-8a27-35190d6eca5d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 140,
                "y": 128
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "fddc3a02-ccd1-49a0-8123-5ef39fb79932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 154,
                "y": 128
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "e55c7cd6-d17f-4726-8de2-bc4760d0a890",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 164,
                "y": 128
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "c00f0a50-8fab-41fc-b02e-d62a8c2a8f66",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 174,
                "y": 128
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "e948d646-de64-4027-a6e2-4c9b0747f44c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 184,
                "y": 128
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "d0a69984-f813-4356-9927-b51f66355094",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 191,
                "y": 128
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "44b10a84-f1de-4bb9-b0b2-83a9c06a163e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 195,
                "y": 128
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "a2926e55-551a-47e1-b0fa-574282dcc574",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 22,
                "y": 86
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "559ca137-7670-431e-b3f5-f34f2b59ebca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 0,
                "x": 20,
                "y": 86
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "6a37aec6-d708-45f3-bc62-8cbee59376ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 15,
                "y": 86
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "cb1ab2f6-ab45-45e5-89bb-1f453b4a6348",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 233,
                "y": 23
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "915539b2-f07b-47f3-afce-ba026af7f82d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 34,
                "y": 23
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "c4a6dd99-de84-4b0e-abfa-24e3f5a0bab9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 45,
                "y": 23
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "e5a54cec-8e47-4174-99a1-0a192c8e3f38",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 19,
                "offset": -1,
                "shift": 9,
                "w": 10,
                "x": 56,
                "y": 23
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "692b1e97-6131-46d3-aba2-bb636230f66c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 2,
                "x": 68,
                "y": 23
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "c61251b4-34db-466d-9af9-b249d8dce4b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 72,
                "y": 23
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "c61fcd80-5dd3-4750-8403-403e4e46f503",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 83,
                "y": 23
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "ffa1121c-f7d4-40b1-a6ba-da8c96019f8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 90,
                "y": 23
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "d06a258d-0f45-412b-8a72-86e3c5e23f15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 104,
                "y": 23
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "86dafac6-6ebc-49fe-b8bc-52a2777f625d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 112,
                "y": 23
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "f7bc57e7-c548-44f8-922a-45c1a7630fe6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 128,
                "y": 23
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "6c55f65f-98ab-4244-9cc2-517bd3eec4ef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 226,
                "y": 23
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "0a9faa31-5feb-4e0f-b01b-eb3bd5e7096f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 139,
                "y": 23
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "2a9da1b2-aef6-4197-8887-e3f921a0ff59",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 19,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 153,
                "y": 23
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "f28df1d3-c36f-4693-bc5a-e02b953654af",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 19,
                "offset": 1,
                "shift": 6,
                "w": 5,
                "x": 166,
                "y": 23
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "6a49b27c-924a-4a99-a7c3-02e441693e09",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 173,
                "y": 23
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "62fcf302-bf0c-4d01-af7f-ad1a4ea2b38d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 184,
                "y": 23
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "60b4cea4-1177-416a-8635-95d2a4176dfd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 6,
                "x": 192,
                "y": 23
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "f087af0a-841a-42a3-87aa-972dda3a957f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 4,
                "x": 200,
                "y": 23
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "1cf0ab7e-af6b-487b-a60e-cfd32ac43b64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 206,
                "y": 23
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "c7e994db-490c-4cc2-a30a-e04f0db7e0c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 215,
                "y": 23
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "56cbd61b-b0c4-472a-9b31-8d63d2f1f8ea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 19,
                "offset": 1,
                "shift": 5,
                "w": 3,
                "x": 29,
                "y": 23
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "f9b6e14b-d647-404f-9018-fef91264be1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 5,
                "x": 121,
                "y": 23
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "d864a487-7d3b-47f0-b3c2-fe475cef08c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 19,
                "offset": 0,
                "shift": 5,
                "w": 4,
                "x": 23,
                "y": 23
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "a47158c9-8df7-45bc-b47e-cb6a94ba7a4d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 19,
                "offset": 0,
                "shift": 6,
                "w": 6,
                "x": 131,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "586fa833-6809-4dcb-a906-2d11d50290ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 8,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "12877aa8-fe75-48df-9a73-e1c445814c08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 17,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "a1b4be89-eff5-4f78-a5f6-63ebfebb71b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 33,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "822db0a2-6dc7-48fa-bad2-aa47719cd2c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 19,
                "offset": 0,
                "shift": 13,
                "w": 14,
                "x": 49,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "9224b166-bd0e-4b6d-8aa1-59b91e4cba97",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 65,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "057ad54c-9ba5-4d76-b0c5-0dbe4f3ce98a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 19,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 75,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "3ef10d46-1cf9-4809-8a0b-534bd227d585",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 19,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 89,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "26a321d4-0f42-46ae-81d6-f9198def38e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 19,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 103,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "63b9a497-f047-4b49-bbd7-c1a66a711955",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 19,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 117,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "c96c99c6-6222-4a4f-ab2c-e35cff934771",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 19,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 139,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "1426f818-85d2-4434-af40-48e49e637f35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 19,
                "offset": -1,
                "shift": 11,
                "w": 12,
                "x": 2,
                "y": 23
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "bce41148-8bee-4a93-9099-bffbc87f574d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 19,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 153,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "880f34d9-cb7b-4a1e-adb1-7dcce782dfac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 171,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "aa2a3d4c-d6cf-4b6c-9a6e-9b7b4f24b61f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "f469d3bc-3ecf-4f04-9d54-aaf4d8369592",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 195,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "9bf0b7cf-fcd1-45a1-81f4-e7ad780d8b5b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 206,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "7c241015-a7b7-4a03-b166-7cf3848788f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 217,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "72e6938a-d5dc-4102-8c5e-f9465f020cf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 228,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "65d8bf27-156b-4871-85c4-6544696c413a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 3,
                "x": 234,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "29372cb9-36b6-4f9c-9a0e-41816815d7b5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 19,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 239,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "7e891825-544f-4fa3-875f-608cb900d3f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 16,
                "y": 23
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "0a91365e-0aea-4222-80f7-0886e9336863",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 19,
                "offset": -1,
                "shift": 12,
                "w": 12,
                "x": 2,
                "y": 44
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "ab581779-2ac3-477a-b52a-beb9a7094579",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 231,
                "y": 44
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "b00b8265-a281-49f3-bd0b-e0becd772028",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 16,
                "y": 44
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "e7dcd537-2e10-41d4-826a-a572b0dc5274",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 12,
                "y": 65
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "1c965048-5c14-439f-adab-0839bead4210",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 26,
                "y": 65
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "ee790415-8f68-462c-82a0-e605db9e9528",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 40,
                "y": 65
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "129f3c5b-9a45-4cb4-90b8-320b88ba89c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 54,
                "y": 65
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "ba2093a2-dca1-41a8-83b6-1c614e53c142",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 68,
                "y": 65
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "2b331627-87d6-41ba-8abf-f7fb291f35e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 19,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 78,
                "y": 65
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "cec7c5aa-eb70-4a97-a852-fad5d97806cc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 92,
                "y": 65
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "ba0a41be-0c1a-4e08-af57-5e128275afc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 104,
                "y": 65
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "0dcd9745-712e-4dad-ba9b-7462760de598",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 116,
                "y": 65
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "dcb4f9c0-8ec5-400f-b0f5-ad99477f89dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 19,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 139,
                "y": 65
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "44bf3f07-7ce7-4ade-bf8e-18d994d7cbf9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 19,
                "offset": 0,
                "shift": 11,
                "w": 11,
                "x": 2,
                "y": 86
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "90d964c3-e0a3-43a4-888e-843cc886e939",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 19,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 151,
                "y": 65
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "94c17cb7-8e2a-4543-8257-bd2a4083b150",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 162,
                "y": 65
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "39bb8612-0a1a-4344-ba5d-6c414c43e793",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 173,
                "y": 65
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "46332f71-064d-4928-a472-c1fc4344c0f5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 184,
                "y": 65
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "72689309-a18d-41dd-baaf-13a514bfd0c2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 195,
                "y": 65
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "4c62b3a7-7ce3-431a-841a-d5d83daf8444",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 206,
                "y": 65
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "ff569ac0-83f4-47dc-a1f0-dd3e299b435f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 217,
                "y": 65
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "4baba9ad-f910-40df-b14f-b4e911dfeea5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 228,
                "y": 65
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "5d5e3c12-20cd-470a-b345-7b4955eb98f3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 19,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 239,
                "y": 65
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "85084f87-1630-4898-8a22-1c3c1afbdfb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 2,
                "y": 65
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "4f4e4b63-5392-4b8f-b5de-c9137a6dcb33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 128,
                "y": 65
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "31e24388-11cd-4106-b08d-6a8e18037771",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 243,
                "y": 44
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "5b77744d-74bd-4fbc-87ed-31ae2df2410c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 110,
                "y": 44
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "c70d9687-e867-47d9-9df0-676f5fd7a8e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 30,
                "y": 44
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "25236edf-6c80-427c-bd55-d2ac583c9f73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 4,
                "x": 41,
                "y": 44
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "aa83d9b3-f788-45a4-a4dd-dc707db92f98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 19,
                "offset": 1,
                "shift": 4,
                "w": 4,
                "x": 47,
                "y": 44
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "f055461e-f3eb-40d0-ba1f-a26a5fce9d27",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 19,
                "offset": -1,
                "shift": 4,
                "w": 6,
                "x": 53,
                "y": 44
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "112a7db1-a8bf-43c8-ba0b-ae13660e2661",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 19,
                "offset": 0,
                "shift": 4,
                "w": 5,
                "x": 61,
                "y": 44
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "355c05a4-c461-4008-afa3-f5cee0625126",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 68,
                "y": 44
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "89226bbd-85fa-422b-bad3-600a5dfbda5a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 79,
                "y": 44
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "8ea9e5f3-b30b-44cb-a540-6bcc35f658ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 88,
                "y": 44
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "28b5c449-6d39-4f13-aa36-e693624c7585",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 99,
                "y": 44
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "349277e6-011a-4475-9793-0dd80ce7a56c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 121,
                "y": 44
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "6c1e780c-5b69-4d03-bfaf-9f7b19174671",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 220,
                "y": 44
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "233dd289-f7dd-4633-a1ca-30a91d05e421",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 132,
                "y": 44
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "cb3dd0c8-0e12-4b1d-8146-56cd88c466bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 19,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 143,
                "y": 44
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "f8c9117d-a371-4f40-b6d4-6e9a683c6698",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 19,
                "offset": 1,
                "shift": 10,
                "w": 8,
                "x": 154,
                "y": 44
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "6b94bb12-68a5-4407-8635-a81a0290d8be",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 164,
                "y": 44
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "5349213a-a1f0-4cfe-9161-bd0fc1c7faa4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 173,
                "y": 44
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "4d5ee1bf-c477-4b10-85c5-6e1a2b335b06",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 182,
                "y": 44
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "2d85cfd0-a012-4202-a716-4b5e138b88b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 7,
                "x": 191,
                "y": 44
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "e9409dba-a916-423b-b480-d3ad8fcc24d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 200,
                "y": 44
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "344f9985-11c9-4851-9e8c-5a3cdf8e7784",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 19,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 210,
                "y": 44
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "f3d2631a-0abc-44e0-b922-fc7f808d5876",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 19,
                "offset": 0,
                "shift": 8,
                "w": 8,
                "x": 219,
                "y": 149
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "2210f8a1-6f87-44e7-9fc2-7fd4b0c2354f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 19,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 229,
                "y": 149
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        {
            "id": "00ed357f-f8f5-4141-b655-e5c09a3884f2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 65
        },
        {
            "id": "26d9bb3c-8883-46a6-babb-30fdf92a997a",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 902
        },
        {
            "id": "e3b82188-5b4a-42e3-97ba-d3fa48823fb1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 913
        },
        {
            "id": "17882649-ed65-42b9-aa44-a7be96138282",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 916
        },
        {
            "id": "bf8a2269-6157-4593-a678-b1b7d3922b74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 32,
            "second": 923
        },
        {
            "id": "95948c4a-172c-49d9-ad77-ff36b282a04b",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 49,
            "second": 49
        },
        {
            "id": "2bcd4753-5070-4d4e-862c-8000fbac1558",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 32
        },
        {
            "id": "71496057-6a86-46e4-9246-fabb73027582",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 84
        },
        {
            "id": "7f43d23e-fa8e-42b6-b8ee-32f8209c3bb4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 86
        },
        {
            "id": "dd7d89c4-d074-461d-93ee-d32078dc8536",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 89
        },
        {
            "id": "f060f10c-5043-4c0f-b414-701b20a68780",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 160
        },
        {
            "id": "4912a6cb-83c4-4062-9780-bc85331c384f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 65,
            "second": 8217
        },
        {
            "id": "36da7f90-065b-44a9-90f0-b9b859b361ca",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 44
        },
        {
            "id": "9c3cec43-a19a-4ed7-b8a1-78513cef2f22",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 46
        },
        {
            "id": "0a2b3170-7075-4a18-9f32-996d6b0fc288",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 70,
            "second": 65
        },
        {
            "id": "3bfc7da8-5feb-4b5d-aad3-d65adc24c872",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 84
        },
        {
            "id": "3c46de92-6e5a-4e8f-9006-2fc55e19ca74",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 86
        },
        {
            "id": "b64566ad-b448-4a30-a461-626b04dbf2ed",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 87
        },
        {
            "id": "204b7e1d-e569-4f10-8892-f1a0657c75ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 89
        },
        {
            "id": "4443c88b-051e-4a1e-9d10-4e1a780a1494",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 76,
            "second": 8217
        },
        {
            "id": "50776381-546a-45c4-959a-67bfe7e8ddd8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 44
        },
        {
            "id": "5524b880-6bb1-4920-a134-e48b802bf044",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 46
        },
        {
            "id": "99819563-cb36-4022-a155-33c285cec0a3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 80,
            "second": 65
        },
        {
            "id": "604fcd83-ef89-45a6-9c13-d5210a5e6469",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 44
        },
        {
            "id": "2a7f903f-1406-45d5-bc3d-fff54a379a5e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 45
        },
        {
            "id": "a7bf426a-ea7a-4040-bd31-253cc46d41b4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 46
        },
        {
            "id": "75289565-7db2-4f16-bf3c-b8e1c1cd17c1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 58
        },
        {
            "id": "fb47e311-00cb-45c2-aec3-b8a3343ef9ea",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 59
        },
        {
            "id": "940dee85-f3c0-47d8-b2db-c532aeb6f843",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 65
        },
        {
            "id": "a3dd0797-6b92-4103-b7f0-cd711a5963b5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 97
        },
        {
            "id": "ac2a1d81-38df-4c11-8e6e-c6ab265bb5f8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 99
        },
        {
            "id": "99ce3e3c-cee6-4969-8a3b-56d288430c67",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 101
        },
        {
            "id": "259eebc2-5032-4762-86b8-e6bac9b4d4b2",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 111
        },
        {
            "id": "762e09f8-0d15-4e2c-a807-529196debaf9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 115
        },
        {
            "id": "8fa61c29-0031-4629-876b-1c76904beeae",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 119
        },
        {
            "id": "2b6b2fee-ceda-4e3f-a443-39f174241c43",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 121
        },
        {
            "id": "da63173c-28e8-4547-8b59-f4e572cfbdc8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 173
        },
        {
            "id": "a3103ead-c382-4fe6-8d76-1cd724b7026c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 84,
            "second": 894
        },
        {
            "id": "d5e2fddc-e5ea-48bc-972a-4bc2009a5bbe",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 44
        },
        {
            "id": "2e4b35c2-4c66-494a-9d2f-af02bb5cdcd6",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 45
        },
        {
            "id": "d679f41a-4f85-4f76-b281-51e9b1c7cc9d",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 46
        },
        {
            "id": "582bc15e-bcfd-47ca-a857-837736d8b8c3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 65
        },
        {
            "id": "5760d231-579c-4acd-8df3-80637bc3e603",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 97
        },
        {
            "id": "f6940949-8e5d-4916-9a41-6f3e2d7a88c9",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 101
        },
        {
            "id": "88af57c1-c267-4424-ba8f-a03194472e04",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 111
        },
        {
            "id": "bc35d61e-d556-4f1e-87c7-89844ca5f032",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 86,
            "second": 173
        },
        {
            "id": "32d6d873-5b27-4b71-b71f-82a8fd191a5c",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 44
        },
        {
            "id": "05bfee1b-cc22-4a21-9f18-871b8df7d215",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 87,
            "second": 46
        },
        {
            "id": "f412e3da-e11b-4ea7-84c7-5c5fc726a479",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 44
        },
        {
            "id": "253c5454-6ec8-4124-bd0c-61f9cd8a5ed3",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 45
        },
        {
            "id": "817aae98-0707-45c8-837e-5c66bfff1673",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 46
        },
        {
            "id": "721efc66-5d86-4ce8-9ed7-c597b36dbffb",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 58
        },
        {
            "id": "856fa28e-1d02-4257-b970-9faf9a826be5",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 59
        },
        {
            "id": "887667c2-1779-453b-86e2-d82a053b0cb0",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 65
        },
        {
            "id": "52086478-012e-4abe-9334-c15a73755045",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 97
        },
        {
            "id": "320b4b58-4b12-44d5-8fdb-9a93cd1c20f7",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 101
        },
        {
            "id": "2b64d054-91cd-4e6a-ad02-7a13f685da77",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 111
        },
        {
            "id": "d786c1f2-f1b4-47e5-b71f-22f3f4ba6a2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 112
        },
        {
            "id": "bd9fc98a-3ada-4198-8478-a914cc18f009",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 113
        },
        {
            "id": "8118a442-c36e-45a2-ab87-2c238e3b6d54",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 117
        },
        {
            "id": "577c45fe-a07b-4971-bcf1-0ba56d6bb03f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 118
        },
        {
            "id": "6924ca9a-6c5f-430f-80b9-e130cda43e51",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 173
        },
        {
            "id": "70a4b870-2a3a-4a79-a9ca-f97706c707cd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 89,
            "second": 894
        },
        {
            "id": "cc825803-2833-47b0-97db-ccb8b99c9fba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 44
        },
        {
            "id": "0505fad1-6994-4dfb-81b2-d71ff4a3dadd",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 114,
            "second": 46
        },
        {
            "id": "358917e2-7e58-4f7e-b4b6-f6b03f12e833",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 44
        },
        {
            "id": "3dca3a29-aafb-4c78-a674-16922042d965",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 118,
            "second": 46
        },
        {
            "id": "e591d1d8-f526-4c6b-90d2-aabfd5c29fa8",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 44
        },
        {
            "id": "f8d173aa-1d25-49b4-a1b5-d88c15b6a192",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 119,
            "second": 46
        },
        {
            "id": "37fd8216-0d61-43dd-8977-631078fe9e2f",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 44
        },
        {
            "id": "73891fee-3abc-4a81-b71f-2fcd3f9c9389",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 121,
            "second": 46
        },
        {
            "id": "5e22ee26-3202-4d0e-9cf8-bd154d50103e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 65
        },
        {
            "id": "f9f6572b-9863-4c0c-8c85-2b5c5eab7dd4",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 902
        },
        {
            "id": "ad6cf522-7155-4802-a3b2-406453d3796e",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 913
        },
        {
            "id": "3fc361b8-c4ef-4aca-b54c-326e7f4608f1",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 916
        },
        {
            "id": "a7dff0a0-60f7-47b8-8780-c182536577ba",
            "modelName": "GMKerningPair",
            "mvc": "1.0",
            "amount": -1,
            "first": 160,
            "second": 923
        }
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 12,
    "styleName": "Regular",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}