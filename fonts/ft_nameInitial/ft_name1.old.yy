{
    "id": "2cc70deb-0d96-49c6-bbb9-1db42ad40690",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "ft_name",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Tahoma",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "b305bb4c-bd17-4d98-918a-785e9be011f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "320608c0-1926-4091-a7fa-7d2c99513baa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 470,
                "y": 98
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "e78f9cc8-7107-4e5c-a38a-fc01b07bacfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 478,
                "y": 98
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cb38f262-62be-47b3-b1f1-e3b7431f0b32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 30,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 490,
                "y": 98
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "e77216a0-bce1-4cfa-bc84-2f5d8aa49d9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 130
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "13a2d189-65a9-4757-bf65-31f1adbb2c7a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 30,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 18,
                "y": 130
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "169a40aa-36db-4868-a951-4dbac11ba557",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 19,
                "x": 47,
                "y": 130
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "25e76fc9-4091-42c3-80e0-dd0243470c2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 68,
                "y": 130
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "34d73832-0a1f-488d-8f24-bfcf057d5384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 75,
                "y": 130
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "24a8db6a-7c08-4a93-9ad4-16db386e2ec1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 30,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 130
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "563d373e-d5d7-4c4a-89af-3d19a1270ca0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 114,
                "y": 130
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "35b0481b-5a0e-48ef-a26f-f9240370793b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 250,
                "y": 130
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8d6c54fe-c620-40b0-8800-fb148cdc8dd8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 30,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 129,
                "y": 130
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "35ca7252-6ea0-4c15-9840-b73c1a2f805c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 139,
                "y": 130
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "77f2983e-0fb1-4626-84ee-76bbc391db50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 150,
                "y": 130
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "a146bf42-ae6f-4427-97f7-f709a636fb00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 157,
                "y": 130
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "3c8e62dc-b003-4df6-b679-b1132a27fe6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 170,
                "y": 130
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a7bd3d93-43b2-4a6a-93de-354a23e2c243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 30,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 187,
                "y": 130
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "e734240a-cd00-42f8-8f89-a72428a5cd89",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 201,
                "y": 130
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "c098d8f2-88f2-4aeb-b1a0-5418d2d86956",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 217,
                "y": 130
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "76e2276d-4b3a-4702-9041-2e0c600bf982",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 233,
                "y": 130
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "c942914f-2033-42e0-95bb-d0a186aa7b81",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 454,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "e70b028a-e2cf-4399-bfef-64fc91d57d9b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 98,
                "y": 130
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "1fe4f0a9-c784-4ba3-b232-f5b37fcb534c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 438,
                "y": 98
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "9712201e-a44f-4743-a60c-f4607d2a4366",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 220,
                "y": 98
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "972c4527-4687-4f9a-829b-df57be11a33f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 76,
                "y": 98
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "2e038c3f-e8fa-492a-bd82-ef3c0cf339fe",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 93,
                "y": 98
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "ab51101f-692f-4b92-b583-a39c9f06a81a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 30,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 100,
                "y": 98
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "a0715f86-b6e9-4372-95f6-4870810fce64",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 110,
                "y": 98
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "0202e616-74b6-4366-82b2-8639427e1026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 128,
                "y": 98
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "bff6d85a-182f-4c9d-8a15-0bc84e9e37cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 146,
                "y": 98
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "414db01b-589d-4a7d-9e14-20154f843cd2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 164,
                "y": 98
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "558f8155-aab6-4389-bbe9-4fb5b6a7cf60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 30,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 178,
                "y": 98
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "813811eb-14eb-474e-add5-dc0c3b2a9d54",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 200,
                "y": 98
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "c4e37b37-4b83-436f-aaad-5552cba1ff3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 237,
                "y": 98
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "31f21c2a-8d93-4cb0-9a88-3bcbd98b3f16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 399,
                "y": 98
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b9420b01-b47d-4d72-b4d3-f340d21c8c25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 254,
                "y": 98
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "26b559df-b46d-41d4-97b2-7626ef18c3c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 273,
                "y": 98
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "0ce0186d-7926-49e7-ab40-916c9cc1c7b2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 288,
                "y": 98
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "6b986de0-d0be-4eb4-b755-e95f98907d6c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 303,
                "y": 98
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "1d37117b-11c3-4f14-ad71-0ac1fcbb672e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 322,
                "y": 98
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "19bbd878-0e54-4ab4-a1e1-ca749f66f88f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 340,
                "y": 98
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "7d2fc29b-8500-421e-b218-c02025f23a51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 352,
                "y": 98
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "3ce24543-3898-4b2b-aa7b-573a2388850c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 365,
                "y": 98
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "f8e14d19-7ac3-4522-9e3c-833ad77ef2c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 384,
                "y": 98
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "00bf34da-7134-4821-9155-056e0680600e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 30,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 417,
                "y": 98
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "a4acf80a-76f0-4c50-9e57-734246d4f079",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 268,
                "y": 130
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "734418a1-f685-4ed4-ae7f-20da738f2e1a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 286,
                "y": 130
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "65840ce1-1799-421e-ad5d-0dfb5ef35ecf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 306,
                "y": 130
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "ae8c45b4-526f-46f4-8e5e-98ad8a453fbc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 170,
                "y": 162
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "b6a19e4d-c59c-4d3f-8212-910cd800ca0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 190,
                "y": 162
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "67661599-59ee-49e3-bd0c-cec0005ccee9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 209,
                "y": 162
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "46131ae3-b1e7-49a9-835e-b35b04ba951b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 226,
                "y": 162
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "b8060762-9819-4fed-8d0e-79640b3da1c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 243,
                "y": 162
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "a5dc1a4e-baf9-4903-935a-28a5ec1820cb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 261,
                "y": 162
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "240f8b1c-808f-4387-8ff1-3fe38b29170d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 30,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 281,
                "y": 162
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "b54261a0-015c-44b2-a785-bfb6bd85cb41",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 308,
                "y": 162
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "b43e2410-fb54-465f-8b3a-c84e4a87868c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 328,
                "y": 162
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "4c3cddb7-26b8-4ebb-891a-d241ee88e3c6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 348,
                "y": 162
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "586ba606-baf8-4cc7-ad0e-cc19d0d2a48b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 365,
                "y": 162
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "126eba86-c706-4d64-8b8a-90fe33c84df9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 376,
                "y": 162
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "58e7724b-3f09-47d5-b78d-43fee90e1d0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 389,
                "y": 162
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "0fcc878c-1280-45de-a45a-db804eacb62f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 399,
                "y": 162
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "9e9b5e7c-aaa8-4f84-b3bb-c269ec6946fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 417,
                "y": 162
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "4158a739-3c29-47b5-a9dc-d349be7d290b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 30,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 436,
                "y": 162
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "ab308346-730f-4386-a117-35ea580ac862",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 445,
                "y": 162
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "904a5e04-5085-4df9-bb41-7d4dc50f05e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 460,
                "y": 162
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d5c13665-80d2-4850-aca9-8367476c1e3c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 476,
                "y": 162
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "311ae3da-12f4-43f3-8aca-a6a675cdeb43",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 491,
                "y": 162
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "dec27ae3-8d62-41f3-8404-0fe2e3fade36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 194
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "703f8a31-3d3c-4286-aa3a-4e2ab7e13aa5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 30,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 158,
                "y": 162
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "ee0f1a0f-f724-4fe0-ac77-38b3de6a7ab5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 142,
                "y": 162
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a35bf88f-be63-4e91-80a9-5f1b2a0a829b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 127,
                "y": 162
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "d0683a04-6aff-4253-bdcd-c803e335c355",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 456,
                "y": 130
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "64ad3fa4-c88d-4b7c-ab3a-f92acda20b3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 30,
                "offset": -1,
                "shift": 9,
                "w": 9,
                "x": 323,
                "y": 130
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "34b66e1c-00ed-43f5-8ed2-0f67943fc56b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 334,
                "y": 130
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "bebc10cd-f716-45dc-bc01-4004b37bba3a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 350,
                "y": 130
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "2efad23f-80ff-45bc-8013-c27492f655a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 30,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 357,
                "y": 130
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "626ab344-5e22-4dcc-8275-2f39ab5ac612",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 380,
                "y": 130
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "13a123ed-292a-499d-b87d-205a76752ad0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 395,
                "y": 130
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "c819a211-8481-4d2e-be05-994aa05702ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 412,
                "y": 130
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "a5e8932f-225a-4a11-9a85-ed92bb571e7d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 428,
                "y": 130
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "8f6bfed0-9028-424d-8a5f-a4086d618f3f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 444,
                "y": 130
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "4796b72e-4820-426c-bd07-8f155ab721ad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 463,
                "y": 130
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a3c69478-4d11-4e45-84f2-b2118d38cbd7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 30,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 115,
                "y": 162
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "1481223b-520c-4e6f-b344-5418561e3707",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 477,
                "y": 130
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "983f4157-c687-46df-8772-43ff760b250e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 492,
                "y": 130
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "09aae0ec-909b-42bb-b2f1-ff3500198068",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 30,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 2,
                "y": 162
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "011f2608-9090-474b-b990-b7310d577377",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 26,
                "y": 162
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "4a9830d4-c675-42cc-ae82-09d447427b4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 44,
                "y": 162
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "ee490a89-679f-4038-ad18-ac5fec52002f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 61,
                "y": 162
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "f1ec3e9e-19d2-4c6d-9a99-d3023e8241ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 76,
                "y": 162
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "14516c70-772e-4031-b031-2cddce5403ce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 30,
                "offset": 5,
                "shift": 15,
                "w": 5,
                "x": 92,
                "y": 162
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f8144586-d0ba-4ab6-a361-6532f28adfad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 99,
                "y": 162
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "dccfd806-7908-484d-b2b8-7eb03727d6bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 30,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 56,
                "y": 98
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "177fbba1-6212-40f8-9c0d-f7cda0bf3cea",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 0,
                "x": 54,
                "y": 98
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "6d0939ea-8d8b-4f60-aa5d-8d062315a6fd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 46,
                "y": 98
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "d422bdaf-5764-47c9-8e88-cd985331d442",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 241,
                "y": 34
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "fad59f75-89e6-4847-aa5f-774741c64839",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 429,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "d1f64383-c462-4b27-9e34-6bbb5ab0c0b0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 445,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "7bc39ea9-5290-451c-9f44-504a98ffcb58",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 460,
                "y": 2
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "de9db66a-9ece-436a-8738-faf1f7ed5932",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 30,
                "offset": 5,
                "shift": 15,
                "w": 5,
                "x": 478,
                "y": 2
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "bc38a634-4b9d-44e9-b509-0721fe6fb101",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 485,
                "y": 2
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "c4e7d242-4a48-48bf-b077-ba2d592b5689",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "487edbd7-0dbf-4a0d-aef3-52209cdffba4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 30,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 14,
                "y": 34
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "303e63e1-3a4c-4020-8296-25bf8d187612",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 37,
                "y": 34
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "4d79f1db-98f0-405d-a4af-0cd8ecd144c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 50,
                "y": 34
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "33f1e112-ef67-4736-8a02-9e072f33c0d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 75,
                "y": 34
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "ce378a3e-df71-459b-803c-e2237358b420",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 230,
                "y": 34
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "daf0d571-0702-4545-ad12-35ab342d4fca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 30,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 93,
                "y": 34
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "b8288bcc-325a-44f5-8cce-6b1c08cba484",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 116,
                "y": 34
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "faeea488-deba-4931-86a4-5c566686cdda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 135,
                "y": 34
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "9213f9e2-16a4-4891-9511-ba1402bf7619",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 147,
                "y": 34
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "be26c04a-11cb-46f5-9b05-904b63ee787b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 165,
                "y": 34
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "1fdcf72e-af35-40fb-9ba0-614fdda68d6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 177,
                "y": 34
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "3193ad1f-6971-4b9d-aef9-9932eaff348d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 30,
                "offset": 4,
                "shift": 13,
                "w": 7,
                "x": 189,
                "y": 34
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "f3346ebf-3148-48ae-b3a3-95b9e7abbf53",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 198,
                "y": 34
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "a1938d1a-1789-4ffd-914f-eaf67d6fa026",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 214,
                "y": 34
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "0058e068-a01e-455b-a6cc-8e6f6cec253d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 422,
                "y": 2
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "436f1ba1-ad7b-4086-a486-6d654d895ab1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 30,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 66,
                "y": 34
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "4cf1e68d-c458-4866-abf9-2226fb55e535",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "a8e251dd-8953-42b8-b866-adbe2d8a0d03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "05b1d622-045d-441c-aff6-259a42a31670",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 30,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "ba61fbe9-0b4b-4b67-aeef-c0562ce777d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 30,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "ae1befff-97bc-496a-8f13-cbe28b271928",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 30,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "4cc355ae-f97a-4ec5-a430-19636f1c4291",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 30,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "82bbc246-9eaf-47e3-bbd5-0b902cb23cb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "95685451-7d38-4e8d-bc05-888a22292c80",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "3ed03431-27b2-4970-94a9-f7ba8adf68ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "dbb2a9c5-592c-43f9-8c91-42c2eac540e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "8fbad373-b0ee-4c0a-b542-3d0329846b17",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "0cc552e7-c8ff-480b-b929-bfbc09a53b25",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "f35dce87-4a80-4de7-8462-7f232748e52f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 379,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "7c1b3118-21a6-4f2a-a4e6-1bac750cd27c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 30,
                "offset": -1,
                "shift": 24,
                "w": 24,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "3d6fcd36-99d4-4f72-bc55-9980e0dba0f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 264,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "6abd21bf-356b-4b37-a196-1304d3baa5d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 282,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "db4a2bb3-1ebb-4264-9f8f-0a622014d968",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 297,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "38e171d0-b1ff-432c-aefe-d5f138d319d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 312,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "9128167c-3c16-4609-8b04-66c69e5413b9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 327,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "37826a10-88d7-4c36-b7e2-d66f2cd5e23a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "ef5d3dc8-911d-44fe-803a-bb551b0a7351",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 354,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "bf0dc432-3cc6-48d2-a96a-fdb9b4d39b2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 366,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "36625629-7bad-4b9d-bb45-65fd5b7701bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 399,
                "y": 2
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "d1ba96e1-f475-4aea-989e-ca9dc3d59ee2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 30,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 256,
                "y": 34
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "6e2f837c-b3ed-4f33-88cb-20594e4eb2b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 112,
                "y": 66
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "57a406bc-6e3d-42d8-8853-8d1048be0628",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 276,
                "y": 34
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "2e2153dc-4f58-4968-ad13-e4eab037583f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 161,
                "y": 66
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "ba71a4f6-ed62-4f74-9f55-74fea9ae3c4a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 181,
                "y": 66
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "99ba8b8a-4116-4727-b1a4-f77e6d85f42f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 201,
                "y": 66
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "c7373bfd-97d4-4467-a7d8-0f218da2d8a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 221,
                "y": 66
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "c33d3f52-1b71-40d2-9da2-e09b6a452292",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 15,
                "x": 241,
                "y": 66
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "1164822e-a0fe-43a7-a617-02ac8030d3bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 258,
                "y": 66
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "452b5a79-c1a8-45dc-9bf1-060a568fde5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 278,
                "y": 66
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "31b0736c-5c15-4407-8870-1a12ac965dce",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 296,
                "y": 66
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "37694512-e130-4efc-b36f-dd3009143d42",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 314,
                "y": 66
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "c1521a9b-a88f-4a69-aa63-b1b5c7d6e25a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 348,
                "y": 66
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "3b3cab5e-3b85-42c3-8c04-2768d268911c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 26,
                "y": 98
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "a579c170-fe45-4840-9717-20a499818572",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 366,
                "y": 66
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "84ee06b5-ef22-4751-869f-ca14cb522bb8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 383,
                "y": 66
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "ae5bb178-4ca1-42dc-b85c-e6c733d7ac24",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 399,
                "y": 66
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "c54b9f97-5438-435a-9a98-e79efa64afda",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 414,
                "y": 66
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "14355500-1989-4e45-80f4-65ffba7fc8d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 429,
                "y": 66
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "f34d55f6-177b-4995-847b-9dcbaf595866",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 444,
                "y": 66
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "938d7471-dc4f-4967-ba2c-576918979653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 459,
                "y": 66
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "d601cfd2-2f10-4d80-ad7d-a021adfe5826",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 474,
                "y": 66
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "5d0aac17-6536-4da6-8d42-16370c7f7bd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 30,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "50a0b6d2-bfc8-4433-8b0c-3c1cb39aaaff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 146,
                "y": 66
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "8ea7084e-5874-42f5-a143-3304cbae73d3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 332,
                "y": 66
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "58bb79a0-a058-42af-91c6-300edfefdca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 130,
                "y": 66
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "f623cebe-4f1d-4259-bcb0-3ebb7e418852",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 418,
                "y": 34
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "29acb436-0a98-40b8-828c-35d5cc262cac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 296,
                "y": 34
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "c6bcb7dc-65df-4070-91e7-e6ff7a61f3ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 312,
                "y": 34
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "4468cf16-90a0-486e-a843-de2c3a705e62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 321,
                "y": 34
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "22c521a4-c376-44be-9975-295c3a8c2814",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 30,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 330,
                "y": 34
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "11957929-3e3c-4d39-899f-fa0ad6270dc7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 30,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 341,
                "y": 34
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "7e0a0056-fc88-4668-ac8f-9e3b0d9f0ddc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 352,
                "y": 34
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "f379b218-bfc5-41b4-acde-9068295181b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 369,
                "y": 34
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "37d1aec1-dc78-471d-80dc-23789e7ae211",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 384,
                "y": 34
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "50afaff8-a8af-40c7-9828-182218406106",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 401,
                "y": 34
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "0a262e03-bf59-4d94-898e-65c24647ba50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 434,
                "y": 34
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "020e6c8c-6d18-4a82-a233-16400045a161",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 95,
                "y": 66
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "e821a755-7914-4b27-a3fc-42506a478343",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 451,
                "y": 34
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "b8fa8819-7f68-4961-88ec-54b52e9c917f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 468,
                "y": 34
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "887a6bba-8615-4b6b-9c88-7b5b9f297027",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 486,
                "y": 34
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "25ee7948-bc9c-4a81-8fd8-83fa72a2cc0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 66
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "80c01e9e-06d6-43e2-8c1c-9c5d2b2d5740",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 17,
                "y": 66
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "04a08f4c-235e-4d20-ad78-e563229c8a2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 66
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "a6336f12-d53f-4f0b-a733-c8fa108b5d75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 47,
                "y": 66
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "bcd80176-d094-427e-aa6c-a044387b3d57",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 62,
                "y": 66
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "e228a56b-9f2f-48b3-bc21-5c6163bf7560",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 79,
                "y": 66
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "1f64275c-74ec-4571-bc32-19bb0b7572ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 18,
                "y": 194
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "0c4eb094-3368-4da4-9c0e-257cfc547319",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 30,
                "offset": 4,
                "shift": 23,
                "w": 15,
                "x": 35,
                "y": 194
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 18,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}