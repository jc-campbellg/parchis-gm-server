{
    "id": "e094e9d8-b1de-4f32-8651-0d6faa44eb92",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "ft_nameInitial",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 1,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Tahoma",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "e02a0621-e4a5-4cb8-8b1d-f75fa6740b7b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "a0aa518e-2b5c-4a01-b441-b3d79f254fc5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 470,
                "y": 98
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "aa0ad71c-5949-4d60-86c0-9c8a509a95a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 478,
                "y": 98
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "6409b623-d600-4bf2-86aa-e9cc4c48ac40",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 30,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 490,
                "y": 98
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "dc702808-24ae-46df-92c5-143498be3d04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 2,
                "y": 130
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "8569c86a-178d-45d9-b4df-9670007182e1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 30,
                "offset": 1,
                "shift": 29,
                "w": 27,
                "x": 18,
                "y": 130
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "8a869e75-d3bb-4189-81d8-d3d7694e6847",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 19,
                "x": 47,
                "y": 130
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "6e815660-abe4-4e95-8a44-c8083d612a32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 68,
                "y": 130
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "884090df-408a-40c7-b552-d54cc9f40f01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 75,
                "y": 130
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "673cd624-a2a3-40b6-86e4-8d9896c66897",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 30,
                "offset": 0,
                "shift": 11,
                "w": 10,
                "x": 86,
                "y": 130
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "537fde91-e9a2-449a-aaf3-807156ea9d50",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 114,
                "y": 130
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "9a4b3709-8685-4a80-8242-9ec643de502d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 250,
                "y": 130
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "8bf12963-b520-444d-90d3-3af4dc7321e7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 30,
                "offset": -1,
                "shift": 8,
                "w": 8,
                "x": 129,
                "y": 130
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "f8e40df7-9ed1-4549-9d9f-e4a940035115",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 139,
                "y": 130
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "96fbc41e-246c-4e6b-8520-8193c6a7452c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 5,
                "x": 150,
                "y": 130
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "249ab33d-d872-4ce4-9169-d4bc69b64bc0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 11,
                "x": 157,
                "y": 130
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "9557989f-bb9b-4cad-9312-cd9905ef2d0c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 170,
                "y": 130
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "a3324a40-c673-45ff-8609-390f99f28577",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 30,
                "offset": 2,
                "shift": 15,
                "w": 12,
                "x": 187,
                "y": 130
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "1326f494-8550-433d-82bc-113875249d35",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 201,
                "y": 130
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "079ad5c1-32a4-4c33-89c0-efe2851f7132",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 217,
                "y": 130
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "bf9d1546-83df-4191-bb58-4e26e302bff3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 233,
                "y": 130
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "5fa570e5-c63e-42f6-a302-99620898def0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 454,
                "y": 98
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "db0bc8de-52fd-4076-b356-4a4382e60cf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 98,
                "y": 130
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "e8fdb9b3-a755-4849-83ca-87654a312259",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 438,
                "y": 98
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "3b4f9306-6377-4529-8889-7465f4551536",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 220,
                "y": 98
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "47b18c4c-8da5-4654-9802-7f106104d6a8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 76,
                "y": 98
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "41c92e75-b0a2-4947-b443-687256cad945",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 93,
                "y": 98
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "97b69eed-b0b5-4db9-8e35-f8941ce44a30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 30,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 100,
                "y": 98
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "d47a13a3-161b-4dc8-8b53-62b31d63b908",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 110,
                "y": 98
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "bc81889d-6fd7-4302-ad64-0f15ae608bf2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 128,
                "y": 98
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "57bf56ee-8b40-4cb7-b7e2-5657b49f82a9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 146,
                "y": 98
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "218cdfa8-bb26-449b-a3af-79b893105aad",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 164,
                "y": 98
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "c35e904b-e5c5-427c-98b4-40866b251378",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 30,
                "offset": 1,
                "shift": 22,
                "w": 20,
                "x": 178,
                "y": 98
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "f92b5de6-ab87-4ec9-bdcc-3562145b7814",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 200,
                "y": 98
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "2349b67e-a12f-4769-b279-022a99849b6f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 237,
                "y": 98
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "a80a20e1-6448-43b8-809d-010de40a814e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 399,
                "y": 98
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "97f22cc7-fce1-489a-b6c5-59ef2aef053c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 254,
                "y": 98
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "60969f8d-5d5f-482f-9ba0-cd745dae008d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 273,
                "y": 98
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "2ff1478b-e5fb-40ed-aed4-6020c87835dc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 288,
                "y": 98
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "96d35384-57d7-4e17-bd6b-e71f10dd04ec",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 303,
                "y": 98
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "ec4dfcef-b472-4147-9432-66f32b41510c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 322,
                "y": 98
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "efefe517-260f-4b31-9213-18184b9d048a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 340,
                "y": 98
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "b2ab349e-a846-46ba-b215-27c0974f5c36",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 352,
                "y": 98
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "b3e5f1f8-d071-4d77-9686-47cf27593c49",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 365,
                "y": 98
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "577a9ca4-081d-4caf-871f-ebfc810f3d82",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 384,
                "y": 98
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "440d7ebb-e1de-4c82-a60d-d11eac587220",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 30,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 417,
                "y": 98
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "e8682c72-5ca5-4612-9403-bc7e42abbcbd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 268,
                "y": 130
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "fddc08bd-0be5-4752-9896-abcf3394a30d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 286,
                "y": 130
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "7edfd536-85b1-4420-a143-d63ec9b553bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 306,
                "y": 130
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "cf16f177-b3e5-414e-8b80-e5857670f1aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 170,
                "y": 162
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "fd570d5b-0b34-4340-a6a2-1eef214f70d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 190,
                "y": 162
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "6c5cdbcb-8e07-4ff2-a476-dcc4889b799c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 209,
                "y": 162
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "e7821e04-255b-432f-a2ab-1d4ce5dddee0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 226,
                "y": 162
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "2df6d111-b129-4ac3-8ec4-6feb13a678bb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 243,
                "y": 162
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "0e6a480f-01ff-401b-bb93-a9ce4506138e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 261,
                "y": 162
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "e9cfcd27-b75b-49d4-a3f9-67a9601e8eb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 30,
                "offset": 0,
                "shift": 25,
                "w": 25,
                "x": 281,
                "y": 162
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "76c2f006-b5f7-411a-bde5-927c35c5d2c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 308,
                "y": 162
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "50ba69a1-ce9d-4e0c-b46c-90194205d108",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 328,
                "y": 162
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "08bc182d-4959-486f-bf51-78b0ddde307d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 348,
                "y": 162
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "6ab8f5c1-9ece-4420-914d-4275ebb0060f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 9,
                "x": 365,
                "y": 162
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "de1aa055-651e-4fe5-adcd-029db5e99134",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 30,
                "offset": 2,
                "shift": 14,
                "w": 11,
                "x": 376,
                "y": 162
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "1d0844e7-957e-4d7f-8d91-12043c3c963a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 30,
                "offset": 1,
                "shift": 11,
                "w": 8,
                "x": 389,
                "y": 162
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "6300ca3e-1a40-42d4-8af0-8f886d8920f1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 399,
                "y": 162
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "a701210d-dfc1-4491-b46c-1f689297de60",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 417,
                "y": 162
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "873d1950-7607-43ef-9a53-25b838b2e696",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 30,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 436,
                "y": 162
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "1d9e9b9d-8d80-4870-9728-34cf52b117b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 445,
                "y": 162
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "5790a051-08d5-48ff-9d0a-e4916d6cad0e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 460,
                "y": 162
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "d3ce1c03-8dd8-4ea0-af39-1f413b20dbf5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 476,
                "y": 162
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "0a3b5f7d-03e3-4050-bb4a-ab2925c9ad5e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 491,
                "y": 162
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "1d59e23a-4cd7-48d8-9442-7e1b2514ff15",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 2,
                "y": 194
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "61040867-7622-4698-93aa-2f8af1d63fa3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 30,
                "offset": 0,
                "shift": 9,
                "w": 10,
                "x": 158,
                "y": 162
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "91759e04-7241-4e47-9849-84b40e1d1ee4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 142,
                "y": 162
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "a7afd061-9a06-4d08-8ea3-9b5a2f3f2f32",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 127,
                "y": 162
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "566ec3d4-ca3d-4cbc-ab84-e8a928fa8384",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 456,
                "y": 130
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "de26aebd-ae57-4b00-8e8f-76c553fb736f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 30,
                "offset": -1,
                "shift": 9,
                "w": 9,
                "x": 323,
                "y": 130
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "58e9595d-4619-4459-8a86-50e0227ca202",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 30,
                "offset": 1,
                "shift": 14,
                "w": 14,
                "x": 334,
                "y": 130
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "86f00713-c38a-47d0-b2d1-6c1bbd8008d6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 5,
                "x": 350,
                "y": 130
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "c44927c5-a2bf-4920-8950-54bd6128d172",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 30,
                "offset": 1,
                "shift": 23,
                "w": 21,
                "x": 357,
                "y": 130
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "737a09ba-3b06-4790-ab64-7430959a6850",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 380,
                "y": 130
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "14c56fe9-9e75-41ab-95cd-87afc75a6124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 395,
                "y": 130
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "401bcd3b-304c-44fd-b02f-1979df33bd5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 412,
                "y": 130
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "3c068bca-ab59-4183-b3d7-4e1fe796aa08",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 428,
                "y": 130
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "58b60d4f-1701-42be-a2e8-13fbd0d10de7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 10,
                "x": 444,
                "y": 130
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "ee204243-546b-44a6-9b67-de514494df6e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 463,
                "y": 130
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "a93555c0-552c-4995-8c0c-cc2d5d733f00",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 30,
                "offset": 0,
                "shift": 10,
                "w": 10,
                "x": 115,
                "y": 162
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "5d4aec4c-32c2-4d88-987c-4aad0bccced9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 477,
                "y": 130
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "0b0d1424-a4d3-438f-8566-98d513e4a779",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 492,
                "y": 130
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "60bb9080-3dd4-4ec3-9481-c46e66cd4d61",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 30,
                "offset": 0,
                "shift": 21,
                "w": 22,
                "x": 2,
                "y": 162
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "6039e77b-510b-49b3-ab4e-bfb06c46bb9e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 16,
                "x": 26,
                "y": 162
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "65f0b64e-bc3a-4a3b-bb0f-9b580b798566",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 44,
                "y": 162
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "44cc86b3-fdaa-4a38-85f2-3f22930b77f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 61,
                "y": 162
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "fa0689da-84af-4b45-9989-a74437ef5d2a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 76,
                "y": 162
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "43c4eede-3bfd-460c-837a-6e8c31d17f2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 30,
                "offset": 5,
                "shift": 15,
                "w": 5,
                "x": 92,
                "y": 162
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "a7ec2dfb-1d09-41f3-9ae7-ad6c7cc01a29",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 99,
                "y": 162
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "568e858f-8d17-47f4-bb3d-be016f2ce3c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 30,
                "offset": 1,
                "shift": 20,
                "w": 18,
                "x": 56,
                "y": 98
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "79af9ff7-01b5-4da9-ac6a-836c95f567f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 0,
                "x": 54,
                "y": 98
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "3caedaa7-39f8-4ba3-9528-e449ab2c72b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 30,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 46,
                "y": 98
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "d127e6d3-f736-491f-b3df-1611f0ba97b6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 241,
                "y": 34
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "c5102cc3-d2c3-47ba-88a7-552e5a32af73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 429,
                "y": 2
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "34289881-6381-4ae1-959a-876a4134ca1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 445,
                "y": 2
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "f9a1c682-e68d-4cad-898b-68269fa74ddf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 16,
                "x": 460,
                "y": 2
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "ca535c6d-6e1d-47d4-8094-04eb07de7a2c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 30,
                "offset": 5,
                "shift": 15,
                "w": 5,
                "x": 478,
                "y": 2
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "376b19c3-2767-49d4-ae51-46a76feda1c3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 485,
                "y": 2
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "5b0c4937-8095-4b2a-9fec-e0664d0d45ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 2,
                "y": 34
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "72441a15-50b1-4cfb-82d0-ad3f6891232d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 30,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 14,
                "y": 34
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "3d074fb1-9d07-42b5-bda2-b362aae1ce55",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 37,
                "y": 34
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "8e822fcb-7dc5-47a7-ac29-b2963f9960cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 30,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 50,
                "y": 34
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "33aa884e-beec-4b3b-b7e5-78d03bf05684",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 75,
                "y": 34
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "97e3effe-92b3-43fd-a559-85cf79ccf122",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 30,
                "offset": 1,
                "shift": 10,
                "w": 9,
                "x": 230,
                "y": 34
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "fd05872d-c9c9-4c19-bd9f-e3f3a5755a47",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 30,
                "offset": 1,
                "shift": 22,
                "w": 21,
                "x": 93,
                "y": 34
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "0a359a8b-090e-4861-b8aa-a63529f2207f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 30,
                "offset": -1,
                "shift": 15,
                "w": 17,
                "x": 116,
                "y": 34
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "0dff7d0d-68dc-482a-8665-d4f8ff206322",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 135,
                "y": 34
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "bd4c804b-81e3-4b7e-aace-2475c35f7555",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 147,
                "y": 34
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "0a1a40bb-d63d-4882-8ec6-d9a7788c2502",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 165,
                "y": 34
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "260df0b3-daa9-4383-b294-73dfaae0640c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 177,
                "y": 34
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "b24f6f1d-0c27-4ae8-a14e-cb082fef4533",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 30,
                "offset": 4,
                "shift": 13,
                "w": 7,
                "x": 189,
                "y": 34
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "8458e746-d8b9-40a6-b1d4-333753785465",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 198,
                "y": 34
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "6b202fd2-c1dc-4561-a374-3f206c47e06f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 14,
                "x": 214,
                "y": 34
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "3c755e93-f858-4363-811c-1b146b9b3b4c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 30,
                "offset": 2,
                "shift": 9,
                "w": 5,
                "x": 422,
                "y": 2
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "cbc6eea3-dbee-431a-bd3b-fb60266a0575",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 30,
                "offset": 3,
                "shift": 13,
                "w": 7,
                "x": 66,
                "y": 34
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "fd11c38c-72ee-429e-8690-1a1dbe90dd4f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 30,
                "offset": 2,
                "shift": 13,
                "w": 9,
                "x": 411,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "a56a9198-86b2-471d-a00a-9fb1aabc237a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 30,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 204,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "18cf65eb-bb08-4412-a43c-f358b787a17d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 30,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 11,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "0921f3c5-c773-4e4f-999c-a3c14fcd3dae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 30,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 27,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "eef080fb-178b-4466-a206-60230efa8aa2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 30,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 54,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "3fddf9af-52f8-4096-ad6a-2d05a9b13ca4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 30,
                "offset": 1,
                "shift": 27,
                "w": 26,
                "x": 81,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "932559c2-91b8-428f-82eb-d4a32cb67d04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 109,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "953e4fc7-664e-4097-97c6-849c402bd6aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 124,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "9f95251a-70f2-44a7-aad0-bd8122610aac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 144,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "cbeb7385-8321-4ffc-b1e7-4458a84c8fe3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 164,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "16fac849-6184-4a6c-b33f-2cca30fdddb6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 184,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "ba567261-499c-4862-9a51-a9b9d587f681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 218,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "174a8b2c-abdb-4e64-86c9-293a3da881da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 379,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "0b45a357-4dfb-4e03-82aa-121faf9ba7a1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 30,
                "offset": -1,
                "shift": 24,
                "w": 24,
                "x": 238,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "a5db3dcd-b86c-45dc-b0be-1cc699218be7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 30,
                "offset": 0,
                "shift": 16,
                "w": 16,
                "x": 264,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "09a3fcbc-e0ba-4cff-ab44-e47c1cefda5f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 282,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "78dd09ad-c914-4de4-9370-a4482d63683a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 297,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "4a2bc230-864d-4f50-a363-f4ef608e92e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 312,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "3a37a446-6661-42ca-bbd5-704385be4d16",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 327,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "f285cac6-712c-4984-9a1e-807684e2b19f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 342,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "0bd0d5a9-e122-41b9-b11a-046a847024fb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 354,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "54c45007-1b3b-4cbb-9dad-403470dd1083",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 30,
                "offset": 0,
                "shift": 12,
                "w": 11,
                "x": 366,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "a52beeb2-630e-4547-ace3-11a45df0d2d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 30,
                "offset": 1,
                "shift": 12,
                "w": 10,
                "x": 399,
                "y": 2
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "7fac54f4-37ff-46ff-a70b-a7f964e38687",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 30,
                "offset": 0,
                "shift": 19,
                "w": 18,
                "x": 256,
                "y": 34
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "c61f1943-3ebd-4174-b789-6c8af9572653",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 30,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 112,
                "y": 66
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "2a9c0031-158c-4da9-ab70-a83304a51238",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 276,
                "y": 34
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "52c6737e-ff23-4cd5-bff0-86f72151e20e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 161,
                "y": 66
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "697451a5-4a75-4a16-84a0-415245b43dd6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 181,
                "y": 66
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "fc77be85-0be5-47e3-9ffe-903ba1f62ff6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 201,
                "y": 66
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "8591728f-169c-4a10-bc9a-6986637cae62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 221,
                "y": 66
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "c38ec5cc-3556-4ad4-a2c9-1eac91574bd3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 15,
                "x": 241,
                "y": 66
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "edc4c560-6037-4bb8-8591-d45bab07185e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 30,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 258,
                "y": 66
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "1adb7f84-b664-4aae-9320-f5e8fe4c87ed",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 278,
                "y": 66
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "4181fa23-d2a7-488b-baf9-0b11b4ecb996",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 296,
                "y": 66
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "323424ab-be17-4956-a4dd-fbaa5579bad1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 314,
                "y": 66
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "c2d636c1-d040-4e2a-b24a-c08bb1743c0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 30,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 348,
                "y": 66
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "c3136c40-be33-456f-8751-cf3e6f4019cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 30,
                "offset": -1,
                "shift": 16,
                "w": 18,
                "x": 26,
                "y": 98
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "e36b320e-dbd1-4f02-9c7a-05649a1fd975",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 366,
                "y": 66
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "a56ca340-05a7-412c-b3ac-484dd9661640",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 30,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 383,
                "y": 66
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "3908be8e-873f-4f6e-aeac-9180a0e19d2d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 399,
                "y": 66
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "5f062774-5436-42d2-97c2-53ee9acf01f2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 414,
                "y": 66
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "eef44613-f447-4e82-8fdc-2481aa2355c4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 429,
                "y": 66
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "db1216b4-b272-4348-970c-dc0bcc201b5c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 444,
                "y": 66
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "a0b1b777-82ad-4e44-aec2-184e254b2fa9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 459,
                "y": 66
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "ac4448c5-e06e-4b38-8aff-d006a963bbb3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 13,
                "x": 474,
                "y": 66
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "a4004007-5e02-4dfc-b674-26a628cc8960",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 30,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 2,
                "y": 98
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "633f9082-925e-4884-a766-236554c34bd9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 30,
                "offset": 0,
                "shift": 13,
                "w": 13,
                "x": 146,
                "y": 66
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "2ac6ab6f-316f-43b2-ac71-433a2fd0039c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 332,
                "y": 66
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "12fb2623-3930-4841-8bb2-f45dc2081673",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 130,
                "y": 66
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "d01378fb-d6bf-487c-8f96-672776e05c8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 418,
                "y": 34
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "0532c58a-bc34-4cf2-9acf-2d95c2202a2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 30,
                "offset": 0,
                "shift": 14,
                "w": 14,
                "x": 296,
                "y": 34
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "1107b452-0daf-47e8-b3b3-d28a1b318607",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 30,
                "offset": 0,
                "shift": 7,
                "w": 7,
                "x": 312,
                "y": 34
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "f2e9087e-bc25-4272-a7f1-2018d0fedeac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 30,
                "offset": 1,
                "shift": 7,
                "w": 7,
                "x": 321,
                "y": 34
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "af8246f8-f5fc-46ec-a3a9-45930d11c8b3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 30,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 330,
                "y": 34
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "28e249f4-1595-4919-9f3a-41e193edda98",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 30,
                "offset": -1,
                "shift": 7,
                "w": 9,
                "x": 341,
                "y": 34
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "db98ed22-1d42-4990-9c44-75e4eb790c86",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 352,
                "y": 34
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "4f22800e-2129-475a-996c-b5542a7bc162",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 369,
                "y": 34
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "aaf13803-88f7-4bc4-9528-9df7e10f879b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 384,
                "y": 34
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "185b8ede-03b0-4805-a9ff-502199244dca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 401,
                "y": 34
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "a276fffb-d1cd-4f39-8729-607ac322f552",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 434,
                "y": 34
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "3af2210f-ad74-40df-a394-b4fc608a3ee6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 95,
                "y": 66
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "93b8903c-24aa-45fd-897f-7a2630561032",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 451,
                "y": 34
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "9f212405-b848-4a18-a8e4-03f6655cf54c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 30,
                "offset": 2,
                "shift": 20,
                "w": 16,
                "x": 468,
                "y": 34
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "f7b8380e-341f-4d02-be8a-aad90e17771c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 30,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 486,
                "y": 34
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "b0ebb1ad-ca75-4d8d-ad8a-1a136057c531",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 2,
                "y": 66
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "0194a844-d147-4df2-8049-28d60e0d36cf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 17,
                "y": 66
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "755af18a-b58c-436d-a1b9-17a4930c3966",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 32,
                "y": 66
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "5f5f71e8-4914-4ce5-be9c-bce8dbf3d70c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 47,
                "y": 66
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "af5e29b6-989e-4976-aefe-a3cb488e4124",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 62,
                "y": 66
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "0cda003d-e219-4854-a09f-5332ba07a7f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 30,
                "offset": 1,
                "shift": 15,
                "w": 14,
                "x": 79,
                "y": 66
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "d17440ae-3d99-44fe-ae39-c1855cff7f8a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 30,
                "offset": -1,
                "shift": 14,
                "w": 15,
                "x": 18,
                "y": 194
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "02b0e019-74e1-47d8-97a1-a36a43df5e46",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 30,
                "offset": 4,
                "shift": 23,
                "w": 15,
                "x": 35,
                "y": 194
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 18,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}