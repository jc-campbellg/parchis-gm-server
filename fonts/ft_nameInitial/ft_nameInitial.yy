{
    "id": "e094e9d8-b1de-4f32-8651-0d6faa44eb92",
    "modelName": "GMFont",
    "mvc": "1.1",
    "name": "ft_nameInitial",
    "AntiAlias": 1,
    "TTFName": "",
    "ascenderOffset": 2,
    "bold": true,
    "charset": 0,
    "first": 0,
    "fontName": "Tahoma",
    "glyphOperations": 0,
    "glyphs": [
        {
            "Key": 32,
            "Value": {
                "id": "5d169c8b-ee2b-4b1f-b870-947380c5ef33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 32,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 9,
                "x": 2,
                "y": 2
            }
        },
        {
            "Key": 33,
            "Value": {
                "id": "bb2367a1-16ca-4d43-8fd8-533f88f0f7e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 33,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 290,
                "y": 158
            }
        },
        {
            "Key": 34,
            "Value": {
                "id": "8ff6894e-ebe5-4589-bf87-c8235859afac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 34,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 298,
                "y": 158
            }
        },
        {
            "Key": 35,
            "Value": {
                "id": "cb5fa93a-4d7f-4d26-aeff-0d1f1b4ffa30",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 35,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 312,
                "y": 158
            }
        },
        {
            "Key": 36,
            "Value": {
                "id": "8cb70744-0441-4493-a511-d1e03587d6d5",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 36,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 335,
                "y": 158
            }
        },
        {
            "Key": 37,
            "Value": {
                "id": "ff99c7c7-de17-494e-9eab-15e66271dc3b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 37,
                "h": 37,
                "offset": 1,
                "shift": 35,
                "w": 33,
                "x": 354,
                "y": 158
            }
        },
        {
            "Key": 38,
            "Value": {
                "id": "450b10dd-041b-4dc7-a1b2-b08f6ec0fba9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 38,
                "h": 37,
                "offset": 1,
                "shift": 23,
                "w": 23,
                "x": 389,
                "y": 158
            }
        },
        {
            "Key": 39,
            "Value": {
                "id": "626e74bf-a957-474a-8949-131e3ce5f1ca",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 39,
                "h": 37,
                "offset": 1,
                "shift": 8,
                "w": 6,
                "x": 414,
                "y": 158
            }
        },
        {
            "Key": 40,
            "Value": {
                "id": "66640e0a-a272-4751-bfe2-a8ebe9dc905f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 40,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 422,
                "y": 158
            }
        },
        {
            "Key": 41,
            "Value": {
                "id": "29752cbf-f2ee-467f-870f-01cb05afefc3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 41,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 11,
                "x": 436,
                "y": 158
            }
        },
        {
            "Key": 42,
            "Value": {
                "id": "e2c612b2-b00c-4b93-9674-b2f25e151341",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 42,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 468,
                "y": 158
            }
        },
        {
            "Key": 43,
            "Value": {
                "id": "8f9387a6-55a7-4d8e-970f-dd2a91837768",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 43,
                "h": 37,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 119,
                "y": 197
            }
        },
        {
            "Key": 44,
            "Value": {
                "id": "46bb4e1e-0bc4-4044-ba6e-904126ebbfcc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 44,
                "h": 37,
                "offset": -1,
                "shift": 9,
                "w": 9,
                "x": 486,
                "y": 158
            }
        },
        {
            "Key": 45,
            "Value": {
                "id": "19fd5096-6258-4ea1-901f-3d6951a087ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 45,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 497,
                "y": 158
            }
        },
        {
            "Key": 46,
            "Value": {
                "id": "285ccdbe-8680-4791-b7c4-cfcd855c7799",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 46,
                "h": 37,
                "offset": 2,
                "shift": 9,
                "w": 6,
                "x": 2,
                "y": 197
            }
        },
        {
            "Key": 47,
            "Value": {
                "id": "b9a336f1-4e69-4099-927f-9701036d51e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 47,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 14,
                "x": 10,
                "y": 197
            }
        },
        {
            "Key": 48,
            "Value": {
                "id": "b5708246-edad-42f8-84ba-c4579835a135",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 48,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 26,
                "y": 197
            }
        },
        {
            "Key": 49,
            "Value": {
                "id": "b1d2592c-f6b2-4e69-978e-ecc974028f1e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 49,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 45,
                "y": 197
            }
        },
        {
            "Key": 50,
            "Value": {
                "id": "4b0a1667-3584-4b93-a62c-57cb4b409c33",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 50,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 16,
                "x": 62,
                "y": 197
            }
        },
        {
            "Key": 51,
            "Value": {
                "id": "e7541f84-53fd-4110-a48f-275aa417c9e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 51,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 80,
                "y": 197
            }
        },
        {
            "Key": 52,
            "Value": {
                "id": "33e53764-57b8-40df-8efc-75f9e56cce03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 52,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 99,
                "y": 197
            }
        },
        {
            "Key": 53,
            "Value": {
                "id": "b922e835-2d89-4ca8-9ce5-ef1a7c08318e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 53,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 271,
                "y": 158
            }
        },
        {
            "Key": 54,
            "Value": {
                "id": "efeda2df-f3a5-4366-999d-822846a3d6e0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 54,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 449,
                "y": 158
            }
        },
        {
            "Key": 55,
            "Value": {
                "id": "7eeba041-fff0-4e6a-97da-d63cfa5d77bf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 55,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 252,
                "y": 158
            }
        },
        {
            "Key": 56,
            "Value": {
                "id": "55f4111a-8925-4130-b4c7-2611317f044c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 56,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 2,
                "y": 158
            }
        },
        {
            "Key": 57,
            "Value": {
                "id": "5f94bfd9-125d-45c3-a81b-db74ba8a1886",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 57,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 336,
                "y": 119
            }
        },
        {
            "Key": 58,
            "Value": {
                "id": "5235a617-60fa-477f-8f45-e968966c2c26",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 58,
                "h": 37,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 356,
                "y": 119
            }
        },
        {
            "Key": 59,
            "Value": {
                "id": "aa4c2329-f14e-4378-a8e7-0e1d691e7d87",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 59,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 9,
                "x": 364,
                "y": 119
            }
        },
        {
            "Key": 60,
            "Value": {
                "id": "e2f58da2-44a8-49bc-a930-5df222d140dd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 60,
                "h": 37,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 375,
                "y": 119
            }
        },
        {
            "Key": 61,
            "Value": {
                "id": "affbaa52-fb3f-4066-b2d0-695e9f7096bd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 61,
                "h": 37,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 396,
                "y": 119
            }
        },
        {
            "Key": 62,
            "Value": {
                "id": "0cea1bec-5fe6-4f89-952b-a31c28d543fa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 62,
                "h": 37,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 416,
                "y": 119
            }
        },
        {
            "Key": 63,
            "Value": {
                "id": "f8d0b8c5-5f68-4f2d-a0b1-6c23686ab409",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 63,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 15,
                "x": 438,
                "y": 119
            }
        },
        {
            "Key": 64,
            "Value": {
                "id": "a8d57f70-36c6-4638-b5ec-05d1e15c3d1c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 64,
                "h": 37,
                "offset": 1,
                "shift": 27,
                "w": 24,
                "x": 455,
                "y": 119
            }
        },
        {
            "Key": 65,
            "Value": {
                "id": "c1764f79-3380-4fd3-8b0a-3abdfa1104f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 65,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 481,
                "y": 119
            }
        },
        {
            "Key": 66,
            "Value": {
                "id": "d99470af-504d-4e52-9f10-d7a15af4f259",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 66,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 18,
                "x": 22,
                "y": 158
            }
        },
        {
            "Key": 67,
            "Value": {
                "id": "fc3842da-23a4-474a-a554-c2f9cd5d039d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 67,
                "h": 37,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 207,
                "y": 158
            }
        },
        {
            "Key": 68,
            "Value": {
                "id": "b815db88-e481-42c8-861e-f5dc64cd7e28",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 68,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 20,
                "x": 42,
                "y": 158
            }
        },
        {
            "Key": 69,
            "Value": {
                "id": "16be8cc7-d0c5-4c1e-a13d-5efc61e880e3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 69,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 64,
                "y": 158
            }
        },
        {
            "Key": 70,
            "Value": {
                "id": "23326a79-ed23-4987-ac3a-c5871fe57234",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 70,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 81,
                "y": 158
            }
        },
        {
            "Key": 71,
            "Value": {
                "id": "bf17f669-c53f-4f4e-bd1a-65438851720b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 71,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 20,
                "x": 98,
                "y": 158
            }
        },
        {
            "Key": 72,
            "Value": {
                "id": "9799ec41-6bd8-4aae-ad40-73fc7d592c0a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 72,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 18,
                "x": 120,
                "y": 158
            }
        },
        {
            "Key": 73,
            "Value": {
                "id": "f58f10d1-a410-4b46-91e4-1914a60541a3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 73,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 140,
                "y": 158
            }
        },
        {
            "Key": 74,
            "Value": {
                "id": "30cfc8b7-6a05-4988-b7ba-bbb5c93243e9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 74,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 13,
                "x": 154,
                "y": 158
            }
        },
        {
            "Key": 75,
            "Value": {
                "id": "608f6dab-e375-47fb-ac57-db0fa61a4066",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 75,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 19,
                "x": 169,
                "y": 158
            }
        },
        {
            "Key": 76,
            "Value": {
                "id": "a0a156c0-2abe-46f9-ab63-b12e502f8a0d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 76,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 15,
                "x": 190,
                "y": 158
            }
        },
        {
            "Key": 77,
            "Value": {
                "id": "952e29a6-79b7-4a60-8f61-c138e61f9a03",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 77,
                "h": 37,
                "offset": 2,
                "shift": 26,
                "w": 22,
                "x": 228,
                "y": 158
            }
        },
        {
            "Key": 78,
            "Value": {
                "id": "910abf21-77de-436c-ba11-98959b5e9893",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 78,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 140,
                "y": 197
            }
        },
        {
            "Key": 79,
            "Value": {
                "id": "371876f9-048f-4088-93e3-990ec0426243",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 79,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 161,
                "y": 197
            }
        },
        {
            "Key": 80,
            "Value": {
                "id": "974e9ee3-e7e9-436e-98c6-71a32f9c4a6a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 80,
                "h": 37,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 185,
                "y": 197
            }
        },
        {
            "Key": 81,
            "Value": {
                "id": "e8f8b5cd-0f71-408d-a3c6-307b02f979a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 81,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 113,
                "y": 236
            }
        },
        {
            "Key": 82,
            "Value": {
                "id": "990e9234-3005-4c96-8b20-cdc4bcd8f05a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 82,
                "h": 37,
                "offset": 2,
                "shift": 21,
                "w": 20,
                "x": 137,
                "y": 236
            }
        },
        {
            "Key": 83,
            "Value": {
                "id": "aa5c6b4c-0129-4ddb-bcd5-b2e44728a2db",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 83,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 159,
                "y": 236
            }
        },
        {
            "Key": 84,
            "Value": {
                "id": "1fa8ee14-bdba-47cc-a182-89e47a443784",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 84,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 178,
                "y": 236
            }
        },
        {
            "Key": 85,
            "Value": {
                "id": "97bd3a22-7b59-43af-a7a4-fa309236d380",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 85,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 198,
                "y": 236
            }
        },
        {
            "Key": 86,
            "Value": {
                "id": "785a1ca6-dc23-4a31-a7cb-a7400332671a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 86,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 219,
                "y": 236
            }
        },
        {
            "Key": 87,
            "Value": {
                "id": "79ca0b16-1ea5-43a4-bff9-fedf68faa64d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 87,
                "h": 37,
                "offset": 0,
                "shift": 30,
                "w": 30,
                "x": 242,
                "y": 236
            }
        },
        {
            "Key": 88,
            "Value": {
                "id": "2f28a4b4-06db-41b8-9e5c-dcbf59949966",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 88,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 22,
                "x": 274,
                "y": 236
            }
        },
        {
            "Key": 89,
            "Value": {
                "id": "d535449f-e96a-4d6a-8fd1-8ea65139f52f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 89,
                "h": 37,
                "offset": -1,
                "shift": 19,
                "w": 21,
                "x": 298,
                "y": 236
            }
        },
        {
            "Key": 90,
            "Value": {
                "id": "be38f2dd-e9d4-4a52-907f-a67bc6701934",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 90,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 321,
                "y": 236
            }
        },
        {
            "Key": 91,
            "Value": {
                "id": "b39baa5e-90f6-4d51-9c41-f2a591da3253",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 91,
                "h": 37,
                "offset": 2,
                "shift": 13,
                "w": 10,
                "x": 341,
                "y": 236
            }
        },
        {
            "Key": 92,
            "Value": {
                "id": "c2d6ae91-3622-4e40-bba8-95ae4377ec75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 92,
                "h": 37,
                "offset": 2,
                "shift": 17,
                "w": 14,
                "x": 353,
                "y": 236
            }
        },
        {
            "Key": 93,
            "Value": {
                "id": "ccb46227-1d10-4bc8-b346-61b84077255b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 93,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 369,
                "y": 236
            }
        },
        {
            "Key": 94,
            "Value": {
                "id": "1dcc7362-d7fd-4359-89b6-21d443bd037c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 94,
                "h": 37,
                "offset": 2,
                "shift": 24,
                "w": 20,
                "x": 381,
                "y": 236
            }
        },
        {
            "Key": 95,
            "Value": {
                "id": "e698be53-da6d-4d52-b5b3-42128603d88e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 95,
                "h": 37,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 403,
                "y": 236
            }
        },
        {
            "Key": 96,
            "Value": {
                "id": "f7b0e361-2c13-49ad-b593-050002e4b6ba",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 96,
                "h": 37,
                "offset": 3,
                "shift": 16,
                "w": 9,
                "x": 425,
                "y": 236
            }
        },
        {
            "Key": 97,
            "Value": {
                "id": "cbe3bcd1-4e5f-4426-b608-6d7e9cd56c45",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 97,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 436,
                "y": 236
            }
        },
        {
            "Key": 98,
            "Value": {
                "id": "0f6c9c1c-20f5-4e53-ac24-c128dbea168a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 98,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 454,
                "y": 236
            }
        },
        {
            "Key": 99,
            "Value": {
                "id": "de19fffc-052f-41fa-8a68-e4e5f139536f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 99,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 473,
                "y": 236
            }
        },
        {
            "Key": 100,
            "Value": {
                "id": "7ec76d69-49f2-4dc6-ad9f-656665bc87a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 100,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 490,
                "y": 236
            }
        },
        {
            "Key": 101,
            "Value": {
                "id": "a7dbbf6b-e4f2-4a62-8d68-f1f5dbf28324",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 101,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 2,
                "y": 275
            }
        },
        {
            "Key": 102,
            "Value": {
                "id": "ea837728-a771-46c4-989c-ed91a3a1f792",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 102,
                "h": 37,
                "offset": 0,
                "shift": 11,
                "w": 13,
                "x": 98,
                "y": 236
            }
        },
        {
            "Key": 103,
            "Value": {
                "id": "658ddee8-62b5-463d-a3db-fe2adfd5e8da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 103,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 79,
                "y": 236
            }
        },
        {
            "Key": 104,
            "Value": {
                "id": "c2f227fe-bb17-405f-9768-f433a60029e8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 104,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 61,
                "y": 236
            }
        },
        {
            "Key": 105,
            "Value": {
                "id": "5acec518-7b7b-4444-a2d7-6c436b0cb715",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 105,
                "h": 37,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 361,
                "y": 197
            }
        },
        {
            "Key": 106,
            "Value": {
                "id": "d0b9e848-0a4f-43e0-b739-0f9445fd945d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 106,
                "h": 37,
                "offset": -2,
                "shift": 11,
                "w": 11,
                "x": 204,
                "y": 197
            }
        },
        {
            "Key": 107,
            "Value": {
                "id": "570c7081-6a5c-4b51-931a-3e59de641735",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 107,
                "h": 37,
                "offset": 1,
                "shift": 17,
                "w": 17,
                "x": 217,
                "y": 197
            }
        },
        {
            "Key": 108,
            "Value": {
                "id": "be965004-965f-49b3-807c-dbf5298f6b14",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 108,
                "h": 37,
                "offset": 1,
                "shift": 9,
                "w": 6,
                "x": 236,
                "y": 197
            }
        },
        {
            "Key": 109,
            "Value": {
                "id": "f97c2758-1653-4cce-b946-7e27c3f9d193",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 109,
                "h": 37,
                "offset": 1,
                "shift": 28,
                "w": 25,
                "x": 244,
                "y": 197
            }
        },
        {
            "Key": 110,
            "Value": {
                "id": "cb412799-2837-43bf-8b52-da7184652961",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 110,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 271,
                "y": 197
            }
        },
        {
            "Key": 111,
            "Value": {
                "id": "207e217a-59fb-41b0-a6a3-0fe23c6b1b04",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 111,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 289,
                "y": 197
            }
        },
        {
            "Key": 112,
            "Value": {
                "id": "6e0c921c-3a33-4d7c-a173-75d7e81d296b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 112,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 309,
                "y": 197
            }
        },
        {
            "Key": 113,
            "Value": {
                "id": "2c8a9418-cc50-428d-9c48-49ffe4fa7a78",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 113,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 328,
                "y": 197
            }
        },
        {
            "Key": 114,
            "Value": {
                "id": "24a41bc8-a542-4a66-9c91-a31672cba9c1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 114,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 12,
                "x": 347,
                "y": 197
            }
        },
        {
            "Key": 115,
            "Value": {
                "id": "68a4acd0-f929-4341-ae87-efbe7ab9224f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 115,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 369,
                "y": 197
            }
        },
        {
            "Key": 116,
            "Value": {
                "id": "7491070f-bd49-4cb7-98e1-1445e84769ae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 116,
                "h": 37,
                "offset": 0,
                "shift": 12,
                "w": 12,
                "x": 47,
                "y": 236
            }
        },
        {
            "Key": 117,
            "Value": {
                "id": "6932c498-d23b-4900-a6f3-df9c395cfa1d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 117,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 386,
                "y": 197
            }
        },
        {
            "Key": 118,
            "Value": {
                "id": "d294a080-6150-4d1d-86d8-a32c565c162d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 118,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 404,
                "y": 197
            }
        },
        {
            "Key": 119,
            "Value": {
                "id": "1e7c5469-0808-4c7b-8c49-55911943a9f4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 119,
                "h": 37,
                "offset": 0,
                "shift": 26,
                "w": 26,
                "x": 423,
                "y": 197
            }
        },
        {
            "Key": 120,
            "Value": {
                "id": "145299ce-cfb8-43df-af92-e9e557e78445",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 120,
                "h": 37,
                "offset": -1,
                "shift": 18,
                "w": 19,
                "x": 451,
                "y": 197
            }
        },
        {
            "Key": 121,
            "Value": {
                "id": "caacb489-04d1-4e13-964b-4a8b5a273159",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 121,
                "h": 37,
                "offset": -1,
                "shift": 17,
                "w": 18,
                "x": 472,
                "y": 197
            }
        },
        {
            "Key": 122,
            "Value": {
                "id": "a8e12309-ac4c-416e-a10a-a355dd00fa8f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 122,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 492,
                "y": 197
            }
        },
        {
            "Key": 123,
            "Value": {
                "id": "c0f8dee5-4bbf-43ca-af21-dae7ab1d8319",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 123,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 2,
                "y": 236
            }
        },
        {
            "Key": 124,
            "Value": {
                "id": "60edb751-df4d-42cb-81b9-c101199b8cd4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 124,
                "h": 37,
                "offset": 7,
                "shift": 18,
                "w": 5,
                "x": 21,
                "y": 236
            }
        },
        {
            "Key": 125,
            "Value": {
                "id": "f530981b-d0b7-4b80-8ad9-652343fcfd72",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 125,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 28,
                "y": 236
            }
        },
        {
            "Key": 126,
            "Value": {
                "id": "d37283dc-57de-43be-ad52-5f1b4a7e6e8b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 126,
                "h": 37,
                "offset": 1,
                "shift": 24,
                "w": 21,
                "x": 313,
                "y": 119
            }
        },
        {
            "Key": 160,
            "Value": {
                "id": "03826dd3-7445-458e-9c31-c8b1382650e2",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 160,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 0,
                "x": 311,
                "y": 119
            }
        },
        {
            "Key": 161,
            "Value": {
                "id": "1125b6b5-32c9-4a1c-b65c-3fe6e8889377",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 161,
                "h": 37,
                "offset": 2,
                "shift": 10,
                "w": 6,
                "x": 303,
                "y": 119
            }
        },
        {
            "Key": 162,
            "Value": {
                "id": "06367a54-2452-42a7-94e8-19b35e49440d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 162,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 364,
                "y": 41
            }
        },
        {
            "Key": 163,
            "Value": {
                "id": "1833ebfc-a145-4773-9fa4-a49bfb038cae",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 163,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 2,
                "y": 41
            }
        },
        {
            "Key": 164,
            "Value": {
                "id": "cf5a869b-fee8-49c0-8487-940ed671935c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 164,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 16,
                "x": 21,
                "y": 41
            }
        },
        {
            "Key": 165,
            "Value": {
                "id": "0f52b178-addf-4b63-8da1-e25b65f0791e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 165,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 19,
                "x": 39,
                "y": 41
            }
        },
        {
            "Key": 166,
            "Value": {
                "id": "b4a08ab1-6d50-465a-af5f-c78f77b0162e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 166,
                "h": 37,
                "offset": 7,
                "shift": 18,
                "w": 5,
                "x": 60,
                "y": 41
            }
        },
        {
            "Key": 167,
            "Value": {
                "id": "1634dffc-85fb-4cbd-9780-bde8059a630a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 167,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 67,
                "y": 41
            }
        },
        {
            "Key": 168,
            "Value": {
                "id": "bdb42f1a-4187-40ae-b69d-157d54fa83a6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 168,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 86,
                "y": 41
            }
        },
        {
            "Key": 169,
            "Value": {
                "id": "a87d8891-3e93-4d06-8a59-c308049038cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 169,
                "h": 37,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 100,
                "y": 41
            }
        },
        {
            "Key": 170,
            "Value": {
                "id": "11f2694b-ec80-41bf-b27f-25b2b8f0a328",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 170,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 12,
                "x": 127,
                "y": 41
            }
        },
        {
            "Key": 171,
            "Value": {
                "id": "fa411593-2602-44c7-bb27-79e14731402f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 171,
                "h": 37,
                "offset": 1,
                "shift": 20,
                "w": 17,
                "x": 141,
                "y": 41
            }
        },
        {
            "Key": 172,
            "Value": {
                "id": "5011cb56-e1fa-4285-b971-06e885a535b8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 172,
                "h": 37,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 170,
                "y": 41
            }
        },
        {
            "Key": 173,
            "Value": {
                "id": "50a9e09a-17ce-4771-83d2-c455c70fce70",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 173,
                "h": 37,
                "offset": 1,
                "shift": 13,
                "w": 10,
                "x": 352,
                "y": 41
            }
        },
        {
            "Key": 174,
            "Value": {
                "id": "73a2bc6b-f9c9-447b-ad7a-ae01b19d051c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 174,
                "h": 37,
                "offset": 1,
                "shift": 27,
                "w": 25,
                "x": 191,
                "y": 41
            }
        },
        {
            "Key": 175,
            "Value": {
                "id": "ff34a18b-2882-4c30-9eb5-797d04456fcb",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 175,
                "h": 37,
                "offset": -1,
                "shift": 18,
                "w": 20,
                "x": 218,
                "y": 41
            }
        },
        {
            "Key": 176,
            "Value": {
                "id": "221e79eb-c399-4e99-99d3-7f03c2313850",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 176,
                "h": 37,
                "offset": 1,
                "shift": 15,
                "w": 13,
                "x": 240,
                "y": 41
            }
        },
        {
            "Key": 177,
            "Value": {
                "id": "85860854-0317-42c9-bd10-1781c3088c0f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 177,
                "h": 37,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 255,
                "y": 41
            }
        },
        {
            "Key": 178,
            "Value": {
                "id": "f68839f1-368e-45cd-b5a2-40c67044b84e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 178,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 276,
                "y": 41
            }
        },
        {
            "Key": 179,
            "Value": {
                "id": "9858473f-4fca-4bd9-bde8-8cd35aa0a9df",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 179,
                "h": 37,
                "offset": 2,
                "shift": 16,
                "w": 12,
                "x": 290,
                "y": 41
            }
        },
        {
            "Key": 180,
            "Value": {
                "id": "31f7f757-99b7-4456-bbba-1902c593ecb9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 180,
                "h": 37,
                "offset": 4,
                "shift": 16,
                "w": 9,
                "x": 304,
                "y": 41
            }
        },
        {
            "Key": 181,
            "Value": {
                "id": "52828134-c136-4a96-a0f7-f86eea97a56a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 181,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 315,
                "y": 41
            }
        },
        {
            "Key": 182,
            "Value": {
                "id": "e845a67d-b4e7-4031-816d-e2422cf5d08a",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 182,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 17,
                "x": 333,
                "y": 41
            }
        },
        {
            "Key": 183,
            "Value": {
                "id": "b4d163f4-d67d-48d1-af71-6777676fd728",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 183,
                "h": 37,
                "offset": 2,
                "shift": 11,
                "w": 6,
                "x": 486,
                "y": 2
            }
        },
        {
            "Key": 184,
            "Value": {
                "id": "b6116f96-8294-4192-b3b1-dd3ba9d17344",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 184,
                "h": 37,
                "offset": 4,
                "shift": 16,
                "w": 8,
                "x": 160,
                "y": 41
            }
        },
        {
            "Key": 185,
            "Value": {
                "id": "4f740549-2723-4254-b878-91b2bd8b9dfc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 185,
                "h": 37,
                "offset": 3,
                "shift": 16,
                "w": 10,
                "x": 474,
                "y": 2
            }
        },
        {
            "Key": 186,
            "Value": {
                "id": "78d3b6ab-d967-4dd9-84df-b4046b4e6415",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 186,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 235,
                "y": 2
            }
        },
        {
            "Key": 187,
            "Value": {
                "id": "7e687653-2d71-4112-857e-79a922a01255",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 187,
                "h": 37,
                "offset": 2,
                "shift": 20,
                "w": 17,
                "x": 13,
                "y": 2
            }
        },
        {
            "Key": 188,
            "Value": {
                "id": "aa6876ef-c727-43e2-af24-00da66ee591b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 188,
                "h": 37,
                "offset": 2,
                "shift": 33,
                "w": 29,
                "x": 32,
                "y": 2
            }
        },
        {
            "Key": 189,
            "Value": {
                "id": "312077a3-48ff-4445-9866-d69eedb67f51",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 189,
                "h": 37,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 63,
                "y": 2
            }
        },
        {
            "Key": 190,
            "Value": {
                "id": "d3093d8e-9ea5-43b4-9d22-c9c188f04e73",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 190,
                "h": 37,
                "offset": 2,
                "shift": 33,
                "w": 30,
                "x": 95,
                "y": 2
            }
        },
        {
            "Key": 191,
            "Value": {
                "id": "fb83d2b5-bd6e-4296-8865-59f4f6da69d1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 191,
                "h": 37,
                "offset": 1,
                "shift": 16,
                "w": 14,
                "x": 127,
                "y": 2
            }
        },
        {
            "Key": 192,
            "Value": {
                "id": "f200ac14-a986-479f-ab14-26f097b1ebaa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 192,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 143,
                "y": 2
            }
        },
        {
            "Key": 193,
            "Value": {
                "id": "52b80148-8e22-441a-913f-545dc4543843",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 193,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 166,
                "y": 2
            }
        },
        {
            "Key": 194,
            "Value": {
                "id": "51878a5b-ec88-4c63-90aa-f24c115ad613",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 194,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 189,
                "y": 2
            }
        },
        {
            "Key": 195,
            "Value": {
                "id": "343079d6-7d9f-451b-812a-edfcf0f225b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 195,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 212,
                "y": 2
            }
        },
        {
            "Key": 196,
            "Value": {
                "id": "aaa484b7-3136-47ab-b04e-61edd94586bc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 196,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 251,
                "y": 2
            }
        },
        {
            "Key": 197,
            "Value": {
                "id": "ac83e5ea-e7f1-47ad-817f-ee8f34c907ac",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 197,
                "h": 37,
                "offset": -1,
                "shift": 20,
                "w": 21,
                "x": 437,
                "y": 2
            }
        },
        {
            "Key": 198,
            "Value": {
                "id": "3bdb56e3-c9e7-481a-b5a1-5e23649b241e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 198,
                "h": 37,
                "offset": -1,
                "shift": 29,
                "w": 29,
                "x": 274,
                "y": 2
            }
        },
        {
            "Key": 199,
            "Value": {
                "id": "e89d151a-fb6b-43e4-aa16-ede87da42110",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 199,
                "h": 37,
                "offset": 0,
                "shift": 19,
                "w": 19,
                "x": 305,
                "y": 2
            }
        },
        {
            "Key": 200,
            "Value": {
                "id": "ceae802b-6d08-4dc7-9d23-ec017262f960",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 200,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 326,
                "y": 2
            }
        },
        {
            "Key": 201,
            "Value": {
                "id": "56a18f8f-e2b3-404b-bbbc-0c0a37c48969",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 201,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 343,
                "y": 2
            }
        },
        {
            "Key": 202,
            "Value": {
                "id": "f5447adf-fc90-4adc-9bf1-49a7b007a08b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 202,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 360,
                "y": 2
            }
        },
        {
            "Key": 203,
            "Value": {
                "id": "e8d045a3-a972-4da2-a5f0-e18125df7466",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 203,
                "h": 37,
                "offset": 2,
                "shift": 18,
                "w": 15,
                "x": 377,
                "y": 2
            }
        },
        {
            "Key": 204,
            "Value": {
                "id": "1781ba78-fd75-4170-a71e-43cc0a8a8266",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 204,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 394,
                "y": 2
            }
        },
        {
            "Key": 205,
            "Value": {
                "id": "38d99aff-f233-4d43-8690-5d1af057798e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 205,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 408,
                "y": 2
            }
        },
        {
            "Key": 206,
            "Value": {
                "id": "ffd94597-7165-4586-b557-5df2da035338",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 206,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 13,
                "x": 422,
                "y": 2
            }
        },
        {
            "Key": 207,
            "Value": {
                "id": "f0434667-6e66-4703-8052-ba0b11760b9c",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 207,
                "h": 37,
                "offset": 1,
                "shift": 14,
                "w": 12,
                "x": 460,
                "y": 2
            }
        },
        {
            "Key": 208,
            "Value": {
                "id": "8ccf62fd-899f-4255-b3e3-e62b8c4be034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 208,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 381,
                "y": 41
            }
        },
        {
            "Key": 209,
            "Value": {
                "id": "603f5a42-fa5f-4d41-ac11-bdcbde24cb31",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 209,
                "h": 37,
                "offset": 2,
                "shift": 22,
                "w": 19,
                "x": 311,
                "y": 80
            }
        },
        {
            "Key": 210,
            "Value": {
                "id": "e2d2d586-11d1-4962-a1a4-1eb79fc0c035",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 210,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 405,
                "y": 41
            }
        },
        {
            "Key": 211,
            "Value": {
                "id": "5e33145b-b327-4767-843e-d7413c176bf3",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 211,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 368,
                "y": 80
            }
        },
        {
            "Key": 212,
            "Value": {
                "id": "435f7ab9-b20f-4d4c-bfe6-b14930010d21",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 212,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 392,
                "y": 80
            }
        },
        {
            "Key": 213,
            "Value": {
                "id": "842ad782-8508-4637-9ad7-c7f3f3e71a75",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 213,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 416,
                "y": 80
            }
        },
        {
            "Key": 214,
            "Value": {
                "id": "36af899d-5a77-4288-b47b-48faf6059676",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 214,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 440,
                "y": 80
            }
        },
        {
            "Key": 215,
            "Value": {
                "id": "b0318cea-1e28-4550-a15e-0c7c87758080",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 215,
                "h": 37,
                "offset": 3,
                "shift": 24,
                "w": 18,
                "x": 464,
                "y": 80
            }
        },
        {
            "Key": 216,
            "Value": {
                "id": "467734bb-2b4f-4bb4-8c92-c923d5583e84",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 216,
                "h": 37,
                "offset": 0,
                "shift": 22,
                "w": 22,
                "x": 484,
                "y": 80
            }
        },
        {
            "Key": 217,
            "Value": {
                "id": "520a252c-96a5-4c65-b6c9-aa813f8a956b",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 217,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 2,
                "y": 119
            }
        },
        {
            "Key": 218,
            "Value": {
                "id": "18410a8f-e775-4ade-9d4b-b77baa957322",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 218,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 23,
                "y": 119
            }
        },
        {
            "Key": 219,
            "Value": {
                "id": "fd13e70d-bb86-47dc-88cc-6c3b33730034",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 219,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 44,
                "y": 119
            }
        },
        {
            "Key": 220,
            "Value": {
                "id": "dcf75a80-bb6f-4e10-a2d0-8c00291d2513",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 220,
                "h": 37,
                "offset": 1,
                "shift": 21,
                "w": 19,
                "x": 84,
                "y": 119
            }
        },
        {
            "Key": 221,
            "Value": {
                "id": "2ce956c6-4f36-4fa0-afa4-cb65c0297654",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 221,
                "h": 37,
                "offset": -1,
                "shift": 19,
                "w": 21,
                "x": 280,
                "y": 119
            }
        },
        {
            "Key": 222,
            "Value": {
                "id": "414fd4bf-762f-4fd9-aaf0-3f6b6dad56c8",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 222,
                "h": 37,
                "offset": 2,
                "shift": 19,
                "w": 17,
                "x": 105,
                "y": 119
            }
        },
        {
            "Key": 223,
            "Value": {
                "id": "a4ee0211-1a3d-4546-9b20-58fefc3514fc",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 223,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 17,
                "x": 124,
                "y": 119
            }
        },
        {
            "Key": 224,
            "Value": {
                "id": "1f534add-9564-4561-8eff-1db4324337f6",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 224,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 143,
                "y": 119
            }
        },
        {
            "Key": 225,
            "Value": {
                "id": "f7146bb3-f8b9-4177-a319-6546df7f1eb1",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 225,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 161,
                "y": 119
            }
        },
        {
            "Key": 226,
            "Value": {
                "id": "5db8c5df-62b2-4a16-9a17-fd4b2b29ff48",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 226,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 179,
                "y": 119
            }
        },
        {
            "Key": 227,
            "Value": {
                "id": "e22c207b-3d08-4c6e-9cac-be883e69a8ee",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 227,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 197,
                "y": 119
            }
        },
        {
            "Key": 228,
            "Value": {
                "id": "d5d18b14-9806-42f5-956c-e61eed906681",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 228,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 215,
                "y": 119
            }
        },
        {
            "Key": 229,
            "Value": {
                "id": "bd4b207a-fe72-46a4-815e-6b78f5441abd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 229,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 16,
                "x": 233,
                "y": 119
            }
        },
        {
            "Key": 230,
            "Value": {
                "id": "b93f8aba-dbeb-40dc-b906-66f9573de6f9",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 230,
                "h": 37,
                "offset": 0,
                "shift": 27,
                "w": 27,
                "x": 251,
                "y": 119
            }
        },
        {
            "Key": 231,
            "Value": {
                "id": "41976cac-6e78-4482-8e51-6022a25b80aa",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 231,
                "h": 37,
                "offset": 0,
                "shift": 15,
                "w": 15,
                "x": 351,
                "y": 80
            }
        },
        {
            "Key": 232,
            "Value": {
                "id": "06002df4-891e-47ec-b893-fd86e25c2fc4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 232,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 65,
                "y": 119
            }
        },
        {
            "Key": 233,
            "Value": {
                "id": "c4942e0f-9382-42da-941b-b80dc14595cd",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 233,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 332,
                "y": 80
            }
        },
        {
            "Key": 234,
            "Value": {
                "id": "fc6a9d8f-39de-49fb-91de-f29b3dd03c2e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 234,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 80,
                "y": 80
            }
        },
        {
            "Key": 235,
            "Value": {
                "id": "4308d47f-8896-490c-9466-87896615e3b4",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 235,
                "h": 37,
                "offset": 0,
                "shift": 17,
                "w": 17,
                "x": 429,
                "y": 41
            }
        },
        {
            "Key": 236,
            "Value": {
                "id": "db79869f-5d52-4673-bcb0-f2014f0ff4a7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 236,
                "h": 37,
                "offset": 0,
                "shift": 9,
                "w": 8,
                "x": 448,
                "y": 41
            }
        },
        {
            "Key": 237,
            "Value": {
                "id": "786841eb-9378-43aa-8e53-7afabdc06580",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 237,
                "h": 37,
                "offset": 1,
                "shift": 9,
                "w": 8,
                "x": 458,
                "y": 41
            }
        },
        {
            "Key": 238,
            "Value": {
                "id": "8e1d7a66-05e0-4a0c-ab56-69c055639450",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 238,
                "h": 37,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 468,
                "y": 41
            }
        },
        {
            "Key": 239,
            "Value": {
                "id": "10abe8b9-3ea5-481a-9876-4a34436124da",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 239,
                "h": 37,
                "offset": -1,
                "shift": 9,
                "w": 11,
                "x": 481,
                "y": 41
            }
        },
        {
            "Key": 240,
            "Value": {
                "id": "635a5a32-f0ae-442d-ab31-5556f7108f39",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 240,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 2,
                "y": 80
            }
        },
        {
            "Key": 241,
            "Value": {
                "id": "891c364f-4707-41fe-bfc9-62683f740c62",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 241,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 22,
                "y": 80
            }
        },
        {
            "Key": 242,
            "Value": {
                "id": "11bd25bd-e90d-4623-a95d-b8c5f34becaf",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 242,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 40,
                "y": 80
            }
        },
        {
            "Key": 243,
            "Value": {
                "id": "72006b85-cf3f-4924-a927-2b43d459d037",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 243,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 60,
                "y": 80
            }
        },
        {
            "Key": 244,
            "Value": {
                "id": "775e70de-9ce1-476a-8a8b-28cab60023f0",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 244,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 99,
                "y": 80
            }
        },
        {
            "Key": 245,
            "Value": {
                "id": "97e0d5c5-03dc-4bb0-b445-d214215f1a6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 245,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 291,
                "y": 80
            }
        },
        {
            "Key": 246,
            "Value": {
                "id": "ef61bd17-3caa-4bfe-a41b-3d9e7f1fc630",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 246,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 119,
                "y": 80
            }
        },
        {
            "Key": 247,
            "Value": {
                "id": "02cff299-dea5-441e-9926-b4604bc7b061",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 247,
                "h": 37,
                "offset": 2,
                "shift": 24,
                "w": 19,
                "x": 139,
                "y": 80
            }
        },
        {
            "Key": 248,
            "Value": {
                "id": "adf60316-31de-4352-818b-47750c756bef",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 248,
                "h": 37,
                "offset": 0,
                "shift": 18,
                "w": 18,
                "x": 160,
                "y": 80
            }
        },
        {
            "Key": 249,
            "Value": {
                "id": "9d71c592-64b0-4467-993e-31b2977c768f",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 249,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 180,
                "y": 80
            }
        },
        {
            "Key": 250,
            "Value": {
                "id": "fa87d4ee-8e96-49f2-b6be-bfd3c54b04d7",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 250,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 198,
                "y": 80
            }
        },
        {
            "Key": 251,
            "Value": {
                "id": "8b54a052-4ee9-49c4-80f2-1b7da92a3e93",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 251,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 216,
                "y": 80
            }
        },
        {
            "Key": 252,
            "Value": {
                "id": "946960b7-4354-447d-89c9-ae43d3b733ff",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 252,
                "h": 37,
                "offset": 1,
                "shift": 19,
                "w": 16,
                "x": 234,
                "y": 80
            }
        },
        {
            "Key": 253,
            "Value": {
                "id": "e6d34d9e-205e-4776-b785-86908ff1ab3e",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 253,
                "h": 37,
                "offset": -1,
                "shift": 17,
                "w": 18,
                "x": 252,
                "y": 80
            }
        },
        {
            "Key": 254,
            "Value": {
                "id": "1622b18a-e966-4335-ab6f-102096b5ad6d",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 254,
                "h": 37,
                "offset": 1,
                "shift": 18,
                "w": 17,
                "x": 272,
                "y": 80
            }
        },
        {
            "Key": 255,
            "Value": {
                "id": "94b7714f-cdaf-4bb7-922c-d629a1ea6695",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 255,
                "h": 37,
                "offset": -1,
                "shift": 17,
                "w": 18,
                "x": 21,
                "y": 275
            }
        },
        {
            "Key": 9647,
            "Value": {
                "id": "deb577b5-29ed-4240-b2bb-6e56ace2ac01",
                "modelName": "GMGlyph",
                "mvc": "1.0",
                "character": 9647,
                "h": 37,
                "offset": 6,
                "shift": 28,
                "w": 18,
                "x": 41,
                "y": 275
            }
        }
    ],
    "hinting": 0,
    "includeTTF": false,
    "interpreter": 0,
    "italic": false,
    "kerningPairs": [
        
    ],
    "last": 0,
    "maintainGms1Font": false,
    "pointRounding": 0,
    "ranges": [
        {
            "x": 32,
            "y": 255
        },
        {
            "x": 9647,
            "y": 9647
        }
    ],
    "sampleText": "abcdef ABCDEF\\u000a0123456789 .,<>\"'&!?\\u000athe quick brown fox jumps over the lazy dog\\u000aTHE QUICK BROWN FOX JUMPS OVER THE LAZY DOG\\u000aDefault character: ▯ (9647)",
    "size": 22,
    "styleName": "Bold",
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f"
}