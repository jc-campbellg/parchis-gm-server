{
    "id": "5c26284f-5926-4476-8bfe-327fa670230d",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_piecePurple",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "101659b2-5722-478d-8ac9-69f063f5ffbf",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "5c26284f-5926-4476-8bfe-327fa670230d",
            "compositeImage": {
                "id": "1beffadb-9c7a-408d-9e07-2e177db2fad8",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "101659b2-5722-478d-8ac9-69f063f5ffbf",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d3cb8c92-9afc-42e1-9d35-8c90692b8a05",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "101659b2-5722-478d-8ac9-69f063f5ffbf",
                    "LayerId": "ddf013de-8112-4f6c-80ac-c779b5184f4d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "ddf013de-8112-4f6c-80ac-c779b5184f4d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "5c26284f-5926-4476-8bfe-327fa670230d",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 26
}