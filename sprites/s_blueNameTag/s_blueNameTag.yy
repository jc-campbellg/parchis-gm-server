{
    "id": "472375ab-ae14-45ae-acf2-d84da09c91f9",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_blueNameTag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 306,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "d33add17-7c5d-4412-bc01-0cf4af3d26c7",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "472375ab-ae14-45ae-acf2-d84da09c91f9",
            "compositeImage": {
                "id": "042878e0-e137-4dc8-9a76-d752d1f52b65",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "d33add17-7c5d-4412-bc01-0cf4af3d26c7",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "969763e4-cb96-40c1-80cc-895dfdee0421",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "d33add17-7c5d-4412-bc01-0cf4af3d26c7",
                    "LayerId": "5d2fbf27-4ea2-4af8-8795-943a2490f3f0"
                }
            ]
        },
        {
            "id": "62e579cf-0249-4aae-a346-10e0c70f88f8",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "472375ab-ae14-45ae-acf2-d84da09c91f9",
            "compositeImage": {
                "id": "5d778694-aa6a-4da8-8af5-c98727d6ac1d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "62e579cf-0249-4aae-a346-10e0c70f88f8",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "eb6b3eb3-d520-400f-970e-e21ba8d2e3be",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "62e579cf-0249-4aae-a346-10e0c70f88f8",
                    "LayerId": "5d2fbf27-4ea2-4af8-8795-943a2490f3f0"
                }
            ]
        },
        {
            "id": "cab57119-8701-446c-9947-e3d15e8e7b5b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "472375ab-ae14-45ae-acf2-d84da09c91f9",
            "compositeImage": {
                "id": "63822c22-d2ee-414e-8e23-7e435b2148e6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "cab57119-8701-446c-9947-e3d15e8e7b5b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "118dda07-3973-4b5f-9c8d-dec2b281a3f8",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "cab57119-8701-446c-9947-e3d15e8e7b5b",
                    "LayerId": "5d2fbf27-4ea2-4af8-8795-943a2490f3f0"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "5d2fbf27-4ea2-4af8-8795-943a2490f3f0",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "472375ab-ae14-45ae-acf2-d84da09c91f9",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 307,
    "xorig": 0,
    "yorig": 0
}