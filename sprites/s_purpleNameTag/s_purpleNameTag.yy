{
    "id": "1d24b181-9201-449c-9c4e-0555ee961453",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_purpleNameTag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 306,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "f5107d12-ae75-4eac-a803-6d61cf5d007e",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d24b181-9201-449c-9c4e-0555ee961453",
            "compositeImage": {
                "id": "64b1e177-2f98-4c44-b0e1-843f57ad9657",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "f5107d12-ae75-4eac-a803-6d61cf5d007e",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c27a5907-50e8-448b-aebb-ed5499a5d68f",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "f5107d12-ae75-4eac-a803-6d61cf5d007e",
                    "LayerId": "b777d2fc-c237-4161-bf6c-318709ec9ee7"
                }
            ]
        },
        {
            "id": "47b8802a-2463-4b24-b7f2-fbffb99efbda",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d24b181-9201-449c-9c4e-0555ee961453",
            "compositeImage": {
                "id": "2506a760-cf2d-4980-b935-3a41c6de493b",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "47b8802a-2463-4b24-b7f2-fbffb99efbda",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "73d6b76b-34ae-4b73-b4ae-382d6c30a610",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "47b8802a-2463-4b24-b7f2-fbffb99efbda",
                    "LayerId": "b777d2fc-c237-4161-bf6c-318709ec9ee7"
                }
            ]
        },
        {
            "id": "2ee1a628-2154-45f3-aa7d-91708b60b0f3",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "1d24b181-9201-449c-9c4e-0555ee961453",
            "compositeImage": {
                "id": "c7741751-22de-45c4-bc8d-dbc31122d82c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "2ee1a628-2154-45f3-aa7d-91708b60b0f3",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "dd9ca04d-e662-4f37-9f51-dceaca2fa678",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "2ee1a628-2154-45f3-aa7d-91708b60b0f3",
                    "LayerId": "b777d2fc-c237-4161-bf6c-318709ec9ee7"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "b777d2fc-c237-4161-bf6c-318709ec9ee7",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "1d24b181-9201-449c-9c4e-0555ee961453",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 307,
    "xorig": 0,
    "yorig": 0
}