{
    "id": "2e811c91-e21c-4c0f-814a-752693d94a16",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_redNameTag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 306,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "51170026-b62b-4369-bcc0-0f859e23a44a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e811c91-e21c-4c0f-814a-752693d94a16",
            "compositeImage": {
                "id": "0ab77410-b6b4-43dc-bd3f-395b4a200aec",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "51170026-b62b-4369-bcc0-0f859e23a44a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "5c71d93f-d81a-4e90-b47b-a166ff3c63b4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "51170026-b62b-4369-bcc0-0f859e23a44a",
                    "LayerId": "99faadd9-47d5-40f5-8220-6253b8ffe8ad"
                }
            ]
        },
        {
            "id": "fd9d7a54-9493-42e5-aa27-5f8c143f1106",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e811c91-e21c-4c0f-814a-752693d94a16",
            "compositeImage": {
                "id": "d677c8ea-eeda-4bf1-b978-696db5102647",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fd9d7a54-9493-42e5-aa27-5f8c143f1106",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "15888ec8-fdd8-403c-92e8-08d5da2e4778",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fd9d7a54-9493-42e5-aa27-5f8c143f1106",
                    "LayerId": "99faadd9-47d5-40f5-8220-6253b8ffe8ad"
                }
            ]
        },
        {
            "id": "5f2e5b8e-4041-4607-9b97-e695fb9adab1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "2e811c91-e21c-4c0f-814a-752693d94a16",
            "compositeImage": {
                "id": "db1ad6a5-f429-40e9-b6c8-8ca61192b66d",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "5f2e5b8e-4041-4607-9b97-e695fb9adab1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e7684eba-d4ae-4afb-a642-373215ee4985",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "5f2e5b8e-4041-4607-9b97-e695fb9adab1",
                    "LayerId": "99faadd9-47d5-40f5-8220-6253b8ffe8ad"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "99faadd9-47d5-40f5-8220-6253b8ffe8ad",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "2e811c91-e21c-4c0f-814a-752693d94a16",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 307,
    "xorig": 0,
    "yorig": 0
}