{
    "id": "27a3bf92-025b-4e7c-a8b8-1ece9a2b85b1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pieceOrange",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "1134456b-a27a-4c23-b76f-8ff4b4e69b57",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "27a3bf92-025b-4e7c-a8b8-1ece9a2b85b1",
            "compositeImage": {
                "id": "0ed0e7f4-4f11-4123-a23b-a7eaa2a6d235",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "1134456b-a27a-4c23-b76f-8ff4b4e69b57",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "c7aa910c-98ae-42e9-8af8-614d72a96f41",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "1134456b-a27a-4c23-b76f-8ff4b4e69b57",
                    "LayerId": "9069ebe3-c731-43a9-939d-4a1f395a2e88"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "9069ebe3-c731-43a9-939d-4a1f395a2e88",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "27a3bf92-025b-4e7c-a8b8-1ece9a2b85b1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 9,
    "yorig": 26
}