{
    "id": "941b28b6-3ffe-4920-84d3-a9d53712327b",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pieceBlue",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b0dd1b43-4c67-49d0-b95f-f0ff52380446",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "941b28b6-3ffe-4920-84d3-a9d53712327b",
            "compositeImage": {
                "id": "2465ec6b-8dbc-40e9-b138-2e6728152e09",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b0dd1b43-4c67-49d0-b95f-f0ff52380446",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "7a5c03d0-c1fb-47ea-8f44-e05aae21ba2d",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b0dd1b43-4c67-49d0-b95f-f0ff52380446",
                    "LayerId": "9cc5dea6-b10d-491a-a335-e607ecf576df"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "9cc5dea6-b10d-491a-a335-e607ecf576df",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "941b28b6-3ffe-4920-84d3-a9d53712327b",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 26
}