{
    "id": "7d0b851e-a5f8-4aee-82b9-193eaf134d69",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_greenNameTag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 306,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "4de785c7-bbca-4b35-aa88-598b25755f45",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d0b851e-a5f8-4aee-82b9-193eaf134d69",
            "compositeImage": {
                "id": "e185b9fb-d3aa-483c-a318-dbb719c7995e",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "4de785c7-bbca-4b35-aa88-598b25755f45",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ffdad589-7ab9-4f11-b884-fd61836808c7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "4de785c7-bbca-4b35-aa88-598b25755f45",
                    "LayerId": "c7604a19-ac52-4d8d-98e1-7eb35ae9f361"
                }
            ]
        },
        {
            "id": "fe863b08-96f8-42ff-bcf7-57e333ac7694",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d0b851e-a5f8-4aee-82b9-193eaf134d69",
            "compositeImage": {
                "id": "a6b4a8a9-368a-452c-ab98-a10b4b458b7f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fe863b08-96f8-42ff-bcf7-57e333ac7694",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "d658eb83-9126-44cb-9c23-71bb42a23807",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fe863b08-96f8-42ff-bcf7-57e333ac7694",
                    "LayerId": "c7604a19-ac52-4d8d-98e1-7eb35ae9f361"
                }
            ]
        },
        {
            "id": "9e680a90-b8b7-4770-9985-6a9dfaf49c0a",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "7d0b851e-a5f8-4aee-82b9-193eaf134d69",
            "compositeImage": {
                "id": "02175507-72b5-49b3-8305-391b36d47c3a",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9e680a90-b8b7-4770-9985-6a9dfaf49c0a",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "e36caca7-b3ab-42fa-9080-a024f3e4b5f4",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9e680a90-b8b7-4770-9985-6a9dfaf49c0a",
                    "LayerId": "c7604a19-ac52-4d8d-98e1-7eb35ae9f361"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "c7604a19-ac52-4d8d-98e1-7eb35ae9f361",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "7d0b851e-a5f8-4aee-82b9-193eaf134d69",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 307,
    "xorig": 0,
    "yorig": 0
}