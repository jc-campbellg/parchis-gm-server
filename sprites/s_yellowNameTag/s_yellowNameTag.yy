{
    "id": "ba8e8cb8-21d3-47f3-9f51-4aaf8182f726",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_yellowNameTag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 306,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "11f11bdf-3eff-4e47-a959-4c968751f33c",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba8e8cb8-21d3-47f3-9f51-4aaf8182f726",
            "compositeImage": {
                "id": "d6ceadd7-7068-475f-896e-c8a24dcdb773",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "11f11bdf-3eff-4e47-a959-4c968751f33c",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2c77b3f5-843c-40fc-bebd-51f7364c3ae1",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "11f11bdf-3eff-4e47-a959-4c968751f33c",
                    "LayerId": "7980357b-2a30-4c00-8e85-a37bb3ebe7f9"
                }
            ]
        },
        {
            "id": "c051b25d-a1f2-4fe2-ad41-d771639a8cf1",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba8e8cb8-21d3-47f3-9f51-4aaf8182f726",
            "compositeImage": {
                "id": "2b6b77dd-c8ff-4e7d-b393-f554cf152152",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "c051b25d-a1f2-4fe2-ad41-d771639a8cf1",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "f5daaa5a-9731-4f56-a388-5b1d72c3b3e9",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "c051b25d-a1f2-4fe2-ad41-d771639a8cf1",
                    "LayerId": "7980357b-2a30-4c00-8e85-a37bb3ebe7f9"
                }
            ]
        },
        {
            "id": "61e198f7-504d-4fc7-96ea-6799986787a9",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ba8e8cb8-21d3-47f3-9f51-4aaf8182f726",
            "compositeImage": {
                "id": "032a1d7f-8031-4da2-beb6-a2453b3871d7",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "61e198f7-504d-4fc7-96ea-6799986787a9",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "2d0939e1-89d2-4238-ac31-d5075d63ba94",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "61e198f7-504d-4fc7-96ea-6799986787a9",
                    "LayerId": "7980357b-2a30-4c00-8e85-a37bb3ebe7f9"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "7980357b-2a30-4c00-8e85-a37bb3ebe7f9",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ba8e8cb8-21d3-47f3-9f51-4aaf8182f726",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 307,
    "xorig": 0,
    "yorig": 0
}