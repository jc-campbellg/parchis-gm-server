{
    "id": "3cfde0c9-e3a0-4020-9ae8-d8952de703f1",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_board",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 600,
    "bbox_left": 0,
    "bbox_right": 590,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "233de385-e72e-4dc6-a13c-7c5e5f8ea012",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "3cfde0c9-e3a0-4020-9ae8-d8952de703f1",
            "compositeImage": {
                "id": "3a96248c-32ed-4d1c-9819-ee2a507ac3b2",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "233de385-e72e-4dc6-a13c-7c5e5f8ea012",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bcc74d8c-379e-4d21-b07a-aa15abd3c6e0",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "233de385-e72e-4dc6-a13c-7c5e5f8ea012",
                    "LayerId": "c634c31d-27f5-4534-8260-84cd1c65f5fb"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 601,
    "layers": [
        {
            "id": "c634c31d-27f5-4534-8260-84cd1c65f5fb",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "3cfde0c9-e3a0-4020-9ae8-d8952de703f1",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 591,
    "xorig": 0,
    "yorig": 0
}