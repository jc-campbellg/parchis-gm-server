{
    "id": "4227cc2d-18ef-438a-8cc3-a1b6709a371c",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pieceYellow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "164cbcc8-f169-45ae-9cb7-fa96604126e5",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "4227cc2d-18ef-438a-8cc3-a1b6709a371c",
            "compositeImage": {
                "id": "c65d5f4f-ac67-4e2d-b0b3-cc59645ad78c",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "164cbcc8-f169-45ae-9cb7-fa96604126e5",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "12426a8f-17fa-4b9c-992b-d5af22cf2d48",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "164cbcc8-f169-45ae-9cb7-fa96604126e5",
                    "LayerId": "d7845f3e-0cfd-4746-8e19-a1c3d6f76c33"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "d7845f3e-0cfd-4746-8e19-a1c3d6f76c33",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "4227cc2d-18ef-438a-8cc3-a1b6709a371c",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 9,
    "yorig": 26
}