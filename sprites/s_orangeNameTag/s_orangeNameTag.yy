{
    "id": "adc6c594-cce0-4b43-8284-e19b5ca23f55",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_orangeNameTag",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 57,
    "bbox_left": 0,
    "bbox_right": 306,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "933c3b0f-1a4f-49fe-892d-3b24a54e84fa",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adc6c594-cce0-4b43-8284-e19b5ca23f55",
            "compositeImage": {
                "id": "9853cf9a-53b3-41dd-b18f-22bef68f24cd",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "933c3b0f-1a4f-49fe-892d-3b24a54e84fa",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "87731a82-c3b2-44c7-b37f-d02b446d83aa",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "933c3b0f-1a4f-49fe-892d-3b24a54e84fa",
                    "LayerId": "6720ade4-6079-44d1-9314-e0796fc1ad47"
                }
            ]
        },
        {
            "id": "fc7fce76-fe0d-4ec1-91ee-3d75da502e50",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adc6c594-cce0-4b43-8284-e19b5ca23f55",
            "compositeImage": {
                "id": "23dff847-5d84-481d-9e21-42b2e91a4504",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "fc7fce76-fe0d-4ec1-91ee-3d75da502e50",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "ff8413cf-d78a-4685-8541-4b5e5e6f91bb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "fc7fce76-fe0d-4ec1-91ee-3d75da502e50",
                    "LayerId": "6720ade4-6079-44d1-9314-e0796fc1ad47"
                }
            ]
        },
        {
            "id": "b2d288ce-7c35-40e2-a9a4-71f3333d82a2",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "adc6c594-cce0-4b43-8284-e19b5ca23f55",
            "compositeImage": {
                "id": "15946af0-4ea5-47c2-8a10-b9adede96751",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b2d288ce-7c35-40e2-a9a4-71f3333d82a2",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "79dda7b8-0df5-4dc4-ab0c-8acc7de27449",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b2d288ce-7c35-40e2-a9a4-71f3333d82a2",
                    "LayerId": "6720ade4-6079-44d1-9314-e0796fc1ad47"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 58,
    "layers": [
        {
            "id": "6720ade4-6079-44d1-9314-e0796fc1ad47",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "adc6c594-cce0-4b43-8284-e19b5ca23f55",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 307,
    "xorig": 0,
    "yorig": 0
}