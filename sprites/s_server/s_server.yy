{
    "id": "35cdfff1-383a-46ac-9e64-025cbf7baa00",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_server",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 511,
    "bbox_left": 0,
    "bbox_right": 511,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "8d247410-9191-4246-8f80-5975a2a94eae",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "35cdfff1-383a-46ac-9e64-025cbf7baa00",
            "compositeImage": {
                "id": "54ffbbb1-4bdf-4e36-bc6d-36fe46bb8e00",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "8d247410-9191-4246-8f80-5975a2a94eae",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "92739028-0e95-46db-90ed-e7ab20ddcde7",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "8d247410-9191-4246-8f80-5975a2a94eae",
                    "LayerId": "786103f3-3b91-4b8b-aca2-22dda4186be3"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 512,
    "layers": [
        {
            "id": "786103f3-3b91-4b8b-aca2-22dda4186be3",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "35cdfff1-383a-46ac-9e64-025cbf7baa00",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 512,
    "xorig": 0,
    "yorig": 0
}