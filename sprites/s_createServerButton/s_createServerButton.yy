{
    "id": "c921bf08-753e-42ae-9529-986e3c0a8e62",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_createServerButton",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 81,
    "bbox_left": 0,
    "bbox_right": 388,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "3661d31c-d7da-4c3a-b778-1cd5cb018dcc",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "c921bf08-753e-42ae-9529-986e3c0a8e62",
            "compositeImage": {
                "id": "f70a1638-e550-4921-8462-63157a38232f",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "3661d31c-d7da-4c3a-b778-1cd5cb018dcc",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "6bf9ecb1-8b3b-45f2-80ec-4eb255bd22fb",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "3661d31c-d7da-4c3a-b778-1cd5cb018dcc",
                    "LayerId": "3addb776-3696-4a80-b8d0-e14acf8aa1af"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 82,
    "layers": [
        {
            "id": "3addb776-3696-4a80-b8d0-e14acf8aa1af",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "c921bf08-753e-42ae-9529-986e3c0a8e62",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 0,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 389,
    "xorig": 0,
    "yorig": 0
}