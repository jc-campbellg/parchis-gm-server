{
    "id": "fb9ebd04-d8a2-4436-9052-f5ca6dcc93be",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pieceGreen",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 30,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "6c83b325-15ed-4b87-8764-60085a9dfd9b",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "fb9ebd04-d8a2-4436-9052-f5ca6dcc93be",
            "compositeImage": {
                "id": "2d32275b-3a61-425d-aa78-39cea1dfad19",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "6c83b325-15ed-4b87-8764-60085a9dfd9b",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "996fe94e-a236-404d-a026-2ce864ee820b",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "6c83b325-15ed-4b87-8764-60085a9dfd9b",
                    "LayerId": "8285270e-3a89-4307-8a0f-20f3c6687cda"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 31,
    "layers": [
        {
            "id": "8285270e-3a89-4307-8a0f-20f3c6687cda",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "fb9ebd04-d8a2-4436-9052-f5ca6dcc93be",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 26
}