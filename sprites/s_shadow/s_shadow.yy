{
    "id": "553252d7-8087-4089-9126-2c508b370f85",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_shadow",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 12,
    "bbox_left": 0,
    "bbox_right": 19,
    "bbox_top": 0,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "b540eb0c-6a62-4cc5-ac5a-c7710cd35b39",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "553252d7-8087-4089-9126-2c508b370f85",
            "compositeImage": {
                "id": "4307b34c-0972-4258-bc85-b2e8ceac4aaf",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "b540eb0c-6a62-4cc5-ac5a-c7710cd35b39",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "aaa49fc9-255d-4540-aa77-555f3dcddeaf",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "b540eb0c-6a62-4cc5-ac5a-c7710cd35b39",
                    "LayerId": "ad23c904-1dbb-4855-a7e1-cac2c3b8b91d"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 13,
    "layers": [
        {
            "id": "ad23c904-1dbb-4855-a7e1-cac2c3b8b91d",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "553252d7-8087-4089-9126-2c508b370f85",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 4,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 20,
    "xorig": 10,
    "yorig": 6
}