{
    "id": "ad99080c-777e-450f-92cd-c5d75e086b96",
    "modelName": "GMSprite",
    "mvc": "1.12",
    "name": "s_pieceRed",
    "For3D": false,
    "HTile": false,
    "VTile": false,
    "bbox_bottom": 31,
    "bbox_left": 0,
    "bbox_right": 18,
    "bbox_top": 1,
    "bboxmode": 0,
    "colkind": 1,
    "coltolerance": 0,
    "edgeFiltering": false,
    "frames": [
        {
            "id": "9580746c-f547-4d4a-be0f-0a23d20f4642",
            "modelName": "GMSpriteFrame",
            "mvc": "1.0",
            "SpriteId": "ad99080c-777e-450f-92cd-c5d75e086b96",
            "compositeImage": {
                "id": "eab2b7e7-e50e-4b82-a1c5-3fcc528015f6",
                "modelName": "GMSpriteImage",
                "mvc": "1.0",
                "FrameId": "9580746c-f547-4d4a-be0f-0a23d20f4642",
                "LayerId": "00000000-0000-0000-0000-000000000000"
            },
            "images": [
                {
                    "id": "bf161c7c-67c8-45a6-9c34-738b1b634dac",
                    "modelName": "GMSpriteImage",
                    "mvc": "1.0",
                    "FrameId": "9580746c-f547-4d4a-be0f-0a23d20f4642",
                    "LayerId": "feb0b183-4b96-4044-a7f6-525e7e2aee14"
                }
            ]
        }
    ],
    "gridX": 0,
    "gridY": 0,
    "height": 32,
    "layers": [
        {
            "id": "feb0b183-4b96-4044-a7f6-525e7e2aee14",
            "modelName": "GMImageLayer",
            "mvc": "1.0",
            "SpriteId": "ad99080c-777e-450f-92cd-c5d75e086b96",
            "blendMode": 0,
            "isLocked": false,
            "name": "default",
            "opacity": 100,
            "visible": true
        }
    ],
    "origin": 9,
    "originLocked": false,
    "playbackSpeed": 15,
    "playbackSpeedType": 0,
    "premultiplyAlpha": false,
    "sepmasks": false,
    "swatchColours": null,
    "swfPrecision": 2.525,
    "textureGroupId": "1225f6b0-ac20-43bd-a82e-be73fa0b6f4f",
    "type": 0,
    "width": 19,
    "xorig": 9,
    "yorig": 26
}