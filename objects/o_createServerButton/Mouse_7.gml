/// @description Ask for port
port = real(get_string("Port:", port));

if (port > 0 && port < 65535) {
	room_goto_next();
}