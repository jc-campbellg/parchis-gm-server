{
    "id": "c0e56958-407e-4bd5-bd63-43c339e07e5a",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_createServerButton",
    "eventList": [
        {
            "id": "84ebc5a4-ed7d-4745-b9c5-aff1c37d3114",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 7,
            "eventtype": 6,
            "m_owner": "c0e56958-407e-4bd5-bd63-43c339e07e5a"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "c921bf08-753e-42ae-9529-986e3c0a8e62",
    "visible": true
}