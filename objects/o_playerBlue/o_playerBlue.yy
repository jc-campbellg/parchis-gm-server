{
    "id": "241bef5a-e83c-4fe5-9145-0eef621ac4a6",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_playerBlue",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a05c2255-ec30-49b1-8019-05be7ba5ed03",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "472375ab-ae14-45ae-acf2-d84da09c91f9",
    "visible": true
}