draw_set_font(ft_console);
draw_set_color(c_white);
draw_text(0, 0, "Start: "+string(start));
draw_text(0, 16, "eatReward: "+string(eatReward));
draw_text(0, 16*2, "goalReward: "+string(goalReward));
draw_text(0, 16*3, "uTurn: "+string(uTurn));
draw_text(0, 16*4, "moveSpeed: "+string(moveSpeed));
draw_text(0, 16*5, "teamPlay: "+string(teamPlay));
draw_text(0, 16*7, "moveTest: "+string(moveTest));