{
    "id": "11b9db98-3b22-4248-9bd5-703f47c5f87b",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_playerPurple",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a05c2255-ec30-49b1-8019-05be7ba5ed03",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "1d24b181-9201-449c-9c4e-0555ee961453",
    "visible": true
}