/// @description set color
myNumber = get_instance_number(o_piece);
switch (myNumber) {
    case 0:
	case 1:
	case 2:
	case 3:
        sprite_index = s_pieceRed
        break;
	case 4:
	case 5:
	case 6:
	case 7:
        sprite_index = s_pieceBlue
        break;
	case 8:
	case 9:
	case 10:
	case 11:
        sprite_index = s_pieceYellow
        break;
	case 12:
	case 13:
	case 14:
	case 15:
        sprite_index = s_pieceGreen
        break;
	case 16:
	case 17:
	case 18:
	case 19:
        sprite_index = s_piecePurple
        break;
	case 20:
	case 21:
	case 22:
	case 23:
        sprite_index = s_pieceOrange
        break;
}

safePlaces = ds_list_create();
winPlaces = ds_list_create();
startPlaces = ds_list_create();

ds_list_add(safePlaces, 0, 7, 12, 17, 24, 29, 34, 41, 46, 51, 58, 63, 68, 75, 80, 85, 92, 97);
ds_list_add(winPlaces, 97, 12, 29, 46, 63, 80);
ds_list_add(startPlaces, 0, 17, 34, 51, 68, 85);

color = get_color_by_sprite();
winSpot = ds_list_find_value(winPlaces, color);
startSpot = ds_list_find_value(startPlaces, color);

ds_list_destroy(winPlaces);
ds_list_destroy(startPlaces);