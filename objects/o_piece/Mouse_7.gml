/// @description Move Animation Test
if (pos == -1) {
	if (can_move_here(startSpot)) {
		send_moveToStart();
	}
} else {
	if (can_move_steps(o_gameState.moveTest)) {
		send_moveToPosition(o_gameState.moveTest);
	}
}