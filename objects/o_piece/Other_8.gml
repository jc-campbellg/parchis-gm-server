/// @description Destroy Path
path_delete(movePath);
send_jumpToPosition(myNumber);

if (pos != -1) {
	var piecesInPos = find_piece_in_pos(pos, onWinningSpace);
	if (array_length_1d(piecesInPos)) {
		var otherPiece = piecesInPos[0];
		
		if (otherPiece.color == color) {
			// Bridge, we are brothers
			with (otherPiece) {
				var pathArray = [pt_steps, pt_stepsFirst, pt_stepsSecond];
	
				if onWinningSpace then pathArray = get_won_path_by_sprite();
				
				x = path_get_point_x(pathArray[1], onWinningSpace ? pos-winSpot : pos);
				y = path_get_point_y(pathArray[1], onWinningSpace ? pos-winSpot : pos);
				
				send_jumpToPosition(myNumber);
			}
		} else {
			// Eat your enemy
			if (ds_list_find_index(safePlaces, pos) < 0) {
				with (otherPiece) {
					send_moveToHome();
				}
				var pathArray = [pt_steps, pt_stepsFirst, pt_stepsSecond];
	
				if onWinningSpace then pathArray = get_won_path_by_sprite();
				
				x = path_get_point_x(pathArray[0], onWinningSpace ? pos-winSpot : pos);
				y = path_get_point_y(pathArray[0], onWinningSpace ? pos-winSpot : pos);
				
				send_jumpToPosition(myNumber);
			} else {
				// Safe Place
				with (otherPiece) {
					var pathArray = [pt_steps, pt_stepsFirst, pt_stepsSecond];
	
					if onWinningSpace then pathArray = get_won_path_by_sprite();
				
					x = path_get_point_x(pathArray[1], onWinningSpace ? pos-winSpot : pos);
					y = path_get_point_y(pathArray[1], onWinningSpace ? pos-winSpot : pos);
					
					send_jumpToPosition(myNumber);
				}
			}
		}
	}
}