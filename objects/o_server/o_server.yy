{
    "id": "92edafa8-65b7-4068-87d2-e1139b53f7d7",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_server",
    "eventList": [
        {
            "id": "604597d0-3560-4cd9-899c-2d4aa3eac737",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 0,
            "m_owner": "92edafa8-65b7-4068-87d2-e1139b53f7d7"
        },
        {
            "id": "beaa0507-e3a2-4222-bef4-da10034b0cfe",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 0,
            "eventtype": 8,
            "m_owner": "92edafa8-65b7-4068-87d2-e1139b53f7d7"
        },
        {
            "id": "a198b7d4-6002-4772-a0cc-468265b52b2a",
            "modelName": "GMEvent",
            "mvc": "1.0",
            "IsDnD": false,
            "collisionObjectId": "00000000-0000-0000-0000-000000000000",
            "enumb": 68,
            "eventtype": 7,
            "m_owner": "92edafa8-65b7-4068-87d2-e1139b53f7d7"
        }
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "00000000-0000-0000-0000-000000000000",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "35cdfff1-383a-46ac-9e64-025cbf7baa00",
    "visible": true
}