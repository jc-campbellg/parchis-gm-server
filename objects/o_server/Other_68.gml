/// @description Online Gaming
var type = async_load[? "type"];

switch(type) {
	case network_type_connect:
		var socket = async_load[? "socket"];
		ds_list_add(socketList, socket);
		send_syncNames(socket);
		break;
		
	case network_type_disconnect:
		var socket = async_load[? "socket"];
		ds_list_delete(socketList, ds_list_find_index(socketList, socket));
		send_removePlayer(socket);
		break;
		
	case network_type_data:
		var socket = async_load[? "id"];
		var buffer = async_load[? "buffer"];
		handle_network_data(socket, buffer);
		break;
}