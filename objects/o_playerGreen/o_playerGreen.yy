{
    "id": "0c1fc35a-7445-4db7-bbdf-a85321afe60c",
    "modelName": "GMObject",
    "mvc": "1.0",
    "name": "o_playerGreen",
    "eventList": [
        
    ],
    "maskSpriteId": "00000000-0000-0000-0000-000000000000",
    "overriddenProperties": null,
    "parentObjectId": "a05c2255-ec30-49b1-8019-05be7ba5ed03",
    "persistent": false,
    "physicsAngularDamping": 0.1,
    "physicsDensity": 0.5,
    "physicsFriction": 0.2,
    "physicsGroup": 0,
    "physicsKinematic": false,
    "physicsLinearDamping": 0.1,
    "physicsObject": false,
    "physicsRestitution": 0.1,
    "physicsSensor": false,
    "physicsShape": 1,
    "physicsShapePoints": null,
    "physicsStartAwake": true,
    "properties": null,
    "solid": false,
    "spriteId": "7d0b851e-a5f8-4aee-82b9-193eaf134d69",
    "visible": true
}