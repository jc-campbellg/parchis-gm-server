/// @description Draw Name
if (!o_gameState.start) {
	if ready then image_index = 1 else image_index = 0;
} else {
	if myTurn then image_index = 2 else image_index = 0;
}
draw_self();
draw_set_font(ft_name);
draw_set_color(c_black);
draw_text(x+62, y+13, name);