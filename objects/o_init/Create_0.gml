/// @description Global variables
enum net {
	syncNames,
	tryAgainColor,
	join,
	newJoin,
	removePlayer,
	jumpToPosition,
	syncPositions,
	moveToPosition,
	ready,
	gameStart,
	chat
};

enum gameColor {
	red,
	blue,
	yellow,
	green,
	purple,
	orange
};

globalvar port;
port = 6510;

globalvar playerInstances;
playerInstances[gameColor.red] = o_playerRed;
playerInstances[gameColor.blue] = o_playerBlue;
playerInstances[gameColor.yellow] = o_playerYellow;
playerInstances[gameColor.green] = o_playerGreen;
playerInstances[gameColor.purple] = o_playerPurple;
playerInstances[gameColor.orange] = o_playerOrange;

enum pieces {
	red1,
	red2,
	red3,
	red4,
	blue1,
	blue2,
	blue3,
	blue4,
	yellow1,
	yellow2,
	yellow3,
	yellow4,
	green1,
	green2,
	green3,
	green4,
	purple1,
	purple2,
	purple3,
	purple4,
	orange1,
	orange2,
	orange3,
	orange4
}